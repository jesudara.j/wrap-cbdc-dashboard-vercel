import { EyeInvisibleOutlined, EyeOutlined } from "@ant-design/icons";
import {
  Button,
  Card,
  Checkbox,
  Col,
  Input,
  message,
  message as MessageDrop,
  Row,
} from "antd";
import { passwordStrength } from "check-password-strength";
import { useRef, useState } from "react";
import {
  Form,
  Link,
  redirect,
  useActionData,
  useLoaderData,
  useNavigate,
  useSubmit,
  useTransition,
} from "remix";
import { userPrefs } from "~/cookies";
import {
  loginUser,
  registerUser,
  requestPasswordReset,
  resetPassword,
} from "~/utils";

export const action = async ({ request }) => {
  // await new Promise(res => setTimeout(res, 1000));
  let url = new URL(request.url);
  let action = url.searchParams.get("action");
  let body = await request.formData();
  console.log(action);

  if (action === "login") {
    const cookieHeader = request.headers.get("Cookie");
    const cookie = (await userPrefs.parse(cookieHeader)) || {};
    const email = body.get("email");
    const password = body.get("password");
    const res = await loginUser({ email, password });
    cookie.token = res?.data?.token;
    console.log("Response", res);
    if (res.error) {
      return res;
    }
    return redirect("/dashboard/", {
      headers: {
        "Set-Cookie": await userPrefs.serialize(cookie),
      },
    });
  } else if (action === "register") {
    const firstname = body.get("firstname");
    const lastname = body.get("lastname");
    const email = body.get("email");
    const phone = body.get("phone");
    const country = body.get("country");
    const password = body.get("password");
    const confirmPass = body.get("confirmpass");
    const res = await registerUser({
      firstname,
      lastname,
      email,
      phone,
      country,
      password,
      confirmPass,
    });
    console.log("Response", res);
    if (res.error) {
      return res;
    }
    return redirect("/?index&view=verify");
  }

  return { message: "No actions matched", error: true };
};

export const loader = async ({ request }) => {
  let url = new URL(request.url);
  let view = url.searchParams.get("view");
  let step = url.searchParams.get("step");
  let resetString = url.searchParams.get("resetstring");
  let id = url.searchParams.get("id");
  let status = url.searchParams.get("status");
  let msg = url.searchParams.get("msg");

  return { view, status, msg, step, resetString, id };
};

export default function Validation() {
  const actionData = useActionData();
  const { view, status, msg, step, resetString, id } = useLoaderData();

  // const [hasAccount, setHasAccount] = useState(true);
  React.useEffect(() => {
    if (actionData?.error) MessageDrop.error(actionData?.message);
    return () => {};
  }, [actionData]);

  return (
    <Row
      gutter={8}
      style={{
        width: "100%",
        minHeight: "100vh",
        margin: 0,
        paddingTop: 50,
        paddingBottom: 50,
        backgroundColor: "#3F47CC",
        backgroundImage: "url('/auth_img.png')",
        backgroundPosition: "130% -40%",
        backgroundSize: "50%",
        backgroundRepeat: "no-repeat",
      }}
    >
      <Col
        xxs={{ span: 24 }}
        xs={{ span: 24 }}
        sm={{ span: 24 }}
        lg={{ span: 24 }}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <img src="/logo_edited.png" width={100} height={100} />
      </Col>
      <Col
        xxs={{ span: 0 }}
        xs={{ span: 0 }}
        sm={{ span: 3 }}
        lg={{ span: 6 }}
      ></Col>
      <Col
        xxs={{ span: 24 }}
        xs={{ span: 24 }}
        sm={{ span: 18 }}
        lg={{ span: 12 }}
      >
        {!view && <LoginView />}
        {view === "login" && <LoginView />}
        {view === "signup" && <RegisterView />}
        {view === "verified" && (
          <SuccessVerification status={status} msg={msg} />
        )}
        {view === "verify" && <VerifyEmail />}
        {view === "reset" && step === "1" && <PasswordResetRequest />}
        {view === "reset" && step === "2" && (
          <PasswordReset id={id} resetString={resetString} />
        )}
      </Col>
      <Col
        xxs={{ span: 0 }}
        xs={{ span: 0 }}
        sm={{ span: 3 }}
        lg={{ span: 6 }}
      ></Col>
    </Row>
  );
}

export function ErrorBoundary({ error }) {
  console.error(error);
  return <div style={{ border: "1ppx solid red" }}>{error}</div>;
}

// Login comp
const LoginView = ({ hasAccount, setHasAccount }) => {
  const [hidePass, setHidePass] = useState(true);
  const [emailVal, setEmailVal] = useState();
  const [passwordVal, setPasswordVal] = useState();
  const [loading, setLoading] = useState(false);
  const submit = useSubmit();
  const loginFormRef = useRef();
  const transition = useTransition();
  return (
    <Card
      // hoverable
      // extra={<img src="/icon-512x512.png" width={40} height={40} />}
      style={{ borderRadius: 10, textAlign: "center" }}
      bodyStyle={{
        paddingLeft: "15%",
        paddingRight: "15%",
        paddingTop: "13%",
        paddingBottom: "13%",
      }}
    >
      <h1 style={{ textTransform: "uppercase", margin: 0, color: "#3F47CC" }}>
        Login to your account
      </h1>
      <span>Use your existing cNGN account details to sign in.</span>
      <br />
      <br />
      <Form
        action="/?index&view=login&action=login"
        method="POST"
        ref={loginFormRef}
      >
        <Input
          placeholder="Enter email"
          type="email"
          size="large"
          name="email"
          value={emailVal}
          style={{
            borderRadius: 5,
            backgroundColor: "#D7F5FF",
            color: "rgba(0, 98, 131, 0.45)",
          }}
          onChange={(e) => setEmailVal(e.target.value)}
        />
        <br />
        <br />
        <Input
          placeholder="Enter password"
          type={hidePass ? "password" : "text"}
          size="large"
          name="password"
          value={passwordVal}
          style={{
            borderRadius: 5,
            backgroundColor: "#D7F5FF",
            color: "rgba(0, 98, 131, 0.45)",
          }}
          onChange={(e) => setPasswordVal(e.target.value)}
          suffix={
            hidePass ? (
              <EyeOutlined
                onClick={() => setHidePass(!hidePass)}
                style={{ color: "#3F47CC" }}
              />
            ) : (
              <EyeInvisibleOutlined
                onClick={() => setHidePass(!hidePass)}
                style={{ color: "#3F47CC" }}
              />
            )
          }
        />
        <br />
        <br />
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <span style={{ color: "#006283" }}>Forgot Password?</span>
          <Link to="/?view=reset&step=1" style={{ color: "#3F47CC" }}>
            Reset it here
          </Link>
        </div>
      </Form>
      <br />
      <br />
      <Button
        type="link"
        size="large"
        block
        style={{
          backgroundColor: "#3F47CC",
          color: "white",
          borderRadius: 5,
        }}
        loading={
          transition.state === "submitting" || transition.state === "loading"
        }
        onClick={async () => {
          submit(loginFormRef.current, { replace: false });
        }}
      >
        SIGN IN
      </Button>

      <br />
      <br />
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <span style={{ color: "#006283" }}>Need an Account?</span>
        <Link
          to="?view=signup"
          style={{ color: "#3F47CC" }}
          // onClick={(e) => {
          //   e.preventDefault();
          //   setHasAccount(!hasAccount);
          // }}
        >
          SIGN UP
        </Link>
      </div>
      <br />
      <br />
      <span style={{ color: "#006283" }}>
        © 2022 WrappedCBDC Powered by Convexity
      </span>
      <br />
      {/* <span style={{fontSize: 12}}>Access Admin: <Link
          to="/admin/?view=login"
          style={{ color: "#3F47CC" }}
          // onClick={(e) => {
          //   e.preventDefault();
          //   setHasAccount(!hasAccount);
          // }}
        >
          LOGIN
        </Link></span> */}
    </Card>
  );
};

const PasswordResetRequest = ({ hasAccount, setHasAccount }) => {
  const [hidePass, setHidePass] = useState(true);
  const [emailVal, setEmailVal] = useState();
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  return (
    <Card
      // hoverable
      // extra={<img src="/icon-512x512.png" width={40} height={40} />}
      style={{ borderRadius: 10, textAlign: "center" }}
      bodyStyle={{
        paddingLeft: "15%",
        paddingRight: "15%",
        paddingTop: "13%",
        paddingBottom: "13%",
      }}
    >
      <h1 style={{ textTransform: "uppercase", margin: 0, color: "#3F47CC" }}>
        RESET PASSWORD
      </h1>
      <span>Enter your registered email to reset your password.</span>
      <br />
      <br />
      <Input
        placeholder="Enter email"
        type="email"
        size="large"
        name="email"
        value={emailVal}
        style={{
          borderRadius: 5,
          backgroundColor: "#D7F5FF",
          color: "rgba(0, 98, 131, 0.45)",
        }}
        onChange={(e) => setEmailVal(e.target.value)}
      />
      <br />
      <br />
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <span style={{ color: "#006283" }}>Remembered Password?</span>
        <Link to="/?view=login" style={{ color: "#3F47CC" }}>
          Sign in
        </Link>
      </div>
      <br />
      <br />
      <Button
        type="link"
        size="large"
        block
        style={{
          backgroundColor: "#3F47CC",
          color: "white",
          borderRadius: 5,
        }}
        loading={loading}
        onClick={async () => {
          // Call reset request API
          setLoading(true);

          const resp = await requestPasswordReset(emailVal);
          if (resp.error) {
            setLoading(false);
            return MessageDrop.error(resp.message);
          }

          MessageDrop.success(resp.message);

          setLoading(false);
        }}
      >
        SEND RESET LINK
      </Button>

      <br />
      <br />
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <span style={{ color: "#006283" }}>Need an Account?</span>
        <Link to="?view=signup" style={{ color: "#3F47CC" }}>
          SIGN UP
        </Link>
      </div>
    </Card>
  );
};

const PasswordReset = ({ hasAccount, setHasAccount, resetString, id }) => {
  const [hidePass, setHidePass] = useState(true);
  const [newPassword, setNewPassword] = useState();
  const [confirmNewPassword, setConfirmNewPassword] = useState();
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  return (
    <Card
      // hoverable
      // extra={<img src="/icon-512x512.png" width={40} height={40} />}
      style={{ borderRadius: 10, textAlign: "center" }}
      bodyStyle={{
        paddingLeft: "15%",
        paddingRight: "15%",
        paddingTop: "13%",
        paddingBottom: "13%",
      }}
    >
      <h1 style={{ textTransform: "uppercase", margin: 0, color: "#3F47CC" }}>
        RESET PASSWORD
      </h1>
      <span>
        Enter a password to restore access to your account, try not to forget
        this one .
      </span>
      <br />
      <br />
      <Input
        placeholder="Enter new password"
        type={hidePass ? "password" : "text"}
        size="large"
        name="password"
        value={newPassword}
        style={{
          borderRadius: 5,
          backgroundColor: "#D7F5FF",
          color: "rgba(0, 98, 131, 0.45)",
        }}
        onChange={(e) => setNewPassword(e.target.value)}
        suffix={
          hidePass ? (
            <EyeOutlined
              onClick={() => setHidePass(!hidePass)}
              style={{ color: "#3F47CC" }}
            />
          ) : (
            <EyeInvisibleOutlined
              onClick={() => setHidePass(!hidePass)}
              style={{ color: "#3F47CC" }}
            />
          )
        }
      />
      <br />
      <br />
      <Input
        placeholder="Confirm new password"
        type={hidePass ? "password" : "text"}
        size="large"
        name="password"
        value={confirmNewPassword}
        style={{
          borderRadius: 5,
          backgroundColor: "#D7F5FF",
          color: "rgba(0, 98, 131, 0.45)",
        }}
        onChange={(e) => setConfirmNewPassword(e.target.value)}
        suffix={
          hidePass ? (
            <EyeOutlined
              onClick={() => setHidePass(!hidePass)}
              style={{ color: "#3F47CC" }}
            />
          ) : (
            <EyeInvisibleOutlined
              onClick={() => setHidePass(!hidePass)}
              style={{ color: "#3F47CC" }}
            />
          )
        }
      />
      <br />
      <br />
      {/* <div style={{display: "flex", alignItems: "center", justifyContent: "space-between"}}>
          <span style={{color: "#006283"}}>Remembered Password?</span>
          <Link to="/?view=login" style={{ color: "#3F47CC" }}>Sign in</Link>
        </div>
        <br />
        <br /> */}
      <Button
        type="link"
        size="large"
        block
        style={{
          backgroundColor: "#3F47CC",
          color: "white",
          borderRadius: 5,
        }}
        loading={loading}
        onClick={async () => {
          // Call reset API
          setLoading(true);

          if (newPassword !== confirmNewPassword) {
            setLoading(false);
            return MessageDrop.warn("Your passwords do not match");
          }

          const resp = await resetPassword(id, resetString, newPassword);

          if (resp.error) {
            setLoading(false);
            return MessageDrop.error(resp.message);
          }

          MessageDrop.success(resp.message);
          navigate("/?view=login");

          setLoading(false);
        }}
      >
        RESET
      </Button>

      <br />
      <br />
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <span style={{ color: "#006283" }}>Need an Account?</span>
        <Link
          to="?view=signup"
          style={{ color: "#3F47CC" }}
          // onClick={(e) => {
          //   e.preventDefault();
          //   setHasAccount(!hasAccount);
          // }}
        >
          SIGN UP
        </Link>
      </div>
    </Card>
  );
};

const SuccessVerification = ({ status, msg }) => {
  const transition = useTransition();
  const navigate = useNavigate();
  return (
    <Card
      // hoverable
      // extra={<img src="/icon-512x512.png" width={40} height={40} />}
      style={{ borderRadius: 10, textAlign: "center" }}
      bodyStyle={{
        paddingLeft: "15%",
        paddingRight: "15%",
        paddingTop: "13%",
        paddingBottom: "13%",
      }}
    >
      <h1 style={{ textTransform: "uppercase", margin: 0, color: "#3F47CC" }}>
        Account Email Verification
      </h1>
      <span style={{ color: status !== "success" ? "red" : "green" }}>
        {status === "success"
          ? "Your account email verification was successful, now you can login."
          : `${msg}`}
      </span>
      <br />
      <br />

      <br />
      <br />
      <Button
        type="link"
        size="large"
        block
        style={{
          backgroundColor: "#3F47CC",
          color: "white",
          borderRadius: 5,
        }}
        loading={transition.state === "submitting"}
        onClick={async () => {
          navigate(status === "success" ? "/?view=login" : "/?view=signup");
        }}
      >
        {status === "success" ? "SIGN IN" : "SIGN UP"}
      </Button>

      <br />
    </Card>
  );
};
const VerifyEmail = ({ status }) => {
  const transition = useTransition();
  const navigate = useNavigate();
  return (
    <Card
      // hoverable
      // extra={<img src="/icon-512x512.png" width={40} height={40} />}
      style={{ borderRadius: 10, textAlign: "center" }}
      bodyStyle={{
        paddingLeft: "15%",
        paddingRight: "15%",
        paddingTop: "13%",
        paddingBottom: "13%",
      }}
    >
      <h1 style={{ textTransform: "uppercase", margin: 0, color: "#3F47CC" }}>
        Verification Email Sent
      </h1>
      <span>
        A verification email has been sent to the email provided, please click
        on link attached to said email to verify your account
      </span>
      <br />
      <span>
        If email can't be found please make sure to check the spam folder of
        your email application
      </span>
      <br />
      <br />

      <br />
      <br />
      <Button
        type="link"
        size="large"
        block
        style={{
          backgroundColor: "#3F47CC",
          color: "white",
          borderRadius: 5,
        }}
        loading={transition.state === "submitting"}
        onClick={async () => {
          navigate("/?view=login");
        }}
      >
        LOG IN
      </Button>

      <br />
    </Card>
  );
};

const RegisterView = ({ hasAccount, setHasAccount }) => {
  const [hidePass, setHidePass] = useState(true);
  const [firstname, setFirstName] = useState();
  const [lastname, setLastName] = useState();
  const [email, setEmail] = useState();
  const [phone, setPhone] = useState();
  const [country, setCountry] = useState();
  const [password, setPassword] = useState();
  const [confirmPass, setConfirmPass] = useState();
  const [loading, setLoading] = useState(false);
  const [policyOneCheck, setPolicyOneCheck] = useState(false);
  const [policyTwoCheck, setPolicyTwoCheck] = useState(false);
  const submit = useSubmit();
  const registerFormRef = useRef();
  const transition = useTransition();

  return (
    <Card
      // hoverable
      // title="Setup your account"
      // extra={<img src="/icon-512x512.png" width={40} height={40} />}
      style={{ borderRadius: 10, textAlign: "center" }}
      bodyStyle={{
        paddingLeft: "15%",
        paddingRight: "15%",
        paddingTop: "13%",
        paddingBottom: "13%",
      }}
    >
      <h1 style={{ textTransform: "uppercase", margin: 0, color: "#3F47CC" }}>
        SETUP YOUR ACCOUNT
      </h1>
      <span style={{ color: "#006283" }}>
        Convexity is your hub for payments, corporate treasury,
        <br /> and more. Please fill out the fields below on behalf of the
        authorized representative for your company.
      </span>
      <br />
      <br />
      <Form
        action="/?index&view=signup&action=register"
        method="POST"
        ref={registerFormRef}
      >
        <Row gutter={8} style={{ width: "100%", margin: 0, padding: 0 }}>
          <Col span={12}>
            <Input
              placeholder="First name"
              type="text"
              size="large"
              name="firstname"
              style={{
                borderRadius: 5,
                backgroundColor: "#D7F5FF",
                color: "rgba(0, 98, 131, 0.45)",
              }}
              value={firstname}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </Col>
          <Col span={12}>
            <Input
              placeholder="Last name"
              type="text"
              size="large"
              name="lastname"
              style={{
                borderRadius: 5,
                backgroundColor: "#D7F5FF",
                color: "rgba(0, 98, 131, 0.45)",
              }}
              value={lastname}
              onChange={(e) => setLastName(e.target.value)}
            />
          </Col>
        </Row>
        <br />
        <Input
          placeholder="Email"
          type="email"
          size="large"
          name="email"
          style={{
            borderRadius: 5,
            backgroundColor: "#D7F5FF",
            color: "rgba(0, 98, 131, 0.45)",
          }}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <br />
        <br />
        <Input
          placeholder="Phone number"
          type="text"
          size="large"
          name="phone"
          style={{
            borderRadius: 5,
            backgroundColor: "#D7F5FF",
            color: "rgba(0, 98, 131, 0.45)",
          }}
          value={phone}
          onChange={(e) => setPhone(e.target.value)}
        />
        <br />
        <br />
        <Input
          placeholder="Country of residence"
          type="text"
          size="large"
          name="country"
          style={{
            borderRadius: 5,
            backgroundColor: "#D7F5FF",
            color: "rgba(0, 98, 131, 0.45)",
          }}
          value={country}
          onChange={(e) => setCountry(e.target.value)}
        />
        <br />
        <br />
        <Input
          placeholder="Password"
          type={hidePass ? "password" : "text"}
          value={password}
          name="password"
          style={{
            borderRadius: 5,
            backgroundColor: "#D7F5FF",
            color: "rgba(0, 98, 131, 0.45)",
          }}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
          size="large"
          suffix={
            hidePass ? (
              <EyeOutlined onClick={() => setHidePass(!hidePass)} />
            ) : (
              <EyeInvisibleOutlined onClick={() => setHidePass(!hidePass)} />
            )
          }
        />
        <h4 style={{ textAlign: "left", margin: 0 }}>
          Your password is: {password && passwordStrength(password).value}
        </h4>
        <br />
        <Input
          placeholder="Confirm password"
          type={hidePass ? "password" : "text"}
          value={confirmPass}
          name="confirmpass"
          style={{
            borderRadius: 5,
            backgroundColor: "#D7F5FF",
            color: "rgba(0, 98, 131, 0.45)",
          }}
          onChange={(e) => setConfirmPass(e.target.value)}
          size="large"
          suffix={
            hidePass ? (
              <EyeOutlined onClick={() => setHidePass(!hidePass)} />
            ) : (
              <EyeInvisibleOutlined onClick={() => setHidePass(!hidePass)} />
            )
          }
        />
        <br />
        <h3
          style={{
            color: "#006283",
            textAlign: "left",
            margin: 0,
            fontSize: 13,
          }}
        >
          Your password must contain the following: Uppercase, lowercase,
          Digits, a special character and at least 8 characters long.
        </h3>
        <br />
        <br />

        <div
          style={{
            display: "flex",
            flexFlow: "column",
            alignItems: "start",
            justifyContent: "start",
          }}
        >
          <Checkbox
            defaultChecked={policyOneCheck}
            onChange={(value) => setPolicyOneCheck(value)}
            style={{ color: "#006283", textAlign: "left" }}
          >
            I agree to the <Link to="#">User Agreement</Link> and{" "}
            <Link to="#">Privacy Policy</Link> and{" "}
            <Link to="#">Acceptable Use Policy.</Link>
          </Checkbox>

          <Checkbox
            defaultChecked={policyTwoCheck}
            onChange={(value) => setPolicyTwoCheck(value)}
            style={{ color: "#006283", textAlign: "left" }}
          >
            I agree to the <Link to="#">Cookie Policy</Link>,{" "}
            <Link to="#">E-Sign Consent</Link>, and{" "}
            <Link to="#">Risk Factors.</Link>
          </Checkbox>
        </div>

        <br />
        <br />
        <Button
          type="primary"
          size="large"
          block
          loading={
            transition.state === "submitting" || transition.state === "loading"
          }
          style={{
            backgroundColor: "#3F47CC",
            color: "white",
            borderRadius: 5,
          }}
          onClick={async () => {
            if (password && passwordStrength(password).value !== "Strong") {
              return message.error(
                "Your password is not strong enough! Choose another one"
              );
            }

            if (!policyOneCheck && !policyTwoCheck) {
              return message.error(
                "You have to agree to our policies to proceed!",
                3
              );
            }

            if (password.length < 8 || confirmPass.length < 8) {
              return message.error(
                "Your password must be at least 8 characters"
              );
            }
            submit(registerFormRef.current, { replace: false });
          }}
        >
          SIGN UP
        </Button>
      </Form>
      <br />
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <span style={{ color: "#006283" }}>Already have an account?</span>
        <Link
          style={{ color: "#3F47CC" }}
          to="?view=login"
          // onClick={(e) => {
          //   e.preventDefault();
          //   setHasAccount(!hasAccount);
          // }}
        >
          LOG IN
        </Link>
      </div>
      <br />
      <br />
      <span style={{ color: "#006283" }}>
        © 2022 WrappedCBDC Powered by Convexity
      </span>
    </Card>
  );
};
