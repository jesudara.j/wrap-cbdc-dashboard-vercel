import { EyeInvisibleOutlined, EyeOutlined } from "@ant-design/icons";
import { Button, Card, Col, Input, message as MessageDrop, Row } from "antd";
import { useRef, useState } from "react";
import { Form, Link, redirect, useActionData, useLoaderData, useNavigate, useSubmit, useTransition } from "remix";
import { userPrefs } from "~/cookies";
import { loginAdmin } from "~/utils";


export const action = async ({ request }) => {
  // await new Promise(res => setTimeout(res, 1000));
  let url = new URL(request.url);
  let action = url.searchParams.get("action");
  let after = url.searchParams.get("after");
  let body = await request.formData();
  console.log(action);
  
  // Log admin into the dashboard
  if(action === "login") {
    const cookieHeader = request.headers.get("Cookie");
    const cookie = (await userPrefs.parse(cookieHeader)) || {};
    const email = body.get("email");
    const password = body.get("password");
    const res = await loginAdmin({ email, password });
    cookie.token = res?.data?.token;
    console.log("Response", res);
    if(res.error) {
      return res;
    }

    if(typeof after !== 'undefined' && after === 'mint_token') {
      return redirect('/admin/dashboard/mint_token', {
        headers: {
          "Set-Cookie": await userPrefs.serialize(cookie),
        }
      });
    }
    return redirect('/admin/dashboard/', {
      headers: {
        "Set-Cookie": await userPrefs.serialize(cookie),
      }
    });
  }

  return { message: "No actions matched", error: true };

};

export const loader = async ({ request }) => {
  let url = new URL(request.url);
  let view = url.searchParams.get("view");
  let step = url.searchParams.get("step");
  let resetString = url.searchParams.get("resetstring");
  let id = url.searchParams.get("id");
  let status = url.searchParams.get("status");
  let msg = url.searchParams.get("msg");
  let after = url.searchParams.get("after");

  return {view, status, msg, step, resetString, id, after};
}


export default function AdminValidation() {
  const actionData = useActionData();
  const {view, status, msg, step, resetString, id, after} = useLoaderData();

  // const [hasAccount, setHasAccount] = useState(true);
  React.useEffect(() => {

    if(actionData?.error) MessageDrop.error(actionData?.message);
    return () => {}
  }, [actionData])


  return (
    <Row gutter={[16, 8]} style={{ width: "100%", minHeight: "100vh", margin: 0, paddingTop: 50, paddingBottom: 50, backgroundColor: "#3F47CC", backgroundImage: "url('/auth_img.png')", backgroundPosition: "115% -25%", backgroundSize: "40%", backgroundRepeat: "no-repeat" }}>
      
      <Col
        xxs={{ span: 0 }}
        xs={{ span: 0 }}
        sm={{ span: 1 }}
        lg={{ span: 2 }}
      ></Col>
      <Col
        xxs={{ span: 24 }}
        xs={{ span: 24 }}
        sm={{ span: 11 }}
        lg={{ span: 10 }}
        style={{display: "flex", alignItems: "center", justifyContent: "center", borderRight: "1px solid white", padding: "0 30px"}}
      >
       {!view && <LoginView after={after} />}
        {view === 'login' && (
          <LoginView
            after={after}
          />
        )}
        {
          view === "reset" && step === "1" && (
            <PasswordResetRequest />
          )
        }
        {
          view === "reset" && step === "2" && (
            <PasswordReset id={id} resetString={resetString} />
          )
        }
      </Col>
      <Col
        xxs={{ span: 24 }}
        xs={{ span: 24 }}
        sm={{ span: 11 }}
        lg={{ span: 10 }}
        style={{display: "flex", flexFlow: "column", alignItems: "center", justifyContent: "end"}}
      >
        <h1 style={{margrin: 0, color: "white", fontFamily: "Poppins-Bold", fontSize: "4rem", fontWeight: "bolder"}}>
        <img src="/logo_edited.png" width={70} height={70} />
          WRAP CBDC
        </h1>
      </Col>
      <Col
        xxs={{ span: 0 }}
        xs={{ span: 0 }}
        sm={{ span: 1 }}
        lg={{ span: 2 }}
      ></Col>
    </Row>
  );
}

export function ErrorBoundary({ error }) {
  console.error(error);
  return (
    <div style={{ border: "1ppx solid red" }}>{error}</div>
  )
}

// Login comp
const LoginView = ({ hasAccount, setHasAccount, after }) => {
  const [hidePass, setHidePass] = useState(true);
  const [emailVal, setEmailVal] = useState();
  const [passwordVal, setPasswordVal] = useState();
  const [loading, setLoading] = useState(false);
  const submit = useSubmit();
  const loginFormRef = useRef();
  const transition = useTransition();
  return (
    <Card
      // hoverable
      // extra={<img src="/icon-512x512.png" width={40} height={40} />}
      style={{borderRadius: 10, textAlign: "center", width: "100%"}}
      bodyStyle={{
        paddingLeft: "15%",
        paddingRight: "15%",
        paddingTop: "13%",
        paddingBottom: "13%",
      }}
    >
      <img src="/primary-nav-icon.png" width={60} height={50} />
      <h1 style={{textTransform: "uppercase", margin: 0, color: "#3F47CC", fontSize: 21}}>LOGIN TO ADMIN DASHBOARD</h1>
      <span>Use your given admin account details to sign in.</span>
      <br/>
      <br/>
      <Form action={`/admin/?index&view=login&action=login${after? `&after=${after}` : ''}`} method="POST" ref={loginFormRef}>
        <Input
          placeholder="Enter email"
          type="email"
          size="large"
          name="email"
          value={emailVal}
          style={{borderRadius: 5, backgroundColor: "#D7F5FF", color: "rgba(0, 98, 131, 0.45)"}}
          onChange={(e) => setEmailVal(e.target.value)}
        />
        <br />
        <br />
        <Input
          placeholder="Enter password"
          type={hidePass ? "password" : "text"}
          size="large"
          name="password"
          value={passwordVal}
          style={{borderRadius: 5, backgroundColor: "#D7F5FF", color: "rgba(0, 98, 131, 0.45)"}}
          onChange={(e) => setPasswordVal(e.target.value)}
          suffix={
            hidePass ? (
              <EyeOutlined onClick={() => setHidePass(!hidePass)} style={{color: "#3F47CC"}} />
            ) : (
              <EyeInvisibleOutlined onClick={() => setHidePass(!hidePass)} style={{color: "#3F47CC"}} />
            )
          }
        />
        <br/>
        <br/>
        <div style={{display: "flex", alignItems: "center", justifyContent: "space-between"}}>
          <span style={{color: "#006283"}}>Forgot Password?</span>
          <Link to="/admin/?view=reset&step=1" style={{ color: "#3F47CC" }}>Reset it here</Link>
        </div>
      </Form>
        <br />
        <br />
        <Button
          type="link"
          size="large"
          block
          style={{
            backgroundColor: "#3F47CC",
            color: "white",
            borderRadius: 5
          }}
          loading={transition.state === 'submitting' || transition.state === 'loading'}
          onClick={async () => {
            submit(loginFormRef.current, { replace: false });
          }}
        >
          SIGN IN
        </Button>
      
      <br />
      <br />
      <span style={{color: "#006283"}}>© 2022 WrappedCBDC Powered by Convexity</span><br/>
      {/* <div style={{display: "flex", alignItems: "center", justifyContent: "space-between"}}>
        <span style={{color: "#006283"}}>
          Need an Account?
        </span>
        <Link
          to="?view=signup"
          style={{ color: "#3F47CC" }}
          // onClick={(e) => {
          //   e.preventDefault();
          //   setHasAccount(!hasAccount);
          // }}
        >
          SIGN UP
        </Link>
      </div> */}
    </Card>
  );
};

const PasswordResetRequest = ({ hasAccount, setHasAccount }) => {
  const [hidePass, setHidePass] = useState(true);
  const [emailVal, setEmailVal] = useState();
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  return (
    <Card
      // hoverable
      // extra={<img src="/icon-512x512.png" width={40} height={40} />}
      style={{borderRadius: 10, textAlign: "center", width: "100%"}}
      bodyStyle={{
        paddingLeft: "15%",
        paddingRight: "15%",
        paddingTop: "13%",
        paddingBottom: "13%",
      }}
    >
      <h1 style={{textTransform: "uppercase", margin: 0, color: "#3F47CC", fontSize: 21}}>RESET ADMIN PASSWORD</h1>
      <span>Use your existing admin email/username to reset password.</span>
      <br/>
      <br/>
        <Input
          placeholder="Enter email"
          type="email"
          size="large"
          name="email"
          value={emailVal}
          style={{borderRadius: 5, backgroundColor: "#D7F5FF", color: "rgba(0, 98, 131, 0.45)"}}
          onChange={(e) => setEmailVal(e.target.value)}
        />
        <br/>
        <br/>
        <div style={{display: "flex", alignItems: "center", justifyContent: "space-between"}}>
          <span style={{color: "#006283"}}>Remembered Password?</span>
          <Link to="/admin/?view=login" style={{ color: "#3F47CC" }}>Sign in</Link>
        </div>
        <br />
        <br />
        <Button
          type="link"
          size="large"
          block
          style={{
            backgroundColor: "#3F47CC",
            color: "white",
            borderRadius: 5
          }}
          loading={loading}
          onClick={async () => {
            // Call reset request API
            setLoading(true);

            const resp = await requestPasswordReset(emailVal);
            if(resp.error) {
              setLoading(false);
              return MessageDrop.error(resp.message);
            }

            MessageDrop.success(resp.message);

            setLoading(false);
          }}
        >
          SEND RESET LINK
        </Button>
      
      <br />
      <br />
      {/* <div style={{display: "flex", alignItems: "center", justifyContent: "space-between"}}>
        <span style={{color: "#006283"}}>
          Need an Account?
        </span>
        <Link
          to="?view=signup"
          style={{ color: "#3F47CC" }}
        >
          SIGN UP
        </Link>
      </div> */}
      <br/>
      <br/>
      <span style={{color: "#006283"}}>© 2022 WrappedCBDC Powered by Convexity</span><br/>
    </Card>
  );
};


const PasswordReset = ({ hasAccount, setHasAccount, resetString, id }) => {
  const [hidePass, setHidePass] = useState(true);
  const [newPassword, setNewPassword] = useState();
  const [confirmNewPassword, setConfirmNewPassword] = useState();
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  return (
    <Card
      // hoverable
      // extra={<img src="/icon-512x512.png" width={40} height={40} />}
      style={{borderRadius: 10, textAlign: "center",}}
      bodyStyle={{
        paddingLeft: "15%",
        paddingRight: "15%",
        paddingTop: "13%",
        paddingBottom: "13%",
      }}
    >
      <h1 style={{textTransform: "uppercase", margin: 0, color: "#3F47CC", fontSize: 21}}>RESET ADMIN PASSWORD</h1>
      <span>Enter a password to restore access to your admin account, try not to forget this one .</span>
      <br/>
      <br/>
      <Input
          placeholder="Enter new password"
          type={hidePass ? "password" : "text"}
          size="large"
          name="password"
          value={newPassword}
          style={{borderRadius: 5, backgroundColor: "#D7F5FF", color: "rgba(0, 98, 131, 0.45)"}}
          onChange={(e) => setNewPassword(e.target.value)}
          suffix={
            hidePass ? (
              <EyeOutlined onClick={() => setHidePass(!hidePass)} style={{color: "#3F47CC"}} />
            ) : (
              <EyeInvisibleOutlined onClick={() => setHidePass(!hidePass)} style={{color: "#3F47CC"}} />
            )
          }
        />
        <br/>
        <br/>
        <Input
          placeholder="Confirm new password"
          type={hidePass ? "password" : "text"}
          size="large"
          name="password"
          value={confirmNewPassword}
          style={{borderRadius: 5, backgroundColor: "#D7F5FF", color: "rgba(0, 98, 131, 0.45)"}}
          onChange={(e) => setConfirmNewPassword(e.target.value)}
          suffix={
            hidePass ? (
              <EyeOutlined onClick={() => setHidePass(!hidePass)} style={{color: "#3F47CC"}} />
            ) : (
              <EyeInvisibleOutlined onClick={() => setHidePass(!hidePass)} style={{color: "#3F47CC"}} />
            )
          }
        />
        <br/>
        <br/>
        {/* <div style={{display: "flex", alignItems: "center", justifyContent: "space-between"}}>
          <span style={{color: "#006283"}}>Remembered Password?</span>
          <Link to="/?view=login" style={{ color: "#3F47CC" }}>Sign in</Link>
        </div>
        <br />
        <br /> */}
        <Button
          type="link"
          size="large"
          block
          style={{
            backgroundColor: "#3F47CC",
            color: "white",
            borderRadius: 5
          }}
          loading={loading}
          onClick={async () => {
            // Call reset API
            setLoading(true);

            if(newPassword !== confirmNewPassword) {
              setLoading(false);
              return MessageDrop.warn("Your passwords do not match");
            }

            const resp = await resetPassword(id, resetString, newPassword);

            if(resp.error) {
              setLoading(false);
              return MessageDrop.error(resp.message);
            }

            MessageDrop.success(resp.message);
            navigate('/?view=login');

            setLoading(false);
          }}
        >
          RESET
        </Button>
      
      <br />
      <br />
      <span style={{color: "#006283"}}>© 2022 WrappedCBDC Powered by Convexity</span><br/>
      {/* <div style={{display: "flex", alignItems: "center", justifyContent: "space-between"}}>
        <span style={{color: "#006283"}}>
          Need an Account?
        </span>
        <Link
          to="?view=signup"
          style={{ color: "#3F47CC" }}
          // onClick={(e) => {
          //   e.preventDefault();
          //   setHasAccount(!hasAccount);
          // }}
        >
          SIGN UP
        </Link>
      </div> */}
    </Card>
  );
};
