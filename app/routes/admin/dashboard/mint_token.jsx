import { Button, Card, Col, DatePicker, message, Popconfirm, Row, Table } from "antd";
import { useLoaderData, useNavigate, useOutletContext } from "remix";
import { userPrefs } from "~/cookies";
import { approveTransactionByAdmin, changeENGNTransactionStatusByAdmin, declineTransactionByAdmin, getTransactions } from "~/utils";

const { RangePicker } = DatePicker;


const columns = [
  {
    title: 'Transaction Type',
    dataIndex: 'type',
    key: 'type',
    render: text =><h4 style={{display: 'inline-block', marginLeft: 10}}><img src="/token_logo.png" width={20} height={20} /> {text}</h4>
  },
  {
    title: 'Total(cNGN)',
    dataIndex: 'amount',
    key: 'amount',
  },
  {
    title: 'Date',
    dataIndex: 'date',
    key: 'date',
  },
  {
    title: 'User',
    dataIndex: 'user',
    key: 'user',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
  },
  // {
  //   title: 'User',
  //   dataIndex: 'user',
  //   key: 'user',
  // },
  {
    title: 'Trx ID',
    dataIndex: 'trx_id',
    key: 'trx_id',
  },
  {
    title: 'Quick Action',
    dataIndex: 'action',
    key: 'action',
    width: "30%",
    render: (text, record, index) => <MintInteractionButtons record={record} index={index} />
  },
];

// Load data for the page
export const loader = async ({ request }) => {
  const cookieHeader = request.headers.get("Cookie");
  const cookie = (await userPrefs.parse(cookieHeader)) || {};

  const { data } = await getTransactions(cookie.token);
  return { data, token: cookie?.token };
};

export default function MintToken() {

  // const [loading, setLoading] = React.useState(true);
  const { user, balances } = useOutletContext();
  // const [chartData, setChartData] = React.useState([]);
  const [transactionData, setTransactionData] = React.useState([]);
  const loader = useLoaderData();
  console.log(user, loader?.data, "User data");

  React.useEffect(() => {
    // Mutate the user transaction data into the chart data
    if(loader?.error !== true) {
      const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
      
      setTransactionData(loader?.data?.filter(val => val['trx_type'] === 'fiat_deposit' || val['trx_type'] === 'enaira_deposit' || val['trx_type'] === 'enaira_redeem').filter(val => val['status'] === 'pending').map((val, i) => {
        return {
          key: i.toString(),
          type: val['trx_type'] === 'crypto_redeem' && val['asset_symbol'] === 'CNGN'? 'CNGN Withdrawal' : val['trx_type'] === 'fiat_redeem' && val['asset_symbol'] === 'NGN'? 'Fiat Redeem' : val['trx_type'] === 'enaira_redeem' && val['asset_symbol'] === 'ENGN'? 'ENaira Redeem' : val['trx_type'] === 'fiat_deposit' && val['asset_symbol'] === 'NGN'? 'Fiat Deposit' : val['trx_type'] === 'crypto_deposit' && val['asset_symbol'] === 'CNGN'? 'CNGN Deposit' : 'ENaira Deposit',
          amount: Number(val['amount']).toFixed(1).replace(/\d(?=(\d{3})+\.)/g, "$&,").toString(),
          date: `${new Date(val['createdAt']).toLocaleString("en-US", {timeZone: timezone}).split(",")[0]}`,
          status: val['status'] || 'Processed',
          // user: val?.user?.email,
          user: val['user']?.email,
          trx_id: val['trx_ref'],
        }
      }));
    }

  }, [loader]);

  return (
    <Row gutter={[16, 16]} style={{width: "100%", marginLeft: 0, marginRight: 0, padding: 0}}>
      <Col xs={{span: 24}} lg={{span: 24}} style={{height: "100%"}}>
        <Card hoverable style={{borderRadius: 20, background: "rgba(253, 253, 255, 0.7)",
          background: "-webkit-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
          background: "-moz-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
          background: "linear-gradient(to bottom right, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)"}}>
          <h1 style={{display: "inline-block", margin: 0}}>Mint Approval</h1>
          <Table bordered={false} columns={columns} dataSource={transactionData} style={{background: "transparent"}} onRow={(record, rowIndex) => {
            return {
              onClick: event => {
                // if (event.target.nodeName === "SPAN" || event.target.nodeName === "BUTTON") {
                  
                //   message.info("Feature will be available soon");
                //   console.log(event.target.nodeName, record, rowIndex, event.target, "Event of the table click");
                //   // Also show modal and confirm flag of transactions
                // }
              }
            };
          }} />
        </Card>
      </Col>
    </Row>
  );
}

function MintInteractionButtons({ record, index }) {

  const loader = useLoaderData();
  const navigate = useNavigate();
  const { user, balances } = useOutletContext();
  const [isDeclineLoading, setIsDeclineLoading] = React.useState(false);
  const [isApproveLoading, setIsApproveLoading] = React.useState(false);
  const [isDeclineConfirmVisible, setIsDeclineConfirmVisible] = React.useState(false);
  const [isApproveConfirmVisible, setIsApproveConfirmVisible] = React.useState(false);
  // For enaira redeem
  const [isRejectLoading, setIsRejectLoading] = React.useState(false);
  const [isFullfilLoading, setIsFullfilLoading] = React.useState(false);
  const [isRejectConfirmVisible, setIsRejectConfirmVisible] = React.useState(false);
  const [isFullfilConfirmVisible, setIsFullfilConfirmVisible] = React.useState(false);

  // Callback for decline confirm and cancel
  const declineBtnConfirm = React.useCallback(async () => {
    // Call decline API to decline transaction
    setIsDeclineConfirmVisible(false);
    const filteredData = loader?.data?.filter(filter => filter['trx_ref'] === record['trx_id'])[0];
    console.log(filteredData, "Filtered Data");
    const resp = await declineTransactionByAdmin(loader?.token, filteredData?.trx_ref, filteredData?.user?.id, user?.id);
    if(resp?.error) {
      setIsDeclineLoading(false);
      navigate("/admin/dashboard/mint_token");
      return message.error(resp?.message);
    }
    setIsDeclineLoading(false);
    navigate("/admin/dashboard/mint_token");
    message.success(resp?.message);
  }, [isDeclineConfirmVisible]);

  const declineBtnCancel = React.useCallback(() => {
    setIsDeclineConfirmVisible(false);
    setIsDeclineLoading(false);
  }, [isDeclineConfirmVisible]);

  // Callback for approve confirm and cancel
  const approveBtnConfirm = React.useCallback(async () => {
    // Call approve API to decline transaction
    setIsApproveConfirmVisible(false);
    console.log(loader?.data, "Data");
    const filteredData = await Promise.all(loader?.data?.filter(filter => filter['trx_ref'] === record['trx_id'])).then(result => result[0]);
    console.log(filteredData);
    const resp = await approveTransactionByAdmin(loader?.token, filteredData?.trx_ref, filteredData?.user?.id, user?.id);
    if(resp?.error) {
      setIsApproveLoading(false);
      navigate("/admin/dashboard/mint_token");
      return message.error(resp?.message);
    }
    setIsApproveLoading(false);
    navigate("/admin/dashboard/mint_token");
    message.success(resp?.message);
  }, [isApproveConfirmVisible]);

  const approveBtnCancel = React.useCallback(() => {
    setIsApproveConfirmVisible(false);
    setIsApproveLoading(false);
  }, [isApproveConfirmVisible]);

  /* 
  * IF transaction type is enaira_redeem then carry out a different operation: changeENGNTransactionStatusByAdmin 
  */
 
  // Callback for fullfil confirm and cancel
  const fullfilBtnConfirm = React.useCallback(async () => {
    // Call changeTrxStatus API to decline transaction
    setIsFullfilConfirmVisible(false);
    console.log(loader?.data, "Data");
    const filteredData = await Promise.all(loader?.data?.filter(filter => filter['trx_ref'] === record['trx_id'])).then(result => result[0]);
    console.log(filteredData);
    const resp = await changeENGNTransactionStatusByAdmin(loader?.token, filteredData?.trx_ref, filteredData?.user?.id, user?.id, 'completed');
    if(resp?.error) {
      setIsFullfilLoading(false);
      navigate("/admin/dashboard/mint_token");
      return message.error(resp?.message);
    }
    setIsFullfilLoading(false);
    navigate("/admin/dashboard/mint_token");
    message.success(resp?.message);
  }, [isFullfilConfirmVisible]);

  const fullfilBtnCancel = React.useCallback(() => {
    setIsFullfilConfirmVisible(false);
    setIsFullfilLoading(false);
  }, [isFullfilConfirmVisible]);

  // Callback for reject status and cancel
  const rejectBtnConfirm = React.useCallback(async () => {
    // Call reject API to reject transaction
    setIsRejectConfirmVisible(false);
    const filteredData = loader?.data?.filter(filter => filter['trx_ref'] === record['trx_id'])[0];
    console.log(filteredData, "Filtered Data");
    const resp = await changeENGNTransactionStatusByAdmin(loader?.token, filteredData?.trx_ref, filteredData?.user?.id, user?.id, 'rejected');
    if(resp?.error) {
      setIsRejectLoading(false);
      navigate("/admin/dashboard/mint_token");
      return message.error(resp?.message);
    }
    setIsRejectLoading(false);
    navigate("/admin/dashboard/mint_token");
    message.success(resp?.message);
  }, [isRejectConfirmVisible]);

  const rejectBtnCancel = React.useCallback(() => {
    setIsRejectConfirmVisible(false);
    setIsRejectLoading(false);
  }, [isRejectConfirmVisible]);

  console.warn(record['type'], "Type of transaction");

  return record['type'] === 'ENaira Redeem'?  (
      <>
      <Popconfirm
        title={`Confirm rejection of transaction with ID: ${record['trx_id']}`}
        visible={isRejectConfirmVisible}
        onVisibleChange={(visible) => setIsRejectConfirmVisible(visible)}
        onConfirm={rejectBtnConfirm}
        onCancel={rejectBtnCancel}
        okText="Yes"
        cancelText="Cancel"
        placement="topRight"
      >
        <Button loading={isRejectLoading} type="link" size="middle" style={{ color: "rgba(254, 119, 120, 1)", backgroundColor: "rgba(254, 119, 120, 0.25)", border: "1.5px solid rgba(254, 119, 120, 1)", marginRight: 8 }} onClick={() => {
          // Pop confirm the action before carrying it out
          setIsRejectLoading(true);
          setIsRejectConfirmVisible(true);
        }}>REJECT</Button>
      </Popconfirm>
      <Popconfirm
        title={`Confirm fullfilment of transaction with ID: ${record['trx_id']}`}
        visible={isFullfilConfirmVisible}
        onVisibleChange={(visible) => setIsFullfilConfirmVisible(visible)}
        onConfirm={fullfilBtnConfirm}
        onCancel={fullfilBtnCancel}
        okText="Yes"
        cancelText="Cancel"
        placement="topRight"
      >
        <Button loading={isFullfilLoading} type="link" size="middle" style={{ color: "rgba(90, 197, 175, 1)", backgroundColor: "rgba(90, 197, 175, 0.25)", border: "1.5px solid rgba(90, 197, 175, 1)" }} onClick={() => {
          // Pop confirm the action before carrying it out
          setIsFullfilLoading(true);
          setIsFullfilConfirmVisible(true);
        }}>FULLFIL</Button>
      </Popconfirm>
    </>
    ) : (
    <>
      <Popconfirm
        title={`Confirm decline of transaction \nwith ID: ${record['trx_id']}`}
        visible={isDeclineConfirmVisible}
        onVisibleChange={(visible) => setIsDeclineConfirmVisible(visible)}
        onConfirm={declineBtnConfirm}
        onCancel={declineBtnCancel}
        okText="Yes"
        cancelText="Cancel"
        placement="topRight"
      >
        <Button loading={isDeclineLoading} type="link" size="middle" style={{ color: "rgba(254, 119, 120, 1)", backgroundColor: "rgba(254, 119, 120, 0.25)", border: "1.5px solid rgba(254, 119, 120, 1)", marginRight: 8 }} onClick={() => {
          // Pop confirm the action before carrying it out
          setIsDeclineLoading(true);
          setIsDeclineConfirmVisible(true);
        }}>DECLINE</Button>
      </Popconfirm>
      <Popconfirm
        title={`Confirm approval of transaction \nwith ID: ${record['trx_id']}`}
        visible={isApproveConfirmVisible}
        onVisibleChange={(visible) => setIsApproveConfirmVisible(visible)}
        onConfirm={approveBtnConfirm}
        onCancel={approveBtnCancel}
        okText="Yes"
        cancelText="Cancel"
        placement="topRight"
      >
        <Button loading={isApproveLoading} type="link" size="middle" style={{ color: "rgba(90, 197, 175, 1)", backgroundColor: "rgba(90, 197, 175, 0.25)", border: "1.5px solid rgba(90, 197, 175, 1)" }} onClick={() => {
          // Pop confirm the action before carrying it out
          setIsApproveLoading(true);
          setIsApproveConfirmVisible(true);
        }}>APPROVE</Button>
      </Popconfirm>
    </>
  );
}