import { Button, Card, Col, DatePicker, message, Row, Table } from "antd";
import { useLoaderData, useOutletContext } from "remix";
import { userPrefs } from "~/cookies";
import { getTransactions } from "~/utils";

const { RangePicker } = DatePicker;
const dataSource = [
  {
    key: '1',
    type: 'Crypto Deposit',
    amount: 10000000,
    date: '08/24/2022',
  },
  {
    key: '2',
    type: 'Fiat Deposit',
    amount: 40000000,
    date: '08/24/2022',
  },
  {
    key: '3',
    type: 'Fiat Redeem',
    amount: 120000000,
    date: '08/24/2022',
  },
  {
    key: '4',
    type: 'cNGN Redeem',
    amount: 75000000,
    date: '08/24/2022',
  },
  
];


const spotDataSource = [
  {
    key: '1',
    coin: 'cNGN',
    total: 10000000,
    available: 8000000,
  },
];

const columns = [
  {
    title: 'Transaction Type',
    dataIndex: 'type',
    key: 'type',
    render: text =><h4 style={{display: 'inline-block', marginLeft: 10}}><img src="/token_logo.png" width={20} height={20} /> {text}</h4>
  },
  {
    title: 'Total(cNGN)',
    dataIndex: 'amount',
    key: 'amount',
  },
  {
    title: 'Date',
    dataIndex: 'date',
    key: 'date',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
  },
  {
    title: 'User',
    dataIndex: 'user',
    key: 'user',
  },
  {
    title: 'Details',
    dataIndex: 'details',
    key: 'details',
  },
  {
    title: 'Trx ID',
    dataIndex: 'trx_id',
    key: 'trx_id',
  },
  {
    title: 'Quick Action',
    dataIndex: 'action',
    key: 'action',
    render: () => <Button type="danger" size="middle">FLAG</Button>
  },
];

// Load data for the page
export const loader = async ({ request }) => {
  const cookieHeader = request.headers.get("Cookie");
  const cookie = (await userPrefs.parse(cookieHeader)) || {};
  return await getTransactions(cookie.token);
};

export default function History() {

  // const [loading, setLoading] = React.useState(true);
  const { user, balances } = useOutletContext();
  // const [chartData, setChartData] = React.useState([]);
  const [transactionData, setTransactionData] = React.useState([]);
  const loader = useLoaderData();
  console.log(user, "User data");

  React.useEffect(() => {
    // Mutate the user transaction data into the chart data
    if(loader?.error !== true) {
      const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
      
      setTransactionData(loader?.data?.map((val, i) => {
        return {
          key: i.toString(),
          type: val['trx_type'] === 'crypto_redeem' && val['asset_symbol'] === 'CNGN'? 'CNGN Withdrawal' : val['trx_type'] === 'fiat_redeem' && val['asset_symbol'] === 'NGN'? 'Fiat Redeem' : val['trx_type'] === 'enaira_redeem' && val['asset_symbol'] === 'ENGN'? 'ENaira Redeem' : val['trx_type'] === 'fiat_deposit' && val['asset_symbol'] === 'NGN'? 'Fiat Deposit' : val['trx_type'] === 'crypto_deposit' && val['asset_symbol'] === 'CNGN'? 'CNGN Deposit' : 'ENaira Deposit',
          amount: Number(val['amount']).toFixed(1).replace(/\d(?=(\d{3})+\.)/g, "$&,").toString(),
          date: `${new Date(val['createdAt']).toLocaleString("en-US", {timeZone: timezone}).split(",")[0]}`,
          status: val['status'] || 'Processed',
          user: val?.user?.email,
          details: val['description'],
          trx_id: val['trx_ref'],
        }
      }));
    }

  }, [loader]);

  return (
    <Row gutter={[16, 16]} style={{width: "100%", marginLeft: 0, marginRight: 0, padding: 0}}>
      <Col xs={{span: 24}} lg={{span: 24}} style={{height: "100%"}}>
        <Card hoverable style={{borderRadius: 20, background: "rgba(253, 253, 255, 0.7)",
          background: "-webkit-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
          background: "-moz-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
          background: "linear-gradient(to bottom right, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)"}}>
          <h1 style={{display: "inline-block", margin: 0}}>Transactions</h1>
          <Table bordered={false} columns={columns} dataSource={transactionData} style={{background: "transparent"}} onRow={(record, rowIndex) => {
            return {
              onClick: event => {
                if (event.target.nodeName === "SPAN" || event.target.nodeName === "BUTTON") {
                  
                  message.info("Feature will be available soon");
                  console.log(event.target.nodeName, record, rowIndex, "Event of the table click");
                  // Also show modal and confirm flag of transactions
                }
              } // Handle button click
            };
          }} />
        </Card>
      </Col>
    </Row>
  );
}