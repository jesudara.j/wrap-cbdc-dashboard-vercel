import {
  ArrowLeftOutlined,
  LoadingOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {
  Button,
  Card,
  Col,
  DatePicker,
  Divider,
  Input,
  List,
  message,
  Row,
  Space,
  Table,
  Tag,
} from "antd";
import { useQuery } from "react-query";
import { useLoaderData, useNavigate, useOutletContext } from "remix";
import { userPrefs } from "~/cookies";
import {
  approveKYCDocs,
  getAllUsers,
  getKYCDocs,
  rejectKYCData,
} from "~/utils";

const { RangePicker } = DatePicker;

const columns = [
  {
    title: "User Email",
    dataIndex: "email",
    key: "email",
    // render: text =><h4 style={{display: 'inline-block', marginLeft: 10}}><img src="/token_logo.png" width={20} height={20} /> {text}</h4>
  },
  {
    title: "Full Name",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Date Created",
    dataIndex: "createdAt",
    key: "createdAt",
  },
  {
    title: "Account Status",
    dataIndex: "status",
    key: "status",
  },
  {
    title: "Transaction Volume",
    dataIndex: "volume",
    key: "volume",
  },
  {
    title: "Action",
    dataIndex: "action",
    key: "action",
    render: () => (
      <Button type="ghost" size="middle">
        VIEW USER
      </Button>
    ),
  },
];

const userColumns = [
  {
    title: "Transaction Type",
    dataIndex: "type",
    key: "type",
    render: (text) => (
      <h4 style={{ display: "inline-block", marginLeft: 10 }}>
        <img src="/token_logo.png" width={20} height={20} /> {text}
      </h4>
    ),
  },
  {
    title: "Total(cNGN)",
    dataIndex: "amount",
    key: "amount",
  },
  {
    title: "Date",
    dataIndex: "date",
    key: "date",
  },
  {
    title: "Status",
    dataIndex: "status",
    key: "status",
  },
  {
    title: "Details",
    dataIndex: "details",
    key: "details",
  },
  {
    title: "Trx ID",
    dataIndex: "trx_id",
    key: "trx_id",
  },
  {
    title: "Quick Action",
    dataIndex: "action",
    key: "action",
    render: () => (
      <>
        <Button type="danger" size="medium">
          FLAG
        </Button>
        <Button type="primary" size="medium">
          UNFLAG
        </Button>
      </>
    ),
  },
];

// Load data for the page
export const loader = async ({ request }) => {
  const cookieHeader = request.headers.get("Cookie");
  const cookie = (await userPrefs.parse(cookieHeader)) || {};
  // get params
  const url = new URL(request.url);
  const action = url.searchParams.get("action");
  const userMail = url.searchParams.get("userMail");
  const { data, error, status } = await getAllUsers(cookie.token);
  return { data, error, status, action, userMail, token: cookie?.token };
};

export default function UserManagement() {
  // const [loading, setLoading] = React.useState(true);
  const { user, balances } = useOutletContext();
  // const [chartData, setChartData] = React.useState([]);
  const [usersList, setUsersList] = React.useState([]);
  const [singleUser, setSingleUser] = React.useState();
  const [singleUserTrx, setSingleUserTrx] = React.useState([]);
  const loader = useLoaderData();
  const navigate = useNavigate();
  const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

  React.useEffect(() => {
    // Mutate the user transaction data into the chart data
    if (loader?.error !== true) {
      const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

      setUsersList(
        loader?.data
          ?.filter((val) => !val["isAdmin"])
          .map((val, i) => {
            return {
              key: i.toString(),
              email: val["email"],
              name: `${val["last_name"]} ${val["first_name"]}`,
              createdAt: `${
                new Date(val["createdAt"])
                  .toLocaleString("en-US", { timeZone: timezone })
                  .split(",")[0]
              }`,
              status: val["status"],
              volume: val?.transactions?.length,
            };
          })
      );

      if (loader?.action && loader?.userMail) {
        const filterUserSingle = loader?.data?.filter(
          (filtered) => filtered["email"] === loader?.userMail
        )[0];
        setSingleUser(filterUserSingle);
        setSingleUserTrx(
          filterUserSingle?.transactions?.map((val, i) => {
            return {
              key: i.toString(),
              type:
                val["trx_type"] === "crypto_redeem" &&
                val["asset_symbol"] === "CNGN"
                  ? "CNGN Withdrawal"
                  : val["trx_type"] === "fiat_redeem" &&
                    val["asset_symbol"] === "NGN"
                  ? "Fiat Redeem"
                  : val["trx_type"] === "enaira_redeem" &&
                    val["asset_symbol"] === "ENGN"
                  ? "ENaira Redeem"
                  : val["trx_type"] === "fiat_deposit" &&
                    val["asset_symbol"] === "NGN"
                  ? "Fiat Deposit"
                  : val["trx_type"] === "crypto_deposit" &&
                    val["asset_symbol"] === "CNGN"
                  ? "CNGN Deposit"
                  : "ENaira Deposit",
              amount: Number(val["amount"])
                .toFixed(1)
                .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                .toString(),
              date: `${
                new Date(val["createdAt"])
                  .toLocaleString("en-US", { timeZone: timezone })
                  .split(",")[0]
              }`,
              status: val["status"] || "Processed",
              details: val["description"],
              trx_id: val["trx_ref"],
            };
          })
        );
      }
    }
  }, [loader]);

  return (
    <Row
      gutter={[16, 16]}
      style={{ width: "100%", marginLeft: 0, marginRight: 0, padding: 0 }}
    >
      <Col xs={{ span: 24 }} lg={{ span: 24 }} style={{ height: "100%" }}>
        {!loader?.action && (
          <Card
            hoverable
            style={{
              borderRadius: 20,
              background: "rgba(253, 253, 255, 0.7)",
              background:
                "-webkit-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
              background:
                "-moz-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
              background:
                "linear-gradient(to bottom right, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
            }}
          >
            <h1 style={{ display: "inline-block", margin: 0 }}>Users</h1>
            <Table
              bordered={false}
              columns={columns}
              dataSource={usersList}
              style={{ background: "transparent" }}
              onRow={(record, rowIndex) => {
                return {
                  onClick: (event) => {
                    if (
                      event.target.nodeName === "SPAN" ||
                      event.target.nodeName === "BUTTON"
                    ) {
                      console.log(
                        event.target.nodeName,
                        record,
                        rowIndex,
                        "Event of the table click"
                      );
                      const filterUserSingle = loader?.data?.filter(
                        (filtered) => filtered["email"] === record?.email
                      )[0];
                      setSingleUser(filterUserSingle);
                      setSingleUserTrx(
                        filterUserSingle?.transactions?.map((val, i) => {
                          return {
                            key: i.toString(),
                            type:
                              val["trx_type"] === "crypto_redeem" &&
                              val["asset_symbol"] === "CNGN"
                                ? "CNGN Withdrawal"
                                : val["trx_type"] === "fiat_redeem" &&
                                  val["asset_symbol"] === "NGN"
                                ? "Fiat Redeem"
                                : val["trx_type"] === "enaira_redeem" &&
                                  val["asset_symbol"] === "ENGN"
                                ? "ENaira Redeem"
                                : val["trx_type"] === "fiat_deposit" &&
                                  val["asset_symbol"] === "NGN"
                                ? "Fiat Deposit"
                                : val["trx_type"] === "crypto_deposit" &&
                                  val["asset_symbol"] === "CNGN"
                                ? "CNGN Deposit"
                                : "ENaira Deposit",
                            amount: Number(val["amount"])
                              .toFixed(1)
                              .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                              .toString(),
                            date: `${
                              new Date(val["createdAt"])
                                .toLocaleString("en-US", { timeZone: timezone })
                                .split(",")[0]
                            }`,
                            status: val["status"] || "Processed",
                            details: val["description"],
                            trx_id: val["trx_ref"],
                          };
                        })
                      );
                      navigate(
                        `/admin/dashboard/user_management?action=view_user&userMail=${record?.email}`
                      );
                      // Also show modal and confirm flag of transactions
                    }
                  }, // Handle button click
                };
              }}
            />
          </Card>
        )}
        {loader?.action && loader?.action === "view_user" && (
          <UserView singleUser={singleUser} singleUserTrx={singleUserTrx} />
        )}
      </Col>
    </Row>
  );
}

function UserView({ singleUser, singleUserTrx }) {
  const loader = useLoaderData();
  const navigate = useNavigate();

  let token = loader?.token,
    userEmail = singleUser?.email;

  const { data, status, error } = useQuery(
    ["kyc_docs", token, userEmail],
    () => getKYCDocs(token, userEmail),
    { refetchInterval: false }
  );
  console.log(data, status, error, "KYC File data");
  const [verifyLoading, setVerifyLoading] = React.useState(false);
  const [rejectLoading, setRejectLoading] = React.useState(false);
  const [docList, setDocList] = React.useState([]);
  const [rejectDocsList, setRejectedDocsList] = React.useState([]);
  const [rejectionText, setRejectionText] = React.useState();

  React.useEffect(() => {
    setRejectionText(
      `The following documents have been marked as not valid by an admin on the Wrap CBDC platform:\n ${rejectDocsList.map(
        (val) => `${val}\n`
      )}Please endeavour to login and re-upload the missing files.`
    );
  }, [rejectDocsList]);
  const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

  return (
    <Card
      extra={
        <Button
          type="link"
          onClick={() => navigate(`/admin/dashboard/user_management`)}
          icon={<ArrowLeftOutlined />}
        >
          BACK
        </Button>
      }
      hoverable
      style={{
        borderRadius: 20,
        background: "rgba(253, 253, 255, 0.7)",
        background:
          "-webkit-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
        background:
          "-moz-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
        background:
          "linear-gradient(to bottom right, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
      }}
    >
      <Row
        gutter={[8, 8]}
        style={{ width: "100%", marginLeft: 0, marginright: 0, padding: 0 }}
      >
        <Col xs={{ span: 24 }} lg={{ span: 12 }}>
          <Row style={{ borderRight: "1px solid gray" }}>
            <Col
              span={18}
              style={{
                display: "flex",
                flexFlow: "column",
                alignItems: "start",
                justifyContent: "center",
              }}
            >
              <h1>User Profile</h1>
              <span>
                Account Status:{" "}
                <Tag
                  color={
                    singleUser?.status.toLowerCase() !== "active"
                      ? "error"
                      : "success"
                  }
                >
                  {singleUser?.status}
                </Tag>
              </span>
            </Col>
            <Col
              span={6}
              style={{
                display: "flex",
                flexFlow: "column",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <UserOutlined style={{ fontSize: 34 }} />
            </Col>
            <Col span={24}>
              <Divider />
              <Space direction="vertical" size="middle">
                <span>
                  Full Name: {singleUser?.last_name} {singleUser?.first_name}
                </span>
                <span>Email: {singleUser?.email}</span>
                <span>Phone Number: {singleUser?.phone}</span>
                <span>Country of residence: {singleUser?.country}</span>
                <span>
                  Date Created:{" "}
                  {
                    new Date(singleUser?.createdAt)
                      .toLocaleString("en-US", { timeZone: timezone })
                      .split(",")[0]
                  }
                </span>
                <span>
                  Total Transaction: {singleUser?.transactions.length}
                </span>
                <span>
                  Total Transaction Volume:{" "}
                  {Number(
                    singleUser?.transactions
                      ?.map((filtered) => filtered["amount"])
                      .reduce((a, b) => a + b, 0)
                  )
                    .toFixed(1)
                    .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                    .toString()}
                </span>
              </Space>
            </Col>
          </Row>
        </Col>
        <Col xs={{ span: 24 }} lg={{ span: 12 }} style={{ paddingLeft: 15 }}>
          <Row gutter={[8, 16]}>
            <Col span={24}>
              <h1>Quick Action</h1>
              <span>Account Status:</span> <br />
              <Button
                size="middle"
                type="link"
                style={{
                  color: "rgba(0, 255, 203, 1)",
                  backgroundColor: "rgba(0, 255, 203, 0.05)",
                  border: "1px solid rgba(0, 255, 203, 0.25)",
                  marginRight: 10,
                }}
              >
                ACTIVE
              </Button>
              <Button
                size="middle"
                type="link"
                style={{
                  color: "rgba(255, 138, 0, 1)",
                  backgroundColor: "rgba(255, 138, 0, 0.05)",
                  border: "1px solid rgba(255, 138, 0, 0.25)",
                  marginRight: 10,
                }}
              >
                UNDER REVIEW
              </Button>
              <Button
                size="middle"
                type="link"
                style={{
                  color: "rgba(255, 0, 0, 1)",
                  backgroundColor: "rgba(255, 0, 0, 0.05)",
                  border: "1px solid rgba(255, 0, 0, 0.25)",
                  marginRight: 10,
                }}
              >
                FREEZE
              </Button>
              <br />
              <br />
            </Col>
            <Col span={24}>
              <h1>KYC Data Review</h1>
              <span>
                Data submitted:{" "}
                <Tag
                  color={singleUser?.kyc_docs_uploaded ? "success" : "error"}
                >
                  {singleUser?.kyc_docs_uploaded ? "Yes" : "No"}
                </Tag>
              </span>
              <br />
              <span>Document review links:</span>
              <br />
              {/* Array of links to the respective docs */}
              {status === "loading" && (
                <>
                  <span>
                    <LoadingOutlined /> Getting docs...
                  </span>
                  <br />
                </>
              )}
              {data && (
                <List
                  size="small"
                  itemLayout="horizontal"
                  bordered
                  dataSource={data.data?.resources}
                  renderItem={(item) => (
                    <List.Item
                      actions={[
                        <a
                          target="_blank"
                          rel="noopener noreferrer"
                          href={item["url"]}
                        >
                          View
                        </a>,
                        <Button
                          size="small"
                          type="danger"
                          onClick={async () => {
                            setRejectedDocsList((prevList) => [
                              ...prevList,
                              item["public_id"]
                                .split("/")[2]
                                .split("-")[0]
                                .split("_")
                                .join(" ")
                                .toUpperCase(),
                            ]);
                            message.info(
                              `${item["public_id"]
                                .split("/")[2]
                                .split("-")[0]
                                .split("_")
                                .join(" ")
                                .toUpperCase()} added to rejection list`
                            );
                          }}
                          disabled={singleUser?.kyc_docs_status?.some(
                            (val) =>
                              val["field"].toUpperCase() ===
                                item["public_id"]
                                  .split("/")[2]
                                  .split("-")[0]
                                  .split("_")
                                  .join(" ")
                                  .toUpperCase() && val["rejected"]
                          )}
                        >
                          Reject
                        </Button>,
                      ]}
                    >
                      {item["public_id"]
                        .split("/")[2]
                        .split("-")[0]
                        .split("_")
                        .join(" ")
                        .toUpperCase()}
                    </List.Item>
                  )}
                />
              )}
              {status === "error" && error && message.error(error)}
              <Button
                loading={verifyLoading}
                disabled={
                  !singleUser?.kyc_docs_uploaded || singleUser?.kyc_verified
                }
                type="primary"
                size="large"
                style={{ marginRight: 8 }}
                onClick={async () => {
                  // Verify the users account by changing the status
                  setVerifyLoading(true);
                  const resp = await approveKYCDocs(
                    loader?.token,
                    "active",
                    singleUser?.email
                  );

                  if (resp?.error) {
                    setVerifyLoading(false);
                    return message.error(resp?.message);
                  }
                  setVerifyLoading(false);
                  message.success(resp?.message);
                  navigate("/admin/dashboard/user_management");
                }}
              >
                VERIFY KYC DATA
              </Button>

              <br />
              <br />
            </Col>
            {rejectDocsList.length && (
              <Col span={24}>
                <Input.TextArea
                  value={rejectionText}
                  rows={5}
                  onChange={(e) => setRejectionText(e.target.value)}
                  style={{ border: ".85px solid #53B9EA", marginBottom: 20 }}
                />
                <br />
                <Button
                  loading={rejectLoading}
                  type="danger"
                  size="large"
                  disabled={
                    singleUser?.kyc_docs_uploaded
                      ? !singleUser?.kyc_docs_uploaded
                      : true
                  }
                  onClick={async () => {
                    // Reject kyc data submitted by user
                    setRejectLoading(true);
                    const resp = await rejectKYCData(
                      loader?.token,
                      rejectDocsList,
                      rejectionText,
                      singleUser?.email
                    );
                    if (resp?.error) {
                      setRejectLoading(false);
                      return message.error(resp?.message);
                    }
                    setRejectLoading(false);
                    message.success(resp?.message);
                    return navigate("/admin/dashboard/user_management");
                  }}
                  style={{ marginRight: 10 }}
                >
                  REJECT KYC DATA
                </Button>
                <Button
                  type="ghost"
                  size="large"
                  onClick={() => {
                    setRejectedDocsList([]);
                    setRejectionText();
                  }}
                >
                  CANCEL
                </Button>
              </Col>
            )}
          </Row>
        </Col>
        <Col xs={{ span: 24 }} lg={{ span: 24 }} style={{ marginTop: 30 }}>
          <h1 style={{ display: "inline-block", margin: 0 }}>
            User Transactions
          </h1>
          <Table
            bordered={false}
            columns={userColumns}
            dataSource={singleUserTrx}
            style={{ background: "transparent" }}
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  if (
                    event.target.nodeName === "SPAN" ||
                    event.target.nodeName === "BUTTON"
                  ) {
                    message.info("Feature will be available soon");
                    console.log(
                      event.target.nodeName,
                      record,
                      rowIndex,
                      "Event of the table click"
                    );
                    // Also show modal and confirm flag of transactions
                  }
                }, // Handle button click
              };
            }}
          />
        </Col>
      </Row>
    </Card>
  );
}
