import { Card, Col, DatePicker, Row, Table } from "antd";
import { useActionData, useOutletContext, useTransition } from "remix";
import { LineChart } from "~/components";

export const action = async ({ request }) => {
  // await new Promise(res => setTimeout(res, 1000));
  // const formData = await request.formData();

};

const { RangePicker } = DatePicker;

const dataSource = [
  {
    key: '1',
    type: 'Crypto Deposit',
    total: 10000000,
    date: '08/24/2022',
    status: 'Processed',
    network: 'xbn'
  },
  {
    key: '2',
    type: 'Fiat Deposit',
    total: 40000000,
    date: '08/24/2022',
    status: 'Processed',
    network: 'xbn'
  },
  {
    key: '3',
    type: 'Fiat Redeem',
    total: 120000000,
    date: '08/24/2022',
    status: 'Processed',
    network: 'xbn'
  },
  {
    key: '4',
    type: 'cNGN Redeem',
    amount: 75000000,
    date: '08/24/2022',
    status: 'Processed',
    network: 'xbn'
  },
  
];

const columns = [
  {
    title: 'Transaction Type',
    dataIndex: 'type',
    key: 'type',
    render: text => <>
      <img src="/token_logo.png" width={30} height={30} />
      <h4 style={{display: 'inline-block', marginLeft: 10}}>{text}</h4>
    </>,
    // responsive: ['md']
  },
  {
    title: 'Network',
    dataIndex: 'network',
    key: 'network',
    // responsive: ['md']
  },
  {
    title: 'Total (cNGN)',
    dataIndex: 'amount',
    key: 'amount',
    // responsive: ['md']
  },
  {
    title: 'Date',
    dataIndex: 'date',
    key: 'date',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
  },
  // {
  //   title: 'Status',
  //   dataIndex: 'status',
  //   key: 'status',
  // },
];


export default function History() {
  const errors = useActionData();
  const transition = useTransition();
  const { user, balances, isMobile } = useOutletContext();
  const [summaryData, setSummaryData] = React.useState(dataSource);


  React.useEffect(() => {
    const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

    setSummaryData(user?.transactions.reverse().map((val, i) => {
      // Mutate transactions list and get useable details
      return {
        key: i.toString(),
        network: val['network'].toUpperCase(),
        status: val['status'],
        type: val['trx_type'] === 'crypto_redeem' && val['asset_symbol'] === 'CNGN'? 'CNGN Withdrawal' : val['trx_type'] === 'fiat_redeem' && val['asset_symbol'] === 'NGN'? 'Fiat Redeem' : val['trx_type'] === 'enaira_redeem' && val['asset_symbol'] === 'ENGN'? 'ENaira Redeem' : val['trx_type'] === 'fiat_deposit' && val['asset_symbol'] === 'NGN'? 'Fiat Deposit' : val['trx_type'] === 'crypto_deposit' && val['asset_symbol'] === 'CNGN'? 'CNGN Deposit' : 'ENaira Deposit',
        date: `${new Date(val['createdAt']).toLocaleString("en-US", {timeZone: timezone}).split(",")[0]}`,
        amount: Number(val['amount']).toFixed(1).replace(/\d(?=(\d{3})+\.)/g, "$&,").toString(),
      }
    }));

    return () => null
  }, [balances])

  return (
    <Row gutter={[16, 16]}>
      <Col span={24}>
        <Card style={{ borderRadius: 20 }}>
          <div style={{
            display: "flex",
            flexFlow: "row",
            alignItems: "center",
            justifyContent: "space-between"
          }}>
            <h1 style={{display: "inline-block", margin: 0}}>Transaction Count</h1>
            {/* <RangePicker size="large" /> */}
          </div>
          
          <LineChart transactions={user?.transactions} />
        </Card>
      </Col>
      <Col span={24}>
        <Card style={{ borderRadius: 20, background: "rgba(253, 253, 255, 0.7)",
              background: "-webkit-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
              background: "-moz-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
              background: "linear-gradient(to bottom right, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)" }}>
          <div style={{
            display: "flex",
            flexFlow: "row",
            alignItems: "center",
            justifyContent: "space-between"
          }}>
            <h1 style={{display: "inline-block", margin: 0}}>History</h1>
            {/* <RangePicker size="large" />
            <Button size="large" type="primary">Deposits</Button>
            <Button size="large" type="ghost">Redeems</Button> */}
          </div>
          <Table dataSource={summaryData} columns={columns} scroll={{x: isMobile? 500 : 0}} />
        </Card>
      </Col>
    </Row>
  );
}
