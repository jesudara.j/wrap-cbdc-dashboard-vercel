import {
  CheckCircleFilled,
  CloseCircleFilled,
  ExclamationCircleFilled,
  RightOutlined,
  SettingFilled,
} from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Empty,
  Image,
  Input,
  message,
  Modal,
  Row,
  Select,
  Spin,
  Steps,
  Switch,
  Tabs,
} from "antd";
// import QRCode from "react-qr-code";
import { useLoaderData, useNavigate, useOutletContext } from "remix";
import { GodModal, UploadComp } from "~/components";
import { userPrefs } from "~/cookies";
import {
  addBantuUserId,
  deleteYubikey,
  initVeriff,
  preferences,
  resendEmailVerification,
  setUP2FA,
  setupYubikey,
  uploadKYCFiles,
  verifyOTP,
} from "~/utils";

const { Step } = Steps;
const { TabPane } = Tabs;

export const loader = async ({ request }) => {
  const cookieHeader = request.headers.get("Cookie");
  const cookie = (await userPrefs.parse(cookieHeader)) || {};
  let url = new URL(request.url);
  let action = url.searchParams.get("action");
  let accountType = url.searchParams.get("type");
  let stage = url.searchParams.get("stage");

  if (action || accountType || stage) {
    // On veriff redirect check for these params and update page

    return { token: cookie?.token, action, accountType, stage };
  }

  return cookie;
};

export default function Account() {
  const { balances, user, isMobile } = useOutletContext();
  const loadData = useLoaderData();
  const { action, accountType, stage } = loadData;

  const [shouldChangePass, setShouldChangePass] = React.useState(false);
  const [firstName, setFirstName] = React.useState(user?.first_name);
  const [lastName, setLastName] = React.useState(user?.last_name);
  const [phone, setPhone] = React.useState(user?.phone);
  const [isModalVisible, setGodModalVisible] = React.useState(false);
  const [isYbModalVisible, setYBModalVisible] = React.useState(false);
  const [isYbDeleteModalVisible, setYBDeleteModal] = React.useState(false);
  const [isProfileButtonLoading, setIsProfileButtonLoading] =
    React.useState(false);
  const [isEmailResendLoading, setIsEmailResendLoading] = React.useState(false);
  const [startedVerification, setStartedVerification] = React.useState(
    action && action === "kyc" && stage ? true : false
  );
  const [verificationPath, setVerificationPath] = React.useState(
    accountType
      ? accountType
      : user?.account_type
      ? user?.account_type
      : "individual"
  );
  const [currentTabKey, setCurrentTabKey] = React.useState(
    action && action === "kyc"
      ? "4"
      : action && action === "security"
      ? "3"
      : "1"
  );

  const [bantuUserId, setBantuUserId] = React.useState(user?.bantu_user_id);
  const [isBantuIdAddLoading, setIsBantuIdAddLoading] = React.useState();

  return (
    <Row
      gutter={[16, 16]}
      style={{
        width: isMobile ? "100%" : "80%",
        marginLeft: 0,
        marginRight: 0,
        padding: 0,
      }}
    >
      <Col span={24}>
        <Tabs
          defaultActiveKey="1"
          activeKey={currentTabKey}
          onChange={(key) => setCurrentTabKey(key)}
          centered={true}
        >
          <TabPane tab="Account Settings" key="1">
            <Card style={{ borderRadius: 20 }}>
              {/* <h1 style={{margin: 0, fontSize: 19, fontWeight: "bold"}}>Account Settings</h1> */}
              <span>
                <ExclamationCircleFilled style={{ color: "" }} /> Click to edit
                and save changes
              </span>
              <br />
              <Row gutter={[16, 16]}>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <Input
                    placeholder="First name"
                    size="large"
                    name="first-name"
                    type="text"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                  />
                </Col>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <Input
                    placeholder="Last name"
                    size="large"
                    name="last-name"
                    type="text"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                  />
                </Col>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <Input
                    placeholder="Phone number"
                    size="large"
                    name="phone"
                    type="text"
                    value={phone}
                    onChange={(e) => setPhone(e.target.value)}
                  />
                  <br />
                  <br />
                  <Button
                    size="large"
                    type="ghost"
                    onClick={async () => {
                      // Update users profile
                      setIsProfileButtonLoading(true);
                      const profile = await preferences(
                        loadData.token,
                        "profile",
                        { firstName, lastName, phone }
                      );
                      console.log(profile, "User profile updated!");
                      setIsProfileButtonLoading(false);
                    }}
                    loading={isProfileButtonLoading}
                  >
                    Save Changes
                  </Button>
                </Col>
                {!user?.email_verified && (
                  <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                    <Button
                      type="link"
                      size="large"
                      onClick={async () => {
                        setIsEmailResendLoading(true);
                        const res = await resendEmailVerification(
                          loadData.token
                        );
                        window.alert(res?.message);
                        setIsEmailResendLoading(false);
                      }}
                      loading={isEmailResendLoading}
                    >
                      Send Verification Link
                    </Button>
                  </Col>
                )}
              </Row>
            </Card>
          </TabPane>
          <TabPane tab="Preferences" key="2">
            <Card style={{ borderRadius: 20, transition: "all 1s ease 0s" }}>
              {/* <h1 style={{margin: 0, fontSize: 19, fontWeight: "bold"}}>Preferences</h1> */}
              <Row gutter={[16, 32]}>
                <Col
                  xs={{ span: 24 }}
                  lg={{ span: 12 }}
                  style={{
                    display: "flex",
                    flexFlow: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <h3>Change Profile image</h3>
                  <Button size="large" type="ghost">
                    Upload{" "}
                  </Button>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  lg={{ span: 12 }}
                  style={{
                    display: "flex",
                    flexFlow: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <h3>Change Currency</h3>
                  <Select
                    defaultActiveFirstOption
                    defaultValue="ngn"
                    style={{ minWidth: 80 }}
                    size="large"
                  >
                    <Select.Option value="ngn">NGN</Select.Option>
                  </Select>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  lg={{ span: 12 }}
                  style={{
                    display: "flex",
                    flexFlow: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <h3>Logout Timer</h3>
                  <Select
                    defaultActiveFirstOption
                    defaultValue="5mins"
                    style={{ minWidth: 80 }}
                    size="large"
                  >
                    <Select.Option value="5mins">5 mins</Select.Option>
                    <Select.Option value="10mins">10 mins</Select.Option>
                    <Select.Option value="15mins">15 mins</Select.Option>
                    <Select.Option value="20mins">20 mins</Select.Option>
                  </Select>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  lg={{ span: 12 }}
                  style={{
                    display: "flex",
                    flexFlow: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  {shouldChangePass && (
                    <Row gutter={[16, 16]}>
                      <Col span={24}>
                        <span>
                          Changing password will put your account on a 5 day
                          security hold/review
                        </span>
                        <br />
                        <br />
                        <Input
                          size="large"
                          placeholder="Current password"
                          name="old-password"
                          style={{ border: "1px solid rba(0,0,0,.6)" }}
                        />
                      </Col>
                      <Col span={24}>
                        <Input
                          size="large"
                          placeholder="New password"
                          name="new-password"
                          style={{ border: "1px solid rba(0,0,0,.6)" }}
                        />
                      </Col>
                      <Col span={24}>
                        <Input
                          size="large"
                          placeholder="Confirm New password"
                          name="confirm-password"
                          style={{ border: "1px solid rba(0,0,0,.6)" }}
                        />
                      </Col>
                      <Col span={24}>
                        <Button type="primary" style={{ marginRight: 10 }}>
                          Save Changes
                        </Button>
                        <Button
                          type="ghost"
                          onClick={() => setShouldChangePass(!shouldChangePass)}
                        >
                          Discard
                        </Button>
                      </Col>
                    </Row>
                  )}

                  {!shouldChangePass && (
                    <>
                      <h3>Change Password</h3>
                      <Button
                        onClick={() => {
                          // setShouldChangePass(!shouldChangePass);
                          const modal = Modal.confirm({
                            title: "Change your password",
                            content: (
                              <>
                                <Input
                                  size="large"
                                  type="text"
                                  placeholder="Enter old and new password"
                                />
                                <br />
                              </>
                            ),
                            afterClose: () =>
                              message.info("Password change state info"),
                            cancelText: "Cancel change",
                            okText: "Go ahead",
                          });
                        }}
                        type="ghost"
                      >
                        Click To Change{" "}
                        <SettingFilled style={{ fontSize: 18 }} />
                      </Button>
                    </>
                  )}
                </Col>
                <Col
                  xs={{ span: 24 }}
                  lg={{ span: 12 }}
                  style={{
                    display: "flex",
                    flexFlow: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <h3>Add Bantu User ID</h3>
                  <br />
                  <Input
                    type="text"
                    placeholder="Enter bantu user ID"
                    size="large"
                    value={bantuUserId}
                    onChange={(e) => {
                      setBantuUserId(e.target.value);
                    }}
                    suffix={
                      <Button
                        type="primary"
                        size="middle"
                        loading={isBantuIdAddLoading}
                        onClick={async () => {
                          // TODO: Make API call and send bantu to server
                          console.log(bantuUserId);
                          if (!bantuUserId) {
                            return message.info("Enter a valid bantu user ID");
                          }
                          setIsBantuIdAddLoading(true);

                          const res = await addBantuUserId(
                            loadData?.token,
                            bantuUserId
                          );
                          if (res.error) {
                            setIsBantuIdAddLoading(false);
                            return message.error(res.message);
                          }

                          setIsBantuIdAddLoading(false);
                          return message.success(res.message);
                        }}
                      >
                        Save
                      </Button>
                    }
                  />
                </Col>
              </Row>
            </Card>
          </TabPane>
          <TabPane tab="Security" key="3">
            <Card style={{ borderRadius: 20, transition: "all 1s ease 0s" }}>
              <Row gutter={[16, 16]}>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <h3>Activate 2FA</h3>
                </Col>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <Switch
                    defaultChecked={user?.two_factor_auth_enabled}
                    onChange={async (value) => {
                      setGodModalVisible(value);
                    }}
                  />
                  <GodModal
                    isMobile={isMobile}
                    title="2 Factor Authentication"
                    isModalVisible={isModalVisible}
                    setGodModalVisible={setGodModalVisible}
                    handleOk={() => {}}
                  >
                    <span>
                      The first step is to download the Google Authenticator app
                      for your Android or iOS device. If you need help getting
                      started, please see Googles Support Page.
                    </span>
                    <br />
                    <br />
                    {isModalVisible && (
                      <Get2FAQRCode
                        accessToken={loadData?.token}
                        isModalVisible={isModalVisible}
                        setGodModalVisible={setGodModalVisible}
                      />
                    )}

                    <Row gutter={[16, 16]} style={{ width: "60%" }}>
                      <Col span={12}>
                        <span>Password change </span>
                      </Col>
                      <Col span={12}>
                        <Switch
                          size="small"
                          defaultChecked={true}
                          disabled
                          onChange={() =>
                            window.alert("Switch for 2FA password change")
                          }
                        />
                      </Col>
                      <Col span={12}>
                        <span>Transfer </span>
                      </Col>
                      <Col span={12}>
                        <Switch
                          size="small"
                          defaultChecked={true}
                          disabled
                          onChange={() =>
                            window.alert("Switch for 2FA transfer")
                          }
                        />
                      </Col>
                    </Row>
                  </GodModal>
                </Col>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <h3>Activate Yubikey</h3>
                </Col>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <Switch
                    defaultChecked={user?.yubikey_activated}
                    onChange={(value) =>
                      value
                        ? setYBModalVisible(value)
                        : setYBDeleteModal(!value)
                    }
                  />
                  <GodModal
                    isMobile={isMobile}
                    title="Activate Yubikey"
                    isModalVisible={isYbModalVisible}
                    setGodModalVisible={setYBModalVisible}
                    handleOk={() => {}}
                  >
                    <YubikeySetupSteps
                      token={loadData.token}
                      setYBModalVisible={setYBModalVisible}
                    />
                  </GodModal>
                  <GodModal
                    isMobile={isMobile}
                    title="Delete Yubikey"
                    isModalVisible={isYbDeleteModalVisible}
                    setGodModalVisible={setYBDeleteModal}
                    handleOk={() => {}}
                  >
                    <YubikeyDeleteSteps
                      token={loadData.token}
                      setYBDeleteModalVisible={setYBDeleteModal}
                    />
                  </GodModal>
                </Col>
              </Row>
            </Card>
          </TabPane>
          <TabPane tab="KYC Verification" key="4">
            <Card style={{ borderRadius: 20, transition: "all 1s ease 0s" }}>
              {startedVerification ||
                (user?.kyc_docs_uploaded && !user?.kyc_verified && (
                  <h1
                    style={{
                      margin: 0,
                      fontSize: 19,
                      fontWeight: "bold",
                      color: "black",
                    }}
                  >
                    KYC Verification under review...{" "}
                    {typeof user?.kyc_docs_status !== "undefined" &&
                    user?.kyc_docs_status.some((val) => val?.rejected)
                      ? "Re-upload rejected documents"
                      : null}
                  </h1>
                ))}
              {startedVerification ||
                (user?.kyc_verified && user?.kyc_docs_uploaded && (
                  <h1
                    style={{
                      margin: 0,
                      fontSize: 19,
                      fontWeight: "bold",
                      color: "black",
                    }}
                  >
                    KYC has been verified successfully
                  </h1>
                ))}
              {!startedVerification && !user?.kyc_docs_uploaded && (
                <Row gutter={[16, 32]}>
                  <Col xs={{ span: 24 }} lg={{ span: 24 }}>
                    <p>
                      If you wish to acquire or redeem cNGN, your account must
                      be verified in order to comply with relevant anti-money
                      laundering (AML) and counter terrorism financing (CTF)
                      laws and regulations.
                    </p>
                    <br />
                    <p>
                      <b>
                        Convexity Wrapped CBDC Token offers two types of
                        accounts:
                      </b>
                    </p>
                  </Col>

                  <Col
                    xs={{ span: 24 }}
                    lg={{ span: 12 }}
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      height: "auto",
                      padding: 20,
                    }}
                  >
                    <Card
                      style={{
                        backgroundColor: "rgba(0, 173, 230, 0.05)",
                        border: "rgba(0, 173, 230, 0.25)",
                        borderRadius: 25,
                        height: "100%",
                      }}
                      bodyStyle={{ textAlign: "center" }}
                    >
                      <Image
                        src="/kyc/individual_kyc.png"
                        width={"70%"}
                        preview={false}
                      />
                      <br />
                      <h1 style={{ color: "#00ADE6", margin: 0 }}>
                        INDIVIDUAL VERIFICATION
                      </h1>
                      <br />
                      <span>
                        An account in the name of an individual at least 18
                        years old
                      </span>
                      <br />
                      <br />
                      <Button
                        type="primary"
                        size="large"
                        onClick={async () => {
                          setVerificationPath("individual");
                          setStartedVerification(true);
                        }}
                      >
                        SELECT
                      </Button>
                    </Card>
                  </Col>
                  <Col
                    xs={{ span: 24 }}
                    lg={{ span: 12 }}
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      height: "auto",
                      padding: 20,
                    }}
                  >
                    <Card
                      style={{
                        backgroundColor: "rgba(0, 173, 230, 0.05)",
                        border: "rgba(0, 173, 230, 0.25)",
                        borderRadius: 25,
                        height: "100%",
                      }}
                      bodyStyle={{ textAlign: "center" }}
                    >
                      <br />
                      {/* <br/> */}
                      <Image
                        src="/kyc/company_kyc.png"
                        width={"70%"}
                        preview={false}
                      />
                      <br />
                      <br />
                      <h1 style={{ color: "#00ADE6", margin: 0 }}>
                        COMPANY VERIFICATION
                      </h1>
                      <br />
                      <span>
                        An account in the name <br />
                        of a corporation/partnership/trust
                      </span>
                      <br />
                      <br />
                      <Button
                        type="primary"
                        size="large"
                        onClick={async () => {
                          setVerificationPath("company");
                          setStartedVerification(true);
                        }}
                      >
                        SELECT
                      </Button>
                    </Card>
                  </Col>
                  <Col
                    xs={{ span: 24 }}
                    lg={{ span: 24 }}
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Card
                      style={{ backgroundColor: "#FF4A11", borderRadius: 25 }}
                      bodyStyle={{ padding: 10, textAlign: "center" }}
                    >
                      <p style={{ color: "white" }}>
                        You will pay a base fee of 85,000 naira only which would
                        be used for verification purposes and refunded to you
                        through rebate on fees
                      </p>
                    </Card>
                  </Col>
                </Row>
              )}
              {
                //Check the documents that where rejected and render the KYC Verification Path
                startedVerification ||
                  (user?.kyc_docs_uploaded &&
                    typeof user?.kyc_docs_status.length &&
                    user?.kyc_docs_status.some((val) => val?.rejected) &&
                    !user?.kyc_verified && (
                      <VerificationPath
                        type={verificationPath}
                        setStartedVerification={setStartedVerification}
                        reUpload={true}
                      />
                    ))
              }
              {startedVerification && (
                <VerificationPath
                  type={verificationPath}
                  setStartedVerification={setStartedVerification}
                  reUpload={false}
                />
              )}
            </Card>
          </TabPane>
        </Tabs>
      </Col>
    </Row>
  );
}

const Get2FAQRCode = ({
  isModalVisible,
  setGodModalVisible,
  accessToken,
  isMobile,
}) => {
  const [isLoadingQrCode, setIsLoadingQRCode] = React.useState(false);
  const [isVerify2FALoading, setIsVerify2FALoading] = React.useState(false);
  const [verificationCode, setVerificationCode] = React.useState();
  const [data, setData] = React.useState();
  React.useEffect(async () => {
    // If modal is visible call the preference endpoint to activate 2FA
    if (isModalVisible) {
      setIsLoadingQRCode(true);

      const res = await setUP2FA(accessToken);
      if (res?.error) {
        setIsLoadingQRCode(false);
        return (
          <div
            style={{
              padding: 30,
              marginLeft: 120,
              border: "1px solid #DAEFF8",
              backgroundColor: "white",
              textAlign: "center",
              display: "inline-block",
            }}
          >
            <span>An error occurred setting up the 2FA</span>
          </div>
        );
      }
      setData(res?.data);
      setIsLoadingQRCode(false);
      //     const resp = await preferences2FA(loadData.token, "2FA", value);
      // console.log(resp, "Activation response");
    }
    return () => {};
  }, [isModalVisible]);

  return (
    <>
      {isLoadingQrCode && <Spin />}
      {!isLoadingQrCode && (
        <>
          <div
            style={{
              padding: 30,
              border: "1px solid #DAEFF8",
              backgroundColor: "white",
              textAlign: "center",
              display: "inline-block",
            }}
          >
            <small>Scan the QR Code or enter the token</small>
            <br />
            <br />
            <img src={data && data?.url} width={"80%"} height={"80%"} />
            <br />
            {/* <QRCode value="test value" bgColor="#DAEFF8" /><br/><br/> */}
            <span>
              Account token (Key): {data && data?.accountToken.substring(0, 7)}
            </span>
          </div>
          <br />
          <br />
          <span>
            The token will not be shown again after 2FA is enabled. If you have
            multiple devices, add your account token to all of them before
            clicking enable. <br />
            (Note: Your Account Token will change each time you reload your
            browser.)
          </span>
          <br />
          <br />
          <Input.Group compact style={{ textAlign: "center" }}>
            <Input
              placeholder="Google Authentication Code"
              style={{
                width: isMobile ? "100%" : "calc(100% - 200px)",
                textAlign: "left",
                border: "1px solid rgba(83, 185, 234, 1)",
              }}
              onChange={(e) => setVerificationCode(e.target.value)}
            />
            <Button
              size="large"
              onClick={async () => {
                setIsVerify2FALoading(true);
                console.log(verificationCode, "Verification code");
                const res = await verifyOTP(accessToken, verificationCode);
                if (res.error) {
                  setIsVerify2FALoading(false);
                  return message.error(res?.message);
                }

                message.success(res?.message);
                setIsVerify2FALoading(false);
                setGodModalVisible(false);
                navigate("/dashboard/"); // As requested by sir Jeminusi
              }}
              loading={isVerify2FALoading}
              block={isMobile}
            >
              Activate 2FA
            </Button>
          </Input.Group>
          <br />
          <br />
          <br />
          <br />
        </>
      )}
    </>
  );
};

const YubikeySetupSteps = ({ token, setYBModalVisible }) => {
  const { isMobile } = useOutletContext();
  const [current, setCurrent] = React.useState(0);
  const [otp, setOTP] = React.useState();
  const [isSuccessful, setIsSuccessful] = React.useState();
  const [activateLoading, setActivateLoading] = React.useState(false);

  return (
    <>
      <Steps current={current}>
        <Step title="Activate Security key" key="1" />

        <Step title="Label Your YubiKey" key="2" />

        <Step title="Complete" key="3" />
      </Steps>
      {current === 0 && (
        <Row
          gutter={[8, 8]}
          style={{
            width: "100%",
            marginLeft: 0,
            marginRight: 0,
            paddingTop: 30,
            paddingBottom: 30,
          }}
        >
          <Col
            xs={{ span: 24 }}
            lg={{ span: 12 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image src="/yubikey/1.png" preview={false} width={"100%"} />
            <span style={{ textAlign: "center", color: "#00ADE6" }}>
              Step 1<br /> Insert your security key into a USB port.
            </span>
          </Col>
          <Col
            xs={{ span: 24 }}
            lg={{ span: 12 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image src="/yubikey/2.png" preview={false} width={"100%"} />
            <span style={{ textAlign: "center", color: "#00ADE6" }}>
              Step 2<br /> If the key has a blinking light, press the button or
              gold disk
            </span>
          </Col>
          <Col
            xs={{ span: 24 }}
            lg={{ span: 24 }}
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Button
              type="primary"
              size="large"
              onClick={() => setCurrent((current) => current + 1)}
            >
              ACTIVATE YUBIKEY
            </Button>
          </Col>
        </Row>
      )}
      {current === 1 && (
        <Row
          gutter={[16, 16]}
          style={{
            width: "100%",
            marginLeft: 0,
            marginRight: 0,
            paddingTop: 30,
            paddingBottom: 30,
          }}
        >
          <Col
            xs={{ span: 24 }}
            lg={{ span: 6 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image
              src="/yubikey/key_single.png"
              preview={false}
              width={"100%"}
            />
          </Col>
          <Col
            xs={{ span: 24 }}
            lg={{ span: 18 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <label>
              <Input
                type="text"
                size="large"
                value={otp}
                onChange={(e) => setOTP(e.target.value)}
                autoFocus
              />
              <span>
                <ExclamationCircleFilled style={{ color: "#00ADE6" }} /> Press
                the button on your YUBIKEY to activate it and continue.
              </span>
            </label>
            <Button
              type="primary"
              size="large"
              loading={activateLoading}
              onClick={async () => {
                setActivateLoading(true);
                console.log(otp);
                // Call api here and get back verification status
                const res = await setupYubikey(token, otp);
                setIsSuccessful(res.data);
                setActivateLoading(false);
                setCurrent((current) => current + 1);
              }}
              block
            >
              ACTIVATE YUBIKEY
            </Button>
          </Col>
        </Row>
      )}
      {current === 2 && (
        <Row
          gutter={[16, 16]}
          style={{
            width: "100%",
            marginLeft: 0,
            marginRight: 0,
            paddingTop: 30,
            paddingBottom: 30,
          }}
        >
          <Col
            xs={{ span: 24 }}
            lg={{ span: 24 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {isSuccessful && (
              <>
                <CheckCircleFilled
                  style={{
                    color: "#93DFF9",
                    fontSize: isMobile ? "10rem" : 400,
                  }}
                />
                <br />
                <span>YUBIKEY successfully added</span>
                <br />
                <br />
                <Button
                  size="large"
                  type="primary"
                  onClick={() => setYBModalVisible(false)}
                  block
                >
                  DONE
                </Button>
              </>
            )}
            {!isSuccessful && (
              <>
                <CloseCircleFilled
                  style={{ color: "red", fontSize: isMobile ? "10rem" : 400 }}
                />
                <br />
                <span>YUBIKEY failed to be added</span>
                <br />
                <br />
                <Button
                  size="large"
                  type="primary"
                  onClick={() => setCurrent((current) => current - 1)}
                  block
                >
                  RETRY
                </Button>
              </>
            )}
          </Col>
        </Row>
      )}
    </>
  );
};

const YubikeyDeleteSteps = ({ token, setYBDeleteModalVisible }) => {
  const { isMobile } = useOutletContext();
  const [current, setCurrent] = React.useState(0);
  const [otp, setOTP] = React.useState();
  const [isSuccessful, setIsSuccessful] = React.useState();
  const [deleteLoading, setDeleteLoading] = React.useState(false);

  return (
    <>
      <Steps current={current}>
        <Step title="Verify Removal" key="1" />

        <Step title="Complete" key="2" />

        {/* <Step title="Complete" key="3" /> */}
      </Steps>
      {current === 0 && (
        <Row
          gutter={[8, 8]}
          style={{
            width: "100%",
            marginLeft: 0,
            marginRight: 0,
            paddingTop: 30,
            paddingBottom: 30,
          }}
        >
          <Col
            xs={{ span: 24 }}
            lg={{ span: 24 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image src="/yubikey/remove.png" preview={false} width={"60%"} />
            <span
              style={{
                textAlign: "center",
                color: "#00ADE6",
                fontWeight: "bolder",
              }}
            >
              For your security, withdrawals from your account will be
              <br />
              disabled for 24 hours once your security key deleted.
            </span>
            <br />
            <span style={{ textAlign: "center", color: "#00ADE6" }}>
              Insert your security key into a USB port.
            </span>
          </Col>
          <Col
            xs={{ span: 24 }}
            lg={{ span: 24 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <label>
              <Input
                type="text"
                size="large"
                value={otp}
                onChange={(e) => setOTP(e.target.value)}
                autoFocus
              />
              <span>
                <ExclamationCircleFilled style={{ color: "#00ADE6" }} /> Press
                the button on your YUBIKEY to activate it and continue.
              </span>
            </label>
            <br />
            <div
              style={{
                display: "flex",
                flexFlow: "row",
                alignItems: "center",
                justifyContent: "space-evenly",
                width: "50%",
              }}
            >
              <Button
                type="ghost"
                size="large"
                onClick={() => setYBDeleteModalVisible(false)}
              >
                CANCEL
              </Button>
              <Button
                type="primary"
                size="large"
                loading={deleteLoading}
                onClick={async () => {
                  setDeleteLoading(true);
                  process.env.NODE_ENV === "development"
                    ? console.log(otp, "OTP")
                    : null;
                  // Call api here and get back verification status
                  const res = await deleteYubikey(token, otp);
                  setIsSuccessful(res.data);
                  setDeleteLoading(false);
                  setCurrent((current) => current + 1);
                }}
              >
                CONTINUE
              </Button>
            </div>
          </Col>
        </Row>
      )}
      {current === 1 && (
        <Row
          gutter={[16, 16]}
          style={{
            width: "100%",
            marginLeft: 0,
            marginRight: 0,
            paddingTop: 30,
            paddingBottom: 30,
          }}
        >
          <Col
            xs={{ span: 24 }}
            lg={{ span: 24 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {isSuccessful && (
              <>
                <CheckCircleFilled
                  style={{
                    color: "#93DFF9",
                    fontSize: isMobile ? "10rem" : 400,
                  }}
                />
                <br />
                <span>Security key Successful Deleted</span>
                <br />
                <span>
                  For your security, withdrawals from your account will be
                  disabled forr 24 hours once your secury key deleted.
                </span>
                <br />
                <br />
                <Button
                  size="large"
                  type="primary"
                  onClick={() => setYBDeleteModalVisible(false)}
                  block
                >
                  GO TO SETTINGS
                </Button>
              </>
            )}
            {!isSuccessful && (
              <>
                <CloseCircleFilled
                  style={{ color: "red", fontSize: isMobile ? "10rem" : 400 }}
                />
                <br />
                <span>YUBIKEY removal failed</span>
                <br />
                <br />
                <Button
                  size="large"
                  type="primary"
                  onClick={() => setCurrent((current) => current - 1)}
                  block
                >
                  RETRY
                </Button>
              </>
            )}
          </Col>
        </Row>
      )}
    </>
  );
};

const VerificationPath = ({
  type = "individual",
  setStartedVerification,
  reUpload,
}) => {
  const { balances, user, isMobile } = useOutletContext();
  const navigate = useNavigate();
  const loadData = useLoaderData();
  const { action, accountType, stage } = loadData;
  const [isSuccessful, setIsSuccessful] = React.useState(false);
  const [fileList, setFileList] = React.useState([]);
  const [uploadLoading, setUploadLoading] = React.useState(false);
  const [currentTabKey, setCurrentTabKey] = React.useState(
    (stage && stage === "upload") || reUpload ? "upload" : "individual"
  );
  const [kycStage, setKYCStage] = React.useState(
    (stage && stage === "upload") || reUpload ? 2 : 1
  );

  // Effect will run on every render
  React.useEffect(() => {
    // if(user?.kyc_docs_uploaded) {
    //   setCurrentTabKey("status");
    //   setIsSuccessful(true);
    // }
  });

  return (
    <>
      <Breadcrumb separator={<RightOutlined />}>
        <Breadcrumb.Item>
          <a href="#" onClick={() => setStartedVerification(false)}>
            KYC VERIFICATION
          </a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>{type.toUpperCase()}</Breadcrumb.Item>
      </Breadcrumb>
      <br />
      <Tabs
        defaultActiveKey="payment"
        activeKey={currentTabKey}
        onChange={(key) => setCurrentTabKey(key)}
      >
        <TabPane
          tab="1. Payment"
          key="payment"
          style={{ backgroundColor: "white" }}
          disabled
        >
          <Card
            style={{
              backgroundColor: "rgba(215, 245, 255, 1)",
              borderRadius: 15,
            }}
          >
            <p style={{ color: "#00ADE6" }}>
              You will pay a base fee of 85,000 naira only which would be used
              for verification purposes and refunded to you through rebate on
              fees
            </p>
            <Button type="primary" size="large" block>
              PAY VERIFICATION FEE
            </Button>
          </Card>
        </TabPane>
        <TabPane
          tab="2. Individual Verification"
          key="individual"
          style={{ backgroundColor: "white" }}
          disabled={kycStage !== 1}
        >
          <Card
            style={{
              backgroundColor: "rgba(215, 245, 255, 1)",
              borderRadius: 15,
            }}
          >
            <p style={{ color: "#00ADE6" }}>
              Begin your verification process through VERIFF and make sure to be
              in a well lit environment.
            </p>
            <Button
              type="primary"
              size="large"
              block
              onClick={async () => {
                const resp = await initVeriff(
                  loadData?.token,
                  user?.first_name,
                  user?.last_name,
                  type
                );
                if (resp.error) {
                  return message.error(resp?.message);
                }
                window.location.replace(resp?.data?.verification?.url);
              }}
            >
              BEGIN VERIFICATION
            </Button>
          </Card>
        </TabPane>
        <TabPane
          tab="3. Document Upload"
          key="upload"
          style={{ backgroundColor: "white" }}
          disabled={kycStage !== 2}
        >
          {/* <span>Allowed file types include: .jpg, .jpeg, .png, .pdf, .docx</span><br/><br/> */}
          <span>Allowed file type - .pdf</span>
          <br />
          <br />

          {type === "individual" && (
            <Card bodyStyle={{ padding: 30 }}>
              <UploadComp
                docStatus={user?.kyc_docs_status}
                label="PROOF OF RESIDENCY"
                required={true}
                bottomText="Not less than three (3) months"
                onFileRemove={(file) =>
                  setFileList((preFiles) => {
                    const index = preFiles.indexOf(file);
                    const newFileList = preFiles.slice();
                    newFileList.splice(index, 1);
                    return newFileList;
                  })
                }
                onFileSelect={(file) =>
                  setFileList((preFile) => [...preFile, file])
                }
              />
              <UploadComp
                docStatus={user?.kyc_docs_status}
                label="BANK STATEMENT"
                required={true}
                bottomText="Not less than three (3) months"
                onFileRemove={(file) =>
                  setFileList((preFiles) => {
                    const index = preFiles.indexOf(file);
                    const newFileList = preFiles.slice();
                    newFileList.splice(index, 1);
                    return newFileList;
                  })
                }
                onFileSelect={(file) =>
                  setFileList((preFile) => [...preFile, file])
                }
              />

              <br />
              <br />
              <Button
                type="primary"
                size="large"
                block
                loading={uploadLoading}
                onClick={async () => {
                  if (!fileList.length)
                    return message.warn(
                      "Upload Files before clicking the upload button"
                    );
                  if (reUpload) {
                    console.log(
                      await Promise.all(
                        user?.kyc_docs_status.map((val) => val?.rejected)
                      ).length,
                      "Length of reupload"
                    );
                    if (
                      fileList.length <
                      (await Promise.all(
                        user?.kyc_docs_status.map((val) => val?.rejected)
                      ).length)
                    )
                      return message.warn(
                        "All files must be uploaded before proceeding"
                      );
                  } else {
                    if (fileList.length < 2)
                      return message.warn(
                        "All files must be uploaded before proceeding"
                      );
                  }
                  // Create FormData and append files
                  setUploadLoading(true);
                  if (type === "individual") {
                    let formData = new FormData();
                    for (let i = 0; i < fileList.length; i++) {
                      formData.append("files", fileList[i]);
                    }
                    formData.append("account_type", type);

                    const res = await uploadKYCFiles(loadData?.token, formData);
                    if (res?.error) {
                      setUploadLoading(false);
                      setIsSuccessful(false);
                      setCurrentTabKey("status");
                      setFileList([]);
                      return message.error(res?.message);
                    }

                    setFileList([]);
                    setIsSuccessful(true);
                    message.success(res?.message);
                    // fileList.forEach(file => {
                    // });
                  }
                  setKYCStage(3);
                  setUploadLoading(false);
                  setCurrentTabKey("status");
                }}
              >
                UPLOAD DOCUMENT
              </Button>
            </Card>
          )}
          {type === "company" && (
            <Card bodyStyle={{ padding: 30 }}>
              <UploadComp
                docStatus={user?.kyc_docs_status}
                label="CONTACT AGENT PROOF OF RESIDENCY"
                required={true}
                bottomText="Not less than three (3) months"
                onFileSelect={(file) =>
                  setFileList((preFile) => [...preFile, file])
                }
                onFileRemove={(file) =>
                  setFileList((preFiles) => {
                    const index = preFiles.indexOf(file);
                    const newFileList = preFiles.slice();
                    newFileList.splice(index, 1);
                    return newFileList;
                  })
                }
              />
              <UploadComp
                docStatus={user?.kyc_docs_status}
                label="CERTIFICATE OF INCORPORATION AND BUSINESS REGISTRATION"
                required={true}
                bottomText="Not less than three (3) months"
                onFileSelect={(file) =>
                  setFileList((preFile) => [...preFile, file])
                }
                onFileRemove={(file) =>
                  setFileList((preFiles) => {
                    const index = preFiles.indexOf(file);
                    const newFileList = preFiles.slice();
                    newFileList.splice(index, 1);
                    return newFileList;
                  })
                }
              />
              <UploadComp
                docStatus={user?.kyc_docs_status}
                label="MEMORANDUM AND ARTICLES OF ASSOCIATION"
                required={true}
                bottomText="Not less than three (3) months"
                onFileSelect={(file) =>
                  setFileList((preFile) => [...preFile, file])
                }
                onFileRemove={(file) =>
                  setFileList((preFiles) => {
                    const index = preFiles.indexOf(file);
                    const newFileList = preFiles.slice();
                    newFileList.splice(index, 1);
                    return newFileList;
                  })
                }
              />
              <UploadComp
                docStatus={user?.kyc_docs_status}
                label="Certificate of Hosting or similar documents listing officials, directors, shareholders and or other authorized signatories."
                required={true}
                bottomText="Not less than three (3) months"
                onFileSelect={(file) =>
                  setFileList((preFile) => [...preFile, file])
                }
                onFileRemove={(file) =>
                  setFileList((preFiles) => {
                    const index = preFiles.indexOf(file);
                    const newFileList = preFiles.slice();
                    newFileList.splice(index, 1);
                    return newFileList;
                  })
                }
              />
              <UploadComp
                docStatus={user?.kyc_docs_status}
                label="Board decision authorizing the agent / contact to work with Convexity Wrapped CBDC Token."
                required={true}
                bottomText="Not less than three (3) months"
                onFileSelect={(file) =>
                  setFileList((preFile) => [...preFile, file])
                }
                onFileRemove={(file) =>
                  setFileList((preFiles) => {
                    const index = preFiles.indexOf(file);
                    const newFileList = preFiles.slice();
                    newFileList.splice(index, 1);
                    return newFileList;
                  })
                }
              />
              <UploadComp
                docStatus={user?.kyc_docs_status}
                label="Sufficient documents to identify the ownership and control structure of the company reflect the interests of the% of the last owners to benefit."
                required={true}
                bottomText="Not less than three (3) months"
                onFileSelect={(file) =>
                  setFileList((preFile) => [...preFile, file])
                }
                onFileRemove={(file) =>
                  setFileList((preFiles) => {
                    const index = preFiles.indexOf(file);
                    const newFileList = preFiles.slice();
                    newFileList.splice(index, 1);
                    return newFileList;
                  })
                }
              />
              <UploadComp
                docStatus={user?.kyc_docs_status}
                label="BANK STATEMENT"
                required={true}
                bottomText="Not less than three (3) months"
                onFileSelect={(file) =>
                  setFileList((preFile) => [...preFile, file])
                }
                onFileRemove={(file) =>
                  setFileList((preFiles) => {
                    const index = preFiles.indexOf(file);
                    const newFileList = preFiles.slice();
                    newFileList.splice(index, 1);
                    return newFileList;
                  })
                }
              />
              <UploadComp
                docStatus={user?.kyc_docs_status}
                label="Proof of registered office and business address (if different from registered office)"
                required={true}
                bottomText="Not less than three (3) months"
                onFileSelect={(file) =>
                  setFileList((preFile) => [...preFile, file])
                }
                onFileRemove={(file) =>
                  setFileList((preFiles) => {
                    const index = preFiles.indexOf(file);
                    const newFileList = preFiles.slice();
                    newFileList.splice(index, 1);
                    return newFileList;
                  })
                }
              />
              <UploadComp
                docStatus={user?.kyc_docs_status}
                label=" Largest proprietor who owns 20% or more, one (1) image identification."
                required={true}
                bottomText="Not less than three (3) months"
                onFileSelect={(file) =>
                  setFileList((preFile) => [...preFile, file])
                }
                onFileRemove={(file) =>
                  setFileList((preFiles) => {
                    const index = preFiles.indexOf(file);
                    const newFileList = preFiles.slice();
                    newFileList.splice(index, 1);
                    return newFileList;
                  })
                }
              />
              <UploadComp
                docStatus={user?.kyc_docs_status}
                label="List of current registered officials"
                required={true}
                bottomText="Not less than three (3) months"
                onFileSelect={(file) =>
                  setFileList((preFile) => [...preFile, file])
                }
                onFileRemove={(file) =>
                  setFileList((preFiles) => {
                    const index = preFiles.indexOf(file);
                    const newFileList = preFiles.slice();
                    newFileList.splice(index, 1);
                    return newFileList;
                  })
                }
              />
              <UploadComp
                docStatus={user?.kyc_docs_status}
                label="List of current registered Directors"
                required={true}
                bottomText="Not less than three (3) months"
                onFileSelect={(file) =>
                  setFileList((preFile) => [...preFile, file])
                }
                onFileRemove={(file) =>
                  setFileList((preFiles) => {
                    const index = preFiles.indexOf(file);
                    const newFileList = preFiles.slice();
                    newFileList.splice(index, 1);
                    return newFileList;
                  })
                }
              />

              <br />
              <br />
              <Button
                type="primary"
                size="large"
                block
                loading={uploadLoading}
                onClick={async () => {
                  // Create FormData and append files
                  if (!fileList.length)
                    return message.warn("Upload Files before proceeding");
                  if (reUpload) {
                    console.log(
                      await Promise.all(
                        user?.kyc_docs_status.map((val) => val?.rejected)
                      ).length,
                      "Length of reupload"
                    );
                    if (
                      fileList.length <
                      (await Promise.all(
                        user?.kyc_docs_status.map((val) => val?.rejected)
                      ).length)
                    )
                      return message.warn(
                        "All files must be uploaded before proceeding"
                      );
                  } else {
                    if (fileList.length < 11)
                      return message.warn(
                        "All files must be uploaded before proceeding"
                      );
                  }

                  setUploadLoading(true);
                  let formData = new FormData();
                  for (let i = 0; i < fileList.length; i++) {
                    formData.append("files", fileList[i]);
                  }
                  formData.append("account_type", type);

                  const res = await uploadKYCFiles(loadData?.token, formData);
                  if (res?.error) {
                    setUploadLoading(false);
                    setIsSuccessful(false);
                    setCurrentTabKey("status");
                    setFileList([]);
                    return message.error(res?.message);
                  }

                  setFileList([]);
                  setIsSuccessful(true);
                  message.success(res?.message);
                  setKYCStage(3);
                  setUploadLoading(false);
                  setCurrentTabKey("status");
                }}
              >
                UPLOAD DOCUMENTS
              </Button>
            </Card>
          )}
        </TabPane>
        <TabPane
          tab="4. Status"
          key="status"
          style={{ backgroundColor: "white" }}
          disabled={kycStage !== 3}
        >
          <Card bodyStyle={{ textAlign: "center" }}>
            {isSuccessful && (
              <>
                <CheckCircleFilled
                  style={{
                    color: "#93DFF9",
                    fontSize: isMobile ? "10rem" : 400,
                  }}
                />
                <br />
                <br />
                <span>DOCUMENT SUBMISSION SUCCESSFUL AND UNDER REVIEW</span>
                <br />
                <br />
                <span>
                  You will receive an email from our verification desk
                  confirming the verification of your documents.
                </span>
                <br />
                <br />
                <Button
                  size="large"
                  type="primary"
                  onClick={() => {
                    setStartedVerification(false);
                    navigate("/dashboard/");
                  }}
                  block
                >
                  CLOSE
                </Button>
              </>
            )}
            {!isSuccessful && (
              <>
                <CloseCircleFilled
                  style={{ color: "red", fontSize: isMobile ? "10rem" : 400 }}
                />
                <br />
                <br />
                <span>DOCUMENT SUBMISSION FAILED</span>
                <br />
                <br />
                <Button
                  size="large"
                  type="primary"
                  onClick={() => {
                    setStartedVerification(false);
                    navigate(
                      `/dashboard/account?action=kyc&type=${type}&stage=upload`
                    );
                  }}
                  block
                >
                  TRY AGAIN
                </Button>
              </>
            )}
          </Card>
        </TabPane>
      </Tabs>
    </>
  );
};

export function ErrorBoundary({ error }) {
  console.error(error);
  return (
    <Empty
      image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
      imageStyle={{
        height: 60,
      }}
      style={{ height: "100vh", verticalAlign: "middle" }}
      description={
        <span>
          <b>An error occurred, please bare with us</b>
          <br />
          <a href="/dashboard/">Back to Dashboard</a>
        </span>
      }
    >
      {error.toString()}
    </Empty>
  );
}
