import { ExclamationCircleFilled } from "@ant-design/icons";
import {
  Button,
  Card,
  Col,
  Empty,
  Input,
  message,
  notification,
  Row,
  Select,
  Tooltip,
} from "antd";
import QRCode from "react-qr-code";
import {
  redirect,
  useActionData,
  useLoaderData,
  useLocation,
  useNavigate,
  useOutletContext,
  useSubmit,
  useTransition,
} from "remix";
import { CNGNCard, ENairaCard } from "~/components";
import { userPrefs } from "~/cookies";
import {
  addBantuUserId,
  copyTextToClipboard,
  depositCNGN,
  depositENGN,
  depositFiat,
  getCryptoDepositQR,
  streamPayment,
  verifyFiatDeposit,
} from "~/utils";

export const action = async ({ request }) => {
  // await new Promise(res => setTimeout(res, 1000));
  const cookieHeader = request.headers.get("Cookie");
  const cookie = (await userPrefs.parse(cookieHeader)) || {};
  let url = new URL(request.url);
  let deposit_type = url.searchParams.get("type");

  const formData = await request.formData();

  if (deposit_type === "fiat") {
    //Fiat deposit flow call

    const amount = formData.get("amount");
    const network = formData.get("network");
    console.log("Submitted value", amount, network);

    const amountErrors = {};
    if (!amount) {
      amountErrors.hasError = true;
      amountErrors.message = "Amount must be entered";
      return amountErrors;
    } else if (!network) {
      amountErrors.hasError = true;
      amountErrors.message = "Network must be selected to proceed";
      return amountErrors;
    }

    if (Number.parseInt(amount) < 100) {
      //! NOTE CHANGE THIS BACK TO #)MIL
      amountErrors.hasError = true;
      amountErrors.message = "Minimum deposit amount must be NGN 500,000";
      return amountErrors;
    }

    // Set the network to mint to as a cookie before redirecting to flutterwave
    cookie.fiatDepositMintNetwork = network;
    // Call the endpoint to submit amount and get flutterwave link to begin payment

    const res = await depositFiat(cookie.token, amount, network);
    if (res.error) {
      return res;
    }

    return redirect(res.data["link"], {
      headers: {
        "Set-Cookie": await userPrefs.serialize(cookie),
      },
    });
  }

  if (deposit_type === "cNGN") {
    // CNGN Deposit flow call
    const cngnAmount = formData.get("cngn-amount");
    console.log("Submitted value", cngnAmount);

    const errors = { cngn };
    if (!cngnAmount) {
      errors.cngn.hasError = true;
      errors.cngn.message = "Amount must be entered";
      return errors;
    }

    if (Number.parseInt(cngnAmount) < 30000000) {
      errors.cngn.hasError = true;
      errors.cngn.message = "Minimum deposit amount must be NGN 30,000,000";
      return errors;
    }

    const res = await depositCNGN(cookie.token, cngnAmount);
    if (res.error) {
      return res;
    }

    // return redirect('/dashboard/deposit');
    return redirect("/dashboard/");
  }

  if (deposit_type === "eNGN") {
    //eNaira deposit flow call
    // submit
  }

  return { message: "No data returned" };
};

// Route loader function
export const loader = async ({ request }) => {
  let url = new URL(request.url);
  const cookieHeader = request.headers.get("Cookie");
  const cookie = (await userPrefs.parse(cookieHeader)) || {};
  // tx_ref, transaction_id, status
  let tx_ref = url.searchParams.get("tx_ref");
  let trx_id = url.searchParams.get("transaction_id");
  let status = url.searchParams.get("status");
  console.log(tx_ref, trx_id, status, "Payment params from loader");

  if (
    (trx_id && tx_ref && status && status === "successful") ||
    status === "completed"
  ) {
    //Verify the deposit on redirect back to page

    // Check if the network mint has been set in cookie
    if (!cookie?.fiatDepositMintNetwork) {
      return {
        res: {
          error: true,
          status: "failed",
          message: "Network token mint not set",
          data: null,
        },
        token: cookie?.token,
      };
    }
    // call the verify deposit endpoint and pass the data to it
    console.log(status, trx_id, tx_ref, "Redirect payment");
    const res = await verifyFiatDeposit(
      cookie.token,
      trx_id,
      cookie?.fiatDepositMintNetwork
    );
    if (res.error) {
      console.log(res);
      return { res, token: cookie?.token };
    }

    return { res, token: cookie?.token };
  }

  return cookie;
};

export default function Deposit() {
  const { user, balances, isMobile } = useOutletContext();
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const { two_factor_auth_enabled, email_verified } = user;

  React.useEffect(() => {
    //Check if the user KYC is verified, if not redirect to /dashboard/
    if (!user?.kyc_verified) {
      message.warn("Verify your KYC to continue with this page", 15);
      navigate("/dashboard/");
    }
  });

  // Check if the user is verified and 2FA Enabled
  React.useEffect(() => {
    if (!two_factor_auth_enabled && !email_verified) {
      message.warn(
        "Verify your email and setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication processes",
        15
      );
    } else if (!two_factor_auth_enabled) {
      message.warn(
        "Setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication process",
        15
      );
    } else if (!email_verified) {
      message.warn("Verify your email address", 15);
    }
    return () => {};
  }, []);

  const [selected, setSelected] = React.useState();
  return (
    <Row
      gutter={[16, 16]}
      style={{ marginLeft: 0, marginRight: 0, padding: 0, width: "100%" }}
    >
      <Col span={24}>
        <CryptoDepositCard
          userCNGNAddress={user?.bantu_public_key}
          userBSCAddress={user?.bsc_public_key}
          selected={selected}
          setSelected={setSelected}
          isMobile={isMobile}
        />
      </Col>
      <Col span={24}>
        <FiatDepositCard
          user={user}
          selected={selected}
          setSelected={setSelected}
          isMobile={isMobile}
        />
      </Col>
    </Row>
  );
}

// Fiat deposit component
const FiatDepositCard = ({ selected, setSelected, user, isMobile }) => {
  const tokenList = [
    { code: "cNGN", disabled: false, prefix: "/icon-512x512.png" },
    { code: "cGHC", disabled: true, prefix: "/icon-512x512.png" },
    { code: "cXOF", disabled: true, prefix: "/icon-512x512.png" },
  ];
  const networkList = [
    {
      title: "Bantu (XBN)",
      val: "XBN",
      prefix: "/bantu_network_logo.png",
      disabled: false,
    },
    {
      title: "Binance Smart Chain (BSC)",
      val: "BSC",
      prefix: "/binance_network_logo.png",
      disabled: false,
    },
    {
      title: "Solana (SOL)",
      val: "SOL",
      prefix: "/solana_network_logo.svg",
      disabled: true,
    },
    {
      title: "Ethereum (ETH)",
      val: "ETH",
      prefix: "/ethereum_network_logo.svg",
      disabled: true,
    },
  ];
  const submit = useSubmit();
  const transition = useTransition();
  const locationInfo = useLocation();
  const navigate = useNavigate();
  const actionData = useActionData();
  const loaderData = useLoaderData();
  const fiatFormRef = React.useRef();
  const [receivable, setReceivable] = React.useState(0);
  const [amount, setAmount] = React.useState();
  const { pathname } = locationInfo;
  const [network, setNetwork] = React.useState(networkList[0].val);
  const [bantuUserId, setBantuUserId] = React.useState(user?.bantu_user_id);
  const [addBantuUserIdLoading, setAddBantuUserIdLoading] =
    React.useState(false);

  const { two_factor_auth_enabled, email_verified } = user;

  React.useEffect(() => {
    console.log("Action details", actionData, loaderData);
    if (loaderData?.res) {
      console.log(loaderData?.res, "Response of deposit");
      // message.error(loaderData?.message);
      const { status, data, message, error } = loaderData?.res;

      const close = () => {
        console.log("Notification was closed");
      };

      const key = `open${Date.now()}`;
      const btn = (
        <Button
          type="primary"
          size="medium"
          onClick={() => {
            if (status !== "success") {
              notification.close(key);
            } else {
              notification.close(key);
              navigate("/dashboard/");
            }
          }}
        >
          {status !== "success" ? "Ok" : "Dashboard"}
        </Button>
      );
      notification.open({
        message: `Transaction ${status.toUpperCase()}`,
        description: `${message}`,
        btn,
        key,
        onClose: close,
      });
    }
    return () => {};
  }, [pathname, loaderData]);

  return (
    <Card
      style={{ borderRadius: 20 }}
      bodyStyle={{
        paddingLeft: isMobile ? 10 : 20,
        paddingRight: isMobile ? 10 : 20,
      }}
    >
      {(typeof selected === "undefined" || selected !== "fiat-deposit") && (
        <>
          <div
            style={{
              display: "flex",
              flexFlow: "row",
              justifyContent: "space-between",
            }}
          >
            <h1>Fiat Deposit</h1>
            <img src="/flutterwave-logo.png" width={160} height={30} />
          </div>
          <span>
            <ExclamationCircleFilled style={{ color: "#00ADE6" }} /> Only
            deposit fiat
          </span>
          <br />
          <br />

          <Button
            type="primary"
            size="large"
            onClick={() => setSelected("fiat-deposit")}
          >
            Select
          </Button>
        </>
      )}
      {selected === "fiat-deposit" && (
        <>
          <div
            style={{
              display: "flex",
              flexFlow: "row",
              justifyContent: "space-between",
            }}
          >
            <h1>Fiat Deposit</h1>
            <img src="/flutterwave-logo.png" width={160} height={30} />
          </div>
          <span>
            <ExclamationCircleFilled style={{ color: "#00ADE6" }} /> Only
            deposit Fiat
          </span>
          <br />
          <br />

          <Input.Group size="large" style={{ width: "100%" }}>
            <Row style={{ width: "100%", marginLeft: 0, marginRight: 0 }}>
              <Col xs={{ span: 24 }} lg={{ span: 6 }}>
                <label>
                  Select Token (to mint)
                  <Select
                    defaultActiveFirstOption={true}
                    defaultValue={tokenList[0].code}
                    style={{ minWidth: "100%" }}
                    size="large"
                  >
                    {tokenList.map((val, i) => (
                      <Select.Option
                        key={i}
                        value={val.code}
                        disabled={val.disabled}
                      >
                        <img
                          src={val.prefix}
                          width={12}
                          height={12}
                          alt={val.code}
                        />{" "}
                        {val.code}
                      </Select.Option>
                    ))}
                  </Select>
                </label>
              </Col>
              <Col xs={{ span: 24 }} lg={{ span: 8 }}>
                <label>
                  Select Network (to mint to)
                  <Select
                    defaultActiveFirstOption={true}
                    defaultValue={network}
                    onChange={(value) => setNetwork(value)}
                    style={{ minWidth: "100%" }}
                    size="large"
                  >
                    {networkList.map((val, i) => (
                      <Select.Option
                        key={i}
                        value={val.val}
                        disabled={val.disabled}
                      >
                        <img
                          src={val.prefix}
                          width={12}
                          height={12}
                          alt={val.title}
                        />{" "}
                        {val.title}
                      </Select.Option>
                    ))}
                  </Select>
                </label>
              </Col>
              {!user?.bantu_user_id && network === "XBN" && (
                <Col span={24}>
                  <label>
                    Add Bantu User ID:
                    <Input
                      type="text"
                      size="large"
                      value={bantuUserId}
                      onChange={(e) => setBantuUserId(e.target.value)}
                      suffix={
                        <Button
                          loading={addBantuUserIdLoading}
                          type="primary"
                          size="middle"
                          onClick={async () => {
                            setAddBantuUserIdLoading(true);
                            const resp = await addBantuUserId(
                              loaderData?.token,
                              bantuUserId
                            );
                            if (resp.error) {
                              setAddBantuUserIdLoading(false);
                              return message.error(resp.message);
                            }

                            setAddBantuUserIdLoading(false);
                            return message.success(resp.message);
                          }}
                        >
                          Save
                        </Button>
                      }
                    />
                  </label>
                </Col>
              )}
              <Col xs={{ span: 24 }} lg={{ span: 10 }}>
                <label>
                  Amount to deposit
                  <br />
                  <Input
                    value={amount}
                    prefix={"NGN"}
                    onChange={(e) => {
                      e.target.value.length
                        ? setReceivable(
                            parseInt(e.target.value) -
                              parseInt(e.target.value) * 0.001
                          )
                        : setReceivable(0);
                      setAmount(e.target.value);
                    }}
                    size="large"
                    type="number"
                    placeholder=""
                    name="amount"
                    id="amount"
                  />
                  <small style={{ color: "red" }}>
                    {actionData?.hasError ? actionData?.message : null}
                  </small>
                </label>
              </Col>
            </Row>
          </Input.Group>
          <br />

          <span>
            Swap fee: <b>0.001</b>
            <Tooltip
              placement="right"
              title="Wrap token charges 0.001 for direct deposit swap from fiat (NGN) to wrapped token (cNGN)"
            >
              {" "}
              <ExclamationCircleFilled style={{ color: "#FFC700" }} />
            </Tooltip>
          </span>
          <br />
          <span>
            Flutterwave fee: <b>1.4%</b>{" "}
            <Tooltip
              placement="right"
              title="Flutterwave charges 1.4% (0.014) for fiat (NGN) deposit"
            >
              {" "}
              <ExclamationCircleFilled style={{ color: "#FFC700" }} />
            </Tooltip>
          </span>
          <br />
          <span>
            Receivable Amount: <b>{receivable}</b>{" "}
            <Tooltip
              placement="right"
              title="Receivable amount into your wallet when swap fee + flutterwave deposit fee is deducted"
            >
              {" "}
              <ExclamationCircleFilled style={{ color: "#FFC700" }} />
            </Tooltip>
          </span>
          <br />
          <br />

          <Button
            type="ghost"
            size="large"
            onClick={() => setSelected(undefined)}
            style={{ marginRight: 10 }}
          >
            Cancel
          </Button>
          <Button
            type="primary"
            size="large"
            onClick={() => {
              if (!two_factor_auth_enabled && !email_verified) {
                return message.warn(
                  "Verify your email and setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication processes",
                  15
                );
              } else if (!two_factor_auth_enabled) {
                return message.warn(
                  "Setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication processes",
                  15
                );
              } else if (!email_verified) {
                return message.warn("Verify your email address", 15);
              }
              let formData = new FormData();
              formData.append("amount", amount.toString());
              formData.append("network", network.toString());
              submit(formData, {
                method: "post",
                action: "/dashboard/deposit?type=fiat",
                replace: false,
              });
            }}
            loading={transition.state === "submitting"}
          >
            Make Deposit
          </Button>
          <br />
          <br />
          <span>
            <ExclamationCircleFilled style={{ color: "red" }} /> Charges include{" "}
            <b>0.001</b> base fee + third party deposit fee
          </span>
          <br />
        </>
      )}
    </Card>
  );
};

// Crypto deposit component
const CryptoDepositCard = ({
  selected,
  setSelected,
  userCNGNAddress,
  userBSCAddress,
  isMobile,
}) => {
  const tokenList = [
    { code: "cNGN", disabled: false, prefix: "/icon-512x512.png" },
    { code: "cGHC", disabled: true, prefix: "/icon-512x512.png" },
    { code: "cXOF", disabled: true, prefix: "/icon-512x512.png" },
  ];
  const cbdcList = [
    { code: "eNAIRA", disabled: false, prefix: "/e-naira-logo.png" },
    { code: "eCEDI", disabled: true, prefix: "/e-naira-logo.png" },
    { code: "eFRANC", disabled: true, prefix: "/e-naira-logo.png" },
    { code: "ePOUND", disabled: true, prefix: "/e-naira-logo.png" },
  ];
  const networkList = [
    {
      title: "Bantu (XBN)",
      val: "XBN",
      prefix: "/bantu_network_logo.png",
      disabled: false,
    },
    {
      title: "Binance Smart Chain (BSC)",
      val: "BSC",
      prefix: "/binance_network_logo.png",
      disabled: false,
    },
    {
      title: "Solana (SOL)",
      val: "SOL",
      prefix: "/solana_network_logo.svg",
      disabled: true,
    },
    {
      title: "Ethereum (ETH)",
      val: "ETH",
      prefix: "/ethereum_network_logo.svg",
      disabled: true,
    },
  ];
  const submit = useSubmit();
  const transition = useTransition();
  const navigate = useNavigate();
  const actionData = useActionData();
  const loaderData = useLoaderData();
  const { user } = useOutletContext();

  const [cryptoSelected, setCryptoSelected] = React.useState();
  const [walletVal, setWalletVal] = React.useState(userCNGNAddress);
  const [qrCode, setQrCode] = React.useState(null);
  const [cNGNAmount, setCNGNAmount] = React.useState(0);
  const [eNGNAmount, setENGNAmount] = React.useState(0);
  const [eNairaId, setENairaId] = React.useState("@jjeminu.01");
  const [userPaymentCode, setUserPaymentCode] = React.useState();
  // const [amountVal, setAmountVal] = React.useState(0);
  // const [currentDate, setCurrentDate] = React.useState();
  const [cngnLoading, setCNNGNLoading] = React.useState(false);
  const [qrCodeLoading, setQRCodeLoading] = React.useState(false);
  const [addBantuUserIdLoading, setAddBantuUserIdLoading] =
    React.useState(false);
  const [eNGNSendRequestLoading, setENGNSendRequestLoading] =
    React.useState(false);
  const [depositStreamRes, setDepositStreamRes] = React.useState();
  const [bantuUserID, setBantuUserId] = React.useState(user?.bantu_user_id);
  const walletAddyRef = React.useRef();
  const eNairaRef = React.useRef();
  const cNGNFormRef = React.useRef();

  const [network, setNetwork] = React.useState(networkList[0].val);

  React.useEffect(() => {
    if (actionData?.cngn?.hasError) {
      message.error(actionData?.cngn?.message);
      // return navigate('/dasboard/deposit');
    }
  }, [actionData]);

  // Effect dependent on the network switching (For cNGN deposit)
  React.useEffect(() => {
    if (cryptoSelected === "cNGN") {
      console.log("Current network on the parent card component", network);
      setQrCode(null);
      setQRCodeLoading(false);
      setWalletVal(network === "XBN" ? userCNGNAddress : userBSCAddress);
    }

    return () => {
      setQrCode(null);
    };
  }, [network, cryptoSelected]);

  React.useEffect(async () => {
    if (cryptoSelected === "cNGN") {
      console.log(network, "Network changed to");
      if (
        !depositStreamRes &&
        network === "XBN" &&
        (cryptoSelected !== null || typeof cryptoSelected !== "undefined") &&
        cryptoSelected === "cNGN"
      ) {
        let streamRes = await streamPayment(
          walletVal,
          loaderData?.token,
          network
        );

        setDepositStreamRes(streamRes);
        console.log(
          "🚀 ~ file: deposit.jsx ~ line 236 ~ CryptoDepositCard ~ stream",
          streamRes
        );
      } else if (
        depositStreamRes &&
        network === "BSC" &&
        (cryptoSelected !== null || typeof cryptoSelected !== "undefined") &&
        cryptoSelected === "cNGN"
      ) {
        depositStreamRes.close();
        let streamRes = await streamPayment(
          walletVal,
          loaderData?.token,
          network
        );
        setDepositStreamRes(null);
      }
    }

    return () => {
      setDepositStreamRes(null);
    };
  }, [network, walletVal, cryptoSelected]);

  return (
    <Card
      style={{ borderRadius: 20 }}
      bodyStyle={{
        paddingLeft: isMobile ? 10 : 20,
        paddingRight: isMobile ? 10 : 20,
      }}
    >
      {(typeof selected === "undefined" || selected !== "crypto-deposit") && (
        <>
          <div
            style={{
              display: "flex",
              flexFlow: "row",
              justifyContent: "space-between",
            }}
          >
            <h1>Crypto Deposit</h1>
            {/* <img src="/flutterwave-logo.png" width={160} height={30} /> */}
          </div>
          <span>
            <ExclamationCircleFilled style={{ color: "#00ADE6" }} /> Only
            deposit cNGN or eNAIRA as it is the only tokens supported
          </span>
          <br />
          <br />

          <Button
            type="primary"
            size="large"
            onClick={() => setSelected("crypto-deposit")}
          >
            Select
          </Button>
        </>
      )}
      {selected === "crypto-deposit" && (
        <>
          <div
            style={{
              display: "flex",
              flexFlow: "row",
              justifyContent: "space-between",
            }}
          >
            <h1>Crypto Deposit</h1>
            {/* <img src="/flutterwave-logo.png" width={160} height={30} /> */}
          </div>
          <span>
            <ExclamationCircleFilled style={{ color: "#00ADE6" }} /> Only
            deposit cNGN or eNAIRA as it is the only tokens supported
          </span>
          <br />
          <br />

          {typeof cryptoSelected === "undefined" && (
            <>
              <Row
                gutter={16}
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  width: isMobile ? "100%" : "70%",
                }}
              >
                <Col span={12}>
                  <CNGNCard
                    isMobile={isMobile}
                    tapHandler={async () => {
                      setCryptoSelected("cNGN");
                      setCNNGNLoading(true);
                    }}
                  />
                </Col>
                <Col span={12}>
                  <ENairaCard
                    isMobile={isMobile}
                    tapHandler={() => setCryptoSelected("eNGN")}
                  />
                </Col>
              </Row>
              <br />
              <br />
              <Button
                type="ghost"
                size="large"
                onClick={() => setSelected(undefined)}
                style={{ marginRight: 10 }}
              >
                Cancel
              </Button>
            </>
          )}
          {cryptoSelected === "cNGN" && (
            // <Form method="post" action="/dashboard/deposit?type=cNGN" ref={cNGNFormRef}>
            <Row
              gutter={[16, 16]}
              style={{
                marginLeft: 0,
                marginRight: 0,
                width: isMobile ? "100%" : "60%",
              }}
            >
              <Col span={24}>
                <label>
                  Select Token
                  <Select
                    defaultActiveFirstOption={true}
                    defaultValue={tokenList[0].code}
                    style={{ minWidth: "100%" }}
                    size="large"
                  >
                    {tokenList.map((val, i) => (
                      <Select.Option
                        key={i}
                        value={val.code}
                        disabled={val.disabled}
                      >
                        <img
                          src={val.prefix}
                          width={12}
                          height={12}
                          alt={val.code}
                        />{" "}
                        {val.code}
                      </Select.Option>
                    ))}
                  </Select>
                </label>
              </Col>
              <Col span={24}>
                <label>
                  Select Network
                  <Select
                    defaultActiveFirstOption={true}
                    defaultValue={network}
                    onChange={(value) => setNetwork(value)}
                    style={{ minWidth: "100%" }}
                    size="large"
                  >
                    {networkList.map((val, i) => (
                      <Select.Option
                        key={i}
                        value={val.val}
                        disabled={val.disabled}
                      >
                        <img
                          src={val.prefix}
                          width={12}
                          height={12}
                          alt={val.code}
                        />{" "}
                        {val.title}
                      </Select.Option>
                    ))}
                  </Select>
                </label>
              </Col>
              {!user?.bantu_user_id && network === "XBN" && (
                <Col span={24}>
                  <label>
                    Add Bantu User ID:
                    <Input
                      type="text"
                      size="large"
                      value={bantuUserID}
                      onChange={(e) => setBantuUserId(e.target.value)}
                      suffix={
                        <Button
                          loading={addBantuUserIdLoading}
                          type="primary"
                          size="middle"
                          onClick={async () => {
                            setAddBantuUserIdLoading(true);
                            const resp = await addBantuUserId(
                              loaderData?.token,
                              bantuUserID
                            );
                            if (resp.error) {
                              setAddBantuUserIdLoading(false);
                              return message.error(resp.message);
                            }

                            setAddBantuUserIdLoading(false);
                            return message.success(resp.message);
                          }}
                        >
                          Save
                        </Button>
                      }
                    />
                  </label>
                </Col>
              )}
              <Col span={24}>
                <label>
                  Enter Amount (Required to generate a deposit QR Code!)
                  <Input
                    type="number"
                    value={cNGNAmount}
                    onChange={(e) => setCNGNAmount(e.target.value)}
                    size="large"
                    suffix={
                      <Button
                        size="large"
                        type="primary"
                        loading={qrCodeLoading}
                        onClick={async () => {
                          setQRCodeLoading(true);
                          const { data, status } = await getCryptoDepositQR(
                            loaderData?.token,
                            cNGNAmount,
                            network.toLowerCase()
                          ); //Get bantu pay QR code to scan
                          if (status === "success") {
                            setQrCode(data?.qrCode);
                          }
                          setQRCodeLoading(false);
                          // console.log(data, "Data from the bantu pay api");
                        }}
                      >
                        GET QR CODE
                      </Button>
                    }
                  />
                </label>
              </Col>
              <Col span={24} style={{ textAlign: "center" }}>
                {/* QR Code here */}
                <div
                  style={{
                    padding: isMobile ? 10 : 30,
                    backgroundColor: "#DAEFF8",
                    position: "relative",
                  }}
                >
                  <span>Scan the QR Code to send token</span>
                  <br />
                  <br />
                  {qrCode && (
                    <img
                      src={qrCode}
                      width={250}
                      height={250}
                      alt="Deposit QR Code"
                    />
                  )}
                  {!qrCode && (
                    <span>
                      Enter amount and click the "GET QR CODE" button to deposit
                      with QR Code
                    </span>
                  )}
                  {/* <QRCode value={qrCode || null} bgColor="#DAEFF8" /> */}
                  <br />
                  <br />
                  <span style={{ color: "red" }}>
                    Send only cNGN ({network}) token to this address.
                  </span>
                </div>
                <br />

                <ul style={{ textAlign: "left" }}>
                  <li>Sending any other coins may result in permanent loss.</li>
                  <li>
                    Sending coins or token to a different network other than the
                    network selected will result in the loss of your deposit.
                  </li>
                </ul>
                <br />
                <Input
                  disabled
                  size="large"
                  type="text"
                  value={walletVal}
                  onChange={(e) => setWalletVal(e.target.value)}
                  ref={walletAddyRef}
                  suffix={
                    <Button
                      type="primary"
                      onClick={async () => {
                        var result = await copyTextToClipboard(
                          walletVal,
                          walletAddyRef
                        );
                        if (result.error) {
                          return message.error(result.message);
                        }

                        return message.success(result.message);
                      }}
                    >
                      COPY
                    </Button>
                  }
                />
                <br />
                <br />
                {/* <strong style={{textAlign: "center"}}>If you have made payment, click Sent Deposit</strong><br/><br/> */}
                <Button
                  type="ghost"
                  size="large"
                  onClick={() => {
                    // If transaction is canceled
                    console.log(depositStreamRes, "Stream Res");
                    if (
                      typeof depositStreamRes !== "undefined" &&
                      network === "XBN"
                    ) {
                      console.log(depositStreamRes, "Stream Res");
                      depositStreamRes?.close(); //Close event
                    }
                    setCNNGNLoading(false);
                    setCryptoSelected(undefined);
                  }}
                >
                  Cancel
                </Button>
                <br />
                <Button
                  type="link"
                  size="large"
                  style={{ marginRight: 10 }}
                  onClick={() => console.log("CNGN Deposit ongoing")}
                  loading={cngnLoading}
                  disabled
                >
                  Listening for transaction...
                </Button>
              </Col>
            </Row>
          )}
          {cryptoSelected === "eNGN" && (
            <Row
              gutter={[16, 16]}
              style={{
                marginLeft: 0,
                marginRight: 0,
                width: isMobile ? "100%" : "60%",
              }}
            >
              <Col span={24}>
                <Input.Group size="large">
                  <Row gutter={[0, 32]}>
                    <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                      <label>
                        Select Token:
                        <Select
                          defaultActiveFirstOption={true}
                          defaultValue={cbdcList[0].code}
                          style={{ minWidth: "100%" }}
                          size="large"
                        >
                          {cbdcList.map((val, i) => (
                            <Select.Option
                              key={i}
                              value={val.code}
                              disabled={val.disabled}
                            >
                              <img
                                src={val.prefix}
                                width={12}
                                height={12}
                                alt={val.code}
                              />{" "}
                              {val.code}
                            </Select.Option>
                          ))}
                        </Select>
                      </label>
                    </Col>
                    <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                      <label>
                        Select Network (to mint to):
                        <Select
                          defaultActiveFirstOption={true}
                          defaultValue={network}
                          onChange={(value) => setNetwork(value)}
                          style={{ minWidth: "100%" }}
                          size="large"
                        >
                          {networkList.map((val, i) => (
                            <Select.Option
                              key={i}
                              value={val.val}
                              disabled={val.disabled}
                            >
                              <img
                                src={val.prefix}
                                width={12}
                                height={12}
                                alt={val.title}
                              />{" "}
                              {val.title}
                            </Select.Option>
                          ))}
                        </Select>
                      </label>
                    </Col>
                    {!user?.bantu_user_id && network === "XBN" && (
                      <Col span={24}>
                        <label>
                          Add Bantu User ID:
                          <Input
                            type="text"
                            size="large"
                            value={bantuUserID}
                            onChange={(e) => setBantuUserId(e.target.value)}
                            suffix={
                              <Button
                                loading={addBantuUserIdLoading}
                                type="primary"
                                size="middle"
                                onClick={async () => {
                                  setAddBantuUserIdLoading(true);
                                  const resp = await addBantuUserId(
                                    loaderData?.token,
                                    bantuUserID
                                  );
                                  if (resp.error) {
                                    setAddBantuUserIdLoading(false);
                                    return message.error(resp.message);
                                  }

                                  setAddBantuUserIdLoading(false);
                                  return message.success(resp.message);
                                }}
                              >
                                Save
                              </Button>
                            }
                          />
                        </label>
                      </Col>
                    )}
                    <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                      <label>
                        Personal Speed Wallet payment code:
                        <Input
                          type="text"
                          size="large"
                          value={userPaymentCode}
                          onChange={(e) => setUserPaymentCode(e.target.value)}
                          style={{ border: ".75px solid grey" }}
                        />
                      </label>
                    </Col>
                    <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                      <label>
                        Enter amount to deposit:
                        <Input
                          type="number"
                          size="large"
                          value={eNGNAmount}
                          onChange={(e) => setENGNAmount(e.target.value)}
                          style={{ border: ".75px solid grey" }}
                        />
                      </label>
                    </Col>
                  </Row>
                </Input.Group>
              </Col>

              <Col span={24} style={{ textAlign: "center" }}>
                {/* QR Code here */}
                <div
                  style={{
                    padding: isMobile ? 10 : 30,
                    backgroundColor: "#DAEFF8",
                  }}
                >
                  <span>Scan the QR Code to send token</span>
                  <br />
                  <br />
                  <QRCode value={eNairaId} bgColor="#DAEFF8" />
                  <br />
                  <br />
                  <span style={{ color: "red" }}>
                    Send only eNAIRA to this address.
                  </span>
                </div>
                <br />

                <ul style={{ textAlign: "left" }}>
                  <li>Sending any other coins may result in permanent loss.</li>
                  <li>
                    Sending coins or token to a different network other than the
                    network selected will result in the loss of your deposit.
                  </li>
                </ul>
                <br />
                <Input
                  disabled
                  size="large"
                  type="text"
                  value={eNairaId}
                  onChange={(e) => setENairaId(e.target.value)}
                  ref={eNairaRef}
                  suffix={
                    <Button
                      type="primary"
                      onClick={async () => {
                        var result = await copyTextToClipboard(
                          eNairaId,
                          eNairaRef
                        );
                        if (result.error) {
                          return message.error(result.message);
                        }

                        return message.success(result.message);
                      }}
                    >
                      COPY
                    </Button>
                  }
                />
                <br />
                <br />

                <span>
                  Proceed to make payment with your merchant or personal speed
                  wallet mobile app to the payment code above, then click the
                  'Sent Deposit' button to continue
                </span>
                <br />
                <strong style={{ textAlign: "center" }}>
                  If you have made payment, click Sent Deposit
                </strong>
                <br />
                <br />
                <Button
                  loading={eNGNSendRequestLoading}
                  type="primary"
                  size="large"
                  style={{ marginRight: 10 }}
                  onClick={async () => {
                    setENGNSendRequestLoading(true);

                    if (!userPaymentCode) {
                      return message.warn(
                        "Users payment code from speed wallet must be entered to proceed"
                      );
                    }

                    // Call endpoint for the eNaira deposit
                    const { status, message, data } = await depositENGN(
                      loaderData?.token,
                      eNGNAmount,
                      userPaymentCode,
                      network
                    );
                    setENGNSendRequestLoading(false);
                    console.log(data, "Enaira deposit data");

                    const key = `open-enaira-deposit${Date.now()}`;
                    const btn = (
                      <Button
                        type="primary"
                        size="medium"
                        onClick={() => {
                          if (status !== "success") {
                            notification.close(key);
                          } else {
                            notification.close(key);
                            navigate("/dashboard/");
                          }
                        }}
                      >
                        {status !== "success" ? "Ok" : "Dashboard"}
                      </Button>
                    );
                    notification.open({
                      message: `Transaction ${status.toUpperCase()}`,
                      description: `${message}`,
                      btn,
                      key,
                      onClose: () => {
                        console.log("Notification was closed");
                      },
                    });
                  }}
                >
                  Sent Deposit
                </Button>
                <Button
                  type="ghost"
                  size="large"
                  onClick={() => setCryptoSelected(undefined)}
                >
                  Cancel
                </Button>
                <br />
              </Col>
            </Row>
          )}
        </>
      )}
    </Card>
  );
};

export function ErrorBoundary({ error }) {
  console.error(error);
  return (
    <Empty
      image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
      imageStyle={{
        height: 60,
      }}
      style={{ height: "100vh", verticalAlign: "middle" }}
      description={
        <span>
          <b>An error occurred, please bare with us</b>
          <br />
          <a href="/dashboard/">Back to Dashboard</a>
        </span>
      }
    >
      {error.toString()}
    </Empty>
  );
}
