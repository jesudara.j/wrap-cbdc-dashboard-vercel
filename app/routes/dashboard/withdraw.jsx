import {
  CheckCircleFilled,
  CloseCircleFilled,
  ExclamationCircleFilled,
} from "@ant-design/icons";
import {
  Alert,
  Button,
  Card,
  Col,
  Empty,
  Input,
  message,
  Row,
  Select,
  Steps,
} from "antd";
import {
  redirect,
  useActionData,
  useLoaderData,
  useLocation,
  useNavigate,
  useOutletContext,
  useSubmit,
  useTransition,
} from "remix";
import { CNGNCardWithdraw, GodModal } from "~/components";
import { userPrefs } from "~/cookies";
import { redeemCNGN, verifyOTP, verifyYubikey } from "~/utils";

const { Step } = Steps;

export const action = async ({ request }) => {
  // await new Promise(res => setTimeout(res, 1000));
  const cookieHeader = request.headers.get("Cookie");
  const cookie = (await userPrefs.parse(cookieHeader)) || {};
  let url = new URL(request.url);
  let redeem_type = url.searchParams.get("type");
  const formData = await request.formData();

  if (redeem_type === "cNGN") {
    const amount = formData.get("amount");
    const network = formData.get("network");
    const wallet_address = formData.get("address");
    console.log("Submitted value", amount, network, wallet_address);

    const errors = {};
    if (!amount) {
      errors.cngnHasError = true;
      errors.cngnMessage = "Amount must be entered";
      return errors;
    }

    if (Number.parseInt(amount) < 10) {
      //! CHANGE THIS LATER
      errors.cngnHasError = true;
      errors.cngnMessage = "Minimum deposit amount must be NGN 500,000";
      return errors;
    }

    // Call the endpoint to submit amount and get flutterwave link to begin payment
    const res = await redeemCNGN(cookie.token, amount, wallet_address, network);
    if (res.error) {
      return res;
    }

    return redirect("/dashboard/");
  }
};

export async function loader({ request }) {
  const cookieHeader = request.headers.get("Cookie");
  const cookie = (await userPrefs.parse(cookieHeader)) || {};

  return cookie;
}

export default function Withdraw() {
  const { user, balances, isMobile } = useOutletContext();
  const navigate = useNavigate();
  const [selected, setSelected] = React.useState();
  const cookies = useLoaderData();

  const { pathname } = useLocation();
  const { two_factor_auth_enabled, email_verified } = user;

  React.useEffect(() => {
    //Check if the user KYC is verified, if not redirect to /dashboard/
    // if(!user?.kyc_verified) {
    //   message.warn("Verify your KYC to continue with this page", 15);
    //   navigate("/dashboard/");
    // }
  });

  // Check if the user is verified and 2FA Enabled
  React.useEffect(() => {
    // if(!two_factor_auth_enabled && !email_verified) {
    //   message.warn("Verify your email and setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication process", 15);
    // } else if(!two_factor_auth_enabled) {
    //   message.warn("Setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication process", 15);
    // } else if(!email_verified) {
    //   message.warn("Verify your email address", 15);
    // }
    return () => {};
  }, [pathname]);

  return (
    <Row
      gutter={[16, 16]}
      style={{ marginLeft: 0, marginRight: 0, padding: 0, width: "100%" }}
    >
      <Col span={24}>
        {!user?.kyc_verified && (
          <Alert
            style={{ marginBottom: 10 }}
            type="warning"
            showIcon
            description={
              <>
                <span>Verify your KYC to continue with this page</span>
                <br />
                <br />
                <Button
                  size="small"
                  type="ghost"
                  onClick={() =>
                    navigate("/dashboard/account?action=kyc", { replace: true })
                  }
                >
                  ACCOUNT
                </Button>
              </>
            }
          />
        )}
        {!two_factor_auth_enabled && (
          <Alert
            style={{ marginBottom: 10 }}
            type="warning"
            showIcon
            description={
              <>
                <span>
                  Setup 2 factor authentication to carry out transactions. click
                  the button below to carry out authentication process
                </span>
                <br />
                <br />
                <Button
                  size="small"
                  type="ghost"
                  onClick={() =>
                    navigate("/dashboard/account?action=security", {
                      replace: true,
                    })
                  }
                >
                  ACCOUNT
                </Button>
              </>
            }
          />
        )}
        <WithdrawCard
          user={user}
          isMobile={isMobile}
          selected={selected}
          setSelected={setSelected}
          bscBalance={
            balances.filter((val) => val.asset_code === "CNGN (BSC)")[0] || {
              balance: 0,
            }
          }
          xbnBalance={
            balances.filter((val) => val.asset_code === "CNGN (XBN)")[0] || {
              balance: 0,
            }
          }
          token={cookies?.token}
        />
      </Col>
    </Row>
  );
}

// Crypto deposit component
const WithdrawCard = ({
  selected,
  setSelected,
  bscBalance,
  xbnBalance,
  isMobile,
  user,
  token,
}) => {
  const tokenList = [
    { code: "cNGN", disabled: false, prefix: "/icon-512x512.png" },
    { code: "cGHC", disabled: true, prefix: "/icon-512x512.png" },
    { code: "cXOF", disabled: true, prefix: "/icon-512x512.png" },
  ];
  const cbdcList = [
    { code: "eNAIRA", disabled: false, prefix: "/e-naira-logo.png" },
    { code: "eCEDI", disabled: true, prefix: "/e-naira-logo.png" },
    { code: "eFRANC", disabled: true, prefix: "/e-naira-logo.png" },
    { code: "ePOUND", disabled: true, prefix: "/e-naira-logo.png" },
  ];
  const networkList = [
    {
      title: "Bantu (XBN)",
      val: "XBN",
      prefix: "/bantu_network_logo.png",
      disabled: false,
    },
    {
      title: "Binance Smart Chain (BSC)",
      val: "BSC",
      prefix: "/binance_network_logo.png",
      disabled: false,
    },
    {
      title: "Solana (SOL)",
      val: "SOL",
      prefix: "/solana_network_logo.svg",
      disabled: true,
    },
    {
      title: "Ethereum (ETH)",
      val: "ETH",
      prefix: "/ethereum_network_logo.svg",
      disabled: true,
    },
  ];

  const [cryptoSelected, setCryptoSelected] = React.useState();
  const [amount, setAmount] = React.useState();
  const [isVerifyModalVisible, setVerifyModalVisible] = React.useState(false);
  const [network, setNetwork] = React.useState(networkList[0].val);
  const [walletVal, setWalletVal] = React.useState();

  const transition = useTransition();
  const actionData = useActionData();
  const cngnFormRef = React.useRef();
  const { two_factor_auth_enabled, yubikey_activated, email_verified } = user;

  React.useEffect(() => {
    if (actionData?.cngnHasError) {
      message.error(actionData?.cngnMessage);
    }

    if (actionData?.error) {
      message.error(actionData?.message);
    }
  }, [actionData]);

  React.useEffect(async () => {
    console.log(network, "Network switch");
    setAmount();
    return () => {
      setNetwork(networkList[0].val);
      setAmount();
    };
  }, [network]);

  return (
    <Card
      style={{ borderRadius: 20 }}
      bodyStyle={{
        paddingLeft: isMobile ? 10 : 20,
        paddingRight: isMobile ? 10 : 20,
      }}
    >
      {(typeof selected === "undefined" || selected !== "crypto-deposit") && (
        <>
          <div
            style={{
              display: "flex",
              flexFlow: "row",
              justifyContent: "space-between",
            }}
          >
            <h1>Withdraw</h1>
            {/* <img src="/flutterwave-logo.png" width={160} height={30} /> */}
          </div>
          <span>
            <ExclamationCircleFilled style={{ color: "#00ADE6" }} /> Select this
            option if you intend to withdraw cNGN{" "}
          </span>
          <br />
          <br />

          <Button
            type="primary"
            size="large"
            onClick={() => setSelected("crypto-deposit")}
          >
            Select
          </Button>
        </>
      )}
      {selected === "crypto-deposit" && (
        <>
          <div
            style={{
              display: "flex",
              flexFlow: "row",
              justifyContent: "space-between",
            }}
          >
            <h1>Withdraw</h1>
            {/* <img src="/flutterwave-logo.png" width={160} height={30} /> */}
          </div>
          <span>
            <ExclamationCircleFilled style={{ color: "#00ADE6" }} /> Select this
            option if you intend to withdraw cNGN{" "}
          </span>
          <br />
          <br />

          {typeof cryptoSelected === "undefined" && (
            <>
              <Row
                gutter={16}
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  width: isMobile ? "100%" : "70%",
                }}
              >
                <Col span={24}>
                  <CNGNCardWithdraw
                    isMobile={isMobile}
                    tapHandler={() => setCryptoSelected("cNGN")}
                  />
                </Col>
                {/* <Col span={12}>
                <ENairaCardRedeem isMobile={isMobile} tapHandler={() => setCryptoSelected('eNGN')} />
                
              </Col> */}
              </Row>
              <br />
              <br />
              <Button
                type="ghost"
                size="large"
                onClick={() => setSelected(undefined)}
                style={{ marginRight: 10 }}
              >
                Cancel
              </Button>
            </>
          )}
          {cryptoSelected === "cNGN" && (
            <Row
              gutter={[16, 16]}
              style={{
                marginLeft: 0,
                marginRight: 0,
                width: isMobile ? "100%" : "60%",
              }}
            >
              <Col span={24}>
                <label>
                  Select Token to withdraw
                  <Select
                    defaultActiveFirstOption={true}
                    defaultValue={tokenList[0].code}
                    onChange={(value) => setAmount(value)}
                    style={{ minWidth: "100%" }}
                    size="large"
                  >
                    {tokenList.map((val, i) => (
                      <Select.Option
                        key={i.toString()}
                        value={val.code}
                        disabled={val.disabled}
                      >
                        <img
                          src={val.prefix}
                          width={12}
                          height={12}
                          alt={val.code}
                        />{" "}
                        {val.code}
                      </Select.Option>
                    ))}
                  </Select>
                </label>
              </Col>
              <Col span={24}>
                <label>
                  Select Network
                  <Select
                    defaultActiveFirstOption={true}
                    defaultValue={network}
                    onChange={(value) => setNetwork(value)}
                    style={{ minWidth: "100%" }}
                    size="large"
                  >
                    {networkList.map((val, i) => (
                      <Select.Option
                        key={i.toString()}
                        value={val.val}
                        disabled={val.disabled}
                      >
                        <img
                          src={val.prefix}
                          width={12}
                          height={12}
                          alt={val.val}
                        />{" "}
                        {val.title}
                      </Select.Option>
                    ))}
                  </Select>
                  {network === "BSC" && (
                    <span style={{ marginTop: 10 }}>
                      <ExclamationCircleFilled style={{ color: "#00ADE6" }} />{" "}
                      Fund your Bsc wallet {user?.bsc_public_key} with BNB
                      through the{" "}
                      <a href="https://testnet.binance.org/faucet-smart">
                        faucet
                      </a>{" "}
                      to cover for gas fees.{" "}
                    </span>
                  )}
                </label>
              </Col>
              <Col span={24}>
                <label>
                  Amount
                  <Input
                    type="number"
                    value={
                      amount &&
                      amount.toString().replace(/\d(?=(\d{3})+\.)/g, "$&,")
                    }
                    size="large"
                    name="cngn-amount"
                    onChange={(e) => setAmount(e.target.value)}
                    suffix={
                      <Button
                        type="primary"
                        onClick={() =>
                          network === "XBN"
                            ? setAmount(Number(xbnBalance.balance).toFixed(0))
                            : setAmount(Number(bscBalance.balance).toFixed(0))
                        }
                      >
                        MAX
                      </Button>
                    }
                  />
                  <h4 style={{ textAlign: "right" }}>
                    cNGN (BSC) Balance:{" "}
                    {Number(bscBalance.balance)
                      .toFixed(2)
                      .replace(/\d(?=(\d{3})+\.)/g, "$&,")}{" "}
                    - cNGN (XBN) Balance:{" "}
                    {Number(xbnBalance.balance)
                      .toFixed(2)
                      .replace(/\d(?=(\d{3})+\.)/g, "$&,")}
                  </h4>
                </label>
              </Col>
              <Col span={24}>
                <label>
                  Wallet Address
                  <Input
                    size="large"
                    type="text"
                    name="address"
                    value={walletVal}
                    onChange={(e) => setWalletVal(e.target.value)}
                    style={{ border: "1px solid rgba(0,0,0,.12)" }}
                  />
                  <span>
                    <ExclamationCircleFilled style={{ color: "#00ADE6" }} />{" "}
                    Make sure to enter a valid {network} address
                  </span>
                </label>
                <br />
                <br />
                <Button
                  type="primary"
                  size="large"
                  style={{ marginRight: 10 }}
                  onClick={() => {
                    if (!two_factor_auth_enabled && !email_verified) {
                      return message.warn(
                        "Verify your email and setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication processes",
                        15
                      );
                    } else if (!two_factor_auth_enabled) {
                      return message.warn(
                        "Setup 2 factor authentication to carry out transactions",
                        15
                      );
                    } else if (!email_verified) {
                      return message.warn("Verify your email address", 15);
                    }
                    if (network === "XBN" && xbnBalance.balance <= 0) {
                      return message.warn(
                        "Your balance is insufficient for withdraw, make a deposit first",
                        6
                      );
                    } else if (network === "BSC" && bscBalance.balance <= 0) {
                      return message.warn(
                        "Your balance is insufficient for withdraw, make a deposit first",
                        6
                      );
                    } else if (!amount) {
                      return message.warn("Enter amount befere proceeding", 6);
                    } else if (!walletVal) {
                      return message.warn(
                        "The wallet to withdraw must be inserted, please make sure to insert the correct address for the network",
                        6
                      );
                    }

                    // Carry out the verification before withdrawal
                    if (two_factor_auth_enabled || yubikey_activated) {
                      // Show Godmodal with both path to verify
                      setVerifyModalVisible(true);
                    }
                  }}
                  loading={transition.state === "submitting"}
                >
                  Withdraw Asset
                </Button>
                <Button
                  type="ghost"
                  size="large"
                  onClick={() => setCryptoSelected(undefined)}
                >
                  Cancel
                </Button>
                <br />
                <GodModal
                  isMobile={isMobile}
                  title="Verify Transaction"
                  isModalVisible={isVerifyModalVisible}
                  setGodModalVisible={setVerifyModalVisible}
                  handleOk={() => {}}
                >
                  <VerifyComp
                    isMobile={isMobile}
                    walletVal={walletVal}
                    amount={amount}
                    network={network}
                    token={token}
                    setVerifyModalVisible={setVerifyModalVisible}
                    twoFA={two_factor_auth_enabled}
                    yubiKey={yubikey_activated}
                  />
                </GodModal>
              </Col>
            </Row>
          )}
          {cryptoSelected === "eNGN" && (
            <Row
              gutter={[16, 16]}
              style={{
                marginLeft: 0,
                marginRight: 0,
                width: isMobile ? "100%" : "60%",
              }}
            >
              <Col span={24}>
                <label>
                  Select Token to Redeem
                  <Select
                    defaultActiveFirstOption={true}
                    defaultValue={cbdcList[0].code}
                    style={{ minWidth: "100%" }}
                    size="large"
                  >
                    {cbdcList.map((val, i) => (
                      <Select.Option
                        key={i.toString()}
                        value={val.code}
                        disabled={val.disabled}
                      >
                        <img
                          src={val.prefix}
                          width={12}
                          height={12}
                          alt={val.code}
                        />{" "}
                        {val.code}
                      </Select.Option>
                    ))}
                  </Select>
                </label>
              </Col>

              <Col span={24}>
                <label>
                  Amount
                  <Input
                    size="large"
                    type="number"
                    suffix={<Button type="primary">MAX</Button>}
                  />
                  <h4 style={{ textAlign: "right" }}>
                    Balance:{" "}
                    {Number(balance.balance)
                      .toFixed(2)
                      .replace(/\d(?=(\d{3})+\.)/g, "$&,")}
                  </h4>
                </label>
              </Col>
              <Col span={24}>
                <label>
                  Payment code
                  <Input
                    size="large"
                    type="text"
                    style={{ border: "1px solid rgba(0,0,0,.12)" }}
                  />
                </label>
                <br />
                <br />
                <Button
                  type="primary"
                  size="large"
                  style={{ marginRight: 10 }}
                  onClick={() => {
                    if (!two_factor_auth_enabled || !email_verified) {
                      return message.warn(
                        "Verify your email and setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication process",
                        15
                      );
                    }
                  }}
                >
                  Send Redeem Request
                </Button>
                <Button
                  type="ghost"
                  size="large"
                  onClick={() => setCryptoSelected(undefined)}
                >
                  Cancel
                </Button>
                <br />
              </Col>
            </Row>
          )}
        </>
      )}
    </Card>
  );
};

export function ErrorBoundary({ error }) {
  console.error(error);
  return (
    <Empty
      image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
      imageStyle={{
        height: 60,
      }}
      style={{ height: "100vh", verticalAlign: "middle" }}
      description={
        <span>
          <b>An error occurred, please bare with us</b>
          <br />
          <a href="/dashboard/">Back to Dashboard</a>
        </span>
      }
    >
      {error.toString()}
    </Empty>
  );
}

const VerifyComp = ({
  token,
  setVerifyModalVisible,
  twoFA,
  yubiKey,
  amount,
  network,
  walletVal,
  isMobile,
}) => {
  const submit = useSubmit();
  const [current, setCurrent] = React.useState(0);
  const [otp, setOTP] = React.useState();
  const [isSuccessful, setIsSuccessful] = React.useState();
  const [loading, setIsLoading] = React.useState(false);
  const [method, setMethod] = React.useState("2fa");
  const inputRef = React.useRef();

  React.useEffect(() => {
    return () => {
      setCurrent(0);
      setOTP();
      setIsSuccessful();
      setIsLoading(false);
      setMethod("2fa");
    };
  }, []);

  return (
    <>
      <Steps current={current}>
        <Step title="Authentication Method" key="1" />

        <Step title="Finish Authentication" key="2" />
      </Steps>
      {current === 0 && (
        <Row
          gutter={[16, 16]}
          style={{
            width: "100%",
            marginLeft: 0,
            marginRight: 0,
            paddingTop: 30,
            paddingBottom: 30,
          }}
        >
          <Col span={24}>
            <p>Verify transaction before proceeding</p>
          </Col>

          <Col xs={{ span: 24 }} lg={{ span: 24 }}>
            <label>
              Select preferred authentication method
              <Select
                defaultActiveFirstOption={true}
                defaultValue={method}
                style={{ minWidth: "100%" }}
                size="large"
                onChange={(value) => {
                  setMethod(value);
                  inputRef.current.focus();
                  inputRef.currentTarget.focus();
                }}
              >
                <Select.Option value="2fa" disabled={!twoFA}>
                  Google 2 Factor Authentication
                </Select.Option>
                <Select.Option value="yubikey" disabled={!yubiKey}>
                  Yubikey Authentication
                </Select.Option>
              </Select>
            </label>
            <br />
            <br />
            <label>
              <Input
                type="text"
                size="large"
                value={otp}
                onChange={(e) => setOTP(e.target.value)}
                autoFocus
                ref={inputRef}
                style={{ border: "1px solid black" }}
              />
              <span>
                <ExclamationCircleFilled style={{ color: "#00ADE6" }} />{" "}
                {method === "2fa"
                  ? "Please enter code from the Google Auth App"
                  : "Press the button on your YUBIKEY to activate it and continue."}
              </span>
            </label>
            <br />
            <br />
            <div
              style={{
                display: "flex",
                flexFlow: "row",
                alignItems: "center",
                justifyContent: "space-evenly",
                width: "60%",
              }}
            >
              <Button
                type="ghost"
                size="large"
                onClick={() => setVerifyModalVisible(false)}
              >
                CANCEL
              </Button>
              <Button
                type="primary"
                size="large"
                loading={loading}
                onClick={async () => {
                  setIsLoading(true);
                  process.env.NODE_ENV === "development"
                    ? console.log(otp, "OTP")
                    : null;
                  // Call api here and get back verification status
                  if (method === "2fa") {
                    const res = await verifyOTP(token, otp);
                    setIsSuccessful(res.data);
                    setIsLoading(false);
                  } else if (method === "yubikey") {
                    const res = await verifyYubikey(token, otp);
                    setIsSuccessful(res.data);
                    setIsLoading(false);
                  }
                  setIsLoading(false);
                  setCurrent((current) => current + 1);
                }}
              >
                {method === "2fa" ? "VERIFY 2FA CODE" : "VERIFY YUBIKEY"}
              </Button>
            </div>
          </Col>
        </Row>
      )}
      {current === 1 && (
        <Row
          gutter={[16, 16]}
          style={{
            width: "100%",
            marginLeft: 0,
            marginRight: 0,
            paddingTop: 30,
            paddingBottom: 30,
          }}
        >
          <Col
            xs={{ span: 24 }}
            lg={{ span: 24 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {isSuccessful && (
              <>
                <CheckCircleFilled
                  style={{
                    color: "#93DFF9",
                    fontSize: isMobile ? "10rem" : 400,
                  }}
                />
                <br />
                <span>Authentication Successful</span>
                <br />
                <span>
                  Carry on with the transaction your preferred method of
                  security authentication was successful
                </span>
                <br />
                <br />
                <Button
                  size="large"
                  type="primary"
                  onClick={() => {
                    // Call actual transaction
                    let formData = new FormData();
                    formData.append("amount", amount.toString());
                    formData.append("network", network.toLowerCase());
                    formData.append("address", walletVal.toString());
                    submit(formData, {
                      method: "post",
                      action: "/dashboard/withdraw?type=cNGN",
                      replace: false,
                    });

                    setVerifyModalVisible(false);
                  }}
                  block
                >
                  CONTINUE TRANSACTION
                </Button>
              </>
            )}
            {!isSuccessful && (
              <>
                <CloseCircleFilled
                  style={{ color: "red", fontSize: isMobile ? "10rem" : 400 }}
                />
                <br />
                <span>Authentication failed</span>
                <br />
                <span>
                  Attempt to authenticate your preferred security authentication
                  failed
                </span>
                <br />
                <br />
                <Button
                  size="large"
                  type="primary"
                  onClick={() => setCurrent((current) => current - 1)}
                  block
                >
                  RETRY
                </Button>
              </>
            )}
          </Col>
        </Row>
      )}
    </>
  );
};
