import { Alert, Button, Card, Col, DatePicker, List, Row, Table } from "antd";
import { useNavigate, useOutletContext } from "remix";
import { LineChart } from "~/components";

const { RangePicker } = DatePicker;
const dataSource = [
  {
    key: "1",
    type: "Crypto Deposit",
    amount: 10000000,
    date: "08/24/2022",
  },
  {
    key: "2",
    type: "Fiat Deposit",
    amount: 40000000,
    date: "08/24/2022",
  },
  {
    key: "3",
    type: "Fiat Redeem",
    amount: 120000000,
    date: "08/24/2022",
  },
  {
    key: "4",
    type: "cNGN Redeem",
    amount: 75000000,
    date: "08/24/2022",
  },
];

const spotDataSource = [
  {
    key: "1",
    coin: "cNGN",
    total: 10000000,
    available: 8000000,
  },
];

const columns = [
  {
    title: "Coin",
    dataIndex: "coin",
    key: "coin",
  },
  {
    title: "Total",
    dataIndex: "total",
    key: "total",
  },
  {
    title: "Available",
    dataIndex: "available",
    key: "available",
  },
];

export default function Overview() {
  const [loading, setLoading] = React.useState(true);
  const { user, balances } = useOutletContext();
  const navigate = useNavigate();
  const [chartData, setChartData] = React.useState([]);
  const [spotData, setSpotData] = React.useState(spotDataSource);
  const [summaryData, setSummaryData] = React.useState(dataSource);
  console.log(user, "User data");

  React.useEffect(() => {
    // Mutate the user transaction data into the chart data
    const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

    setSpotData(
      balances
        ?.filter(
          (val) => val["asset_type"] !== "native" && val["asset_code"] !== "BSC"
        )
        .map((val, i) => {
          return {
            key: i.toString(),
            coin: val["asset_type"] === "native" ? "XBN" : val["asset_code"],
            total: Number(val["balance"])
              .toFixed(2)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,")
              .toString(),
            available: Number(val["balance"])
              .toFixed(2)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,")
              .toString(),
          };
        })
    );

    setSummaryData(
      user?.transactions
        .filter((val) => val["status"] === "completed")
        .map((val, i) => {
          // Mutate transactions list and get useable details
          return {
            key: i.toString(),
            type:
              val["trx_type"] === "crypto_redeem" &&
              val["asset_symbol"] === "CNGN"
                ? "CNGN Withdrawal"
                : val["trx_type"] === "fiat_redeem" &&
                  val["asset_symbol"] === "NGN"
                ? "Fiat Redeem"
                : val["trx_type"] === "enaira_redeem" &&
                  val["asset_symbol"] === "ENGN"
                ? "ENaira Redeem"
                : val["trx_type"] === "fiat_deposit" &&
                  val["asset_symbol"] === "NGN"
                ? "Fiat Deposit"
                : val["trx_type"] === "crypto_deposit" &&
                  val["asset_symbol"] === "CNGN"
                ? "CNGN Deposit"
                : "ENaira Deposit",
            date: `${
              new Date(val["createdAt"])
                .toLocaleString("en-US", { timeZone: timezone })
                .split(",")[0]
            }`,
            amount: Number(val["amount"])
              .toFixed(1)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,")
              .toString(),
          };
        })
    );
  }, [balances]);

  return (
    <Row
      gutter={[16, 16]}
      style={{ width: "100%", marginLeft: 0, marginRight: 0, padding: 0 }}
    >
      <Col span={24}>
        {!user?.kyc_verified && (
          <Alert
            style={{ marginBottom: 15 }}
            type="warning"
            showIcon
            description={
              <>
                <span>Verify your KYC to continue with this page</span>
                <br />
                <br />
                <Button
                  size="small"
                  type="ghost"
                  onClick={() =>
                    navigate("/dashboard/account?action=kyc", { replace: true })
                  }
                >
                  ACCOUNT
                </Button>
              </>
            }
          />
        )}
        {!user?.two_factor_auth_enabled && (
          <Alert
            type="warning"
            showIcon
            description={
              <>
                <span>
                  Setup 2 factor authentication to carry out transactions. click
                  the button below to carry out authentication process
                </span>
                <br />
                <br />
                <Button
                  size="small"
                  type="ghost"
                  onClick={() =>
                    navigate("/dashboard/account?action=security", {
                      replace: true,
                    })
                  }
                >
                  ACCOUNT
                </Button>
              </>
            }
          />
        )}
      </Col>
      <Col span={24}>
        <Card hoverable style={{ borderRadius: 20 }}>
          <div
            style={{
              display: "flex",
              flexFlow: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <h1 style={{ display: "inline-block", margin: 0 }}>
              Transaction Count
            </h1>
            {/* <RangePicker size="large" onChange={(date, dateString) => console.log(date, dateString)} /> */}
          </div>

          <LineChart transactions={user?.transactions} />
        </Card>
      </Col>
      <Col xs={{ span: 24 }} lg={{ span: 12 }} style={{ height: "100%" }}>
        <Card
          hoverable
          style={{
            borderRadius: 20,
            background: "rgba(253, 253, 255, 0.7)",
            background:
              "-webkit-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
            background:
              "-moz-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
            background:
              "linear-gradient(to bottom right, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
          }}
        >
          <h1 style={{ display: "inline-block", margin: 0 }}>Spot</h1>
          <Table
            bordered={false}
            columns={columns}
            dataSource={spotData}
            style={{ background: "transparent" }}
          />
        </Card>
        {/* <Row gutter={16}  style={{height: "100%", marginLeft: 0, marginRight: 0, padding: 0}}> */}
        {/* <Col span={24}>
            <Card style={{borderRadius: 20}}>
              <h1 style={{display: "inline-block", margin: 0}}>Overview</h1>
              <PieChart />
            </Card>
          </Col> */}
        {/* <Col span={24} style={{height: "100%"}}>
           
          </Col>
        </Row> */}
      </Col>
      <Col xs={{ span: 24 }} lg={{ span: 12 }}>
        <Card
          hoverable
          title="History"
          extra={<a href="/dashboard/history">See all</a>}
          style={{
            borderRadius: 20,
            background: "rgba(253, 253, 255, 0.7)",
            background:
              "-webkit-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
            background:
              "-moz-linear-gradient(top left, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
            background:
              "linear-gradient(to bottom right, rgba(255, 255, 255, 1.0) 10%, rgba(253, 253, 255, 0.7) 30%, rgba(215, 246, 255, 0.5) 60%)",
          }}
        >
          <List
            itemLayout="horizontal"
            dataSource={summaryData}
            renderItem={(item, index) => (
              <List.Item
                actions={[
                  <span
                    style={{
                      color:
                        item?.type?.match("Deposit") !== null
                          ? "#2BB596"
                          : item?.type?.match("Withdrawal") !== null
                          ? "red"
                          : item?.type?.match("Redeem") !== null
                          ? "red"
                          : "grey",
                    }}
                  >
                    {item.amount}
                  </span>,
                  <span style={{ color: "#6F6C99" }}>{item.date}</span>,
                ]}
              >
                {/* <Skeleton avatar title={false} loading={false} active> */}
                <List.Item.Meta
                  avatar={<img src="/token_logo.png" width={30} height={30} />}
                  title={item.type}
                />
                {/* </Skeleton> */}
              </List.Item>
            )}
          />
        </Card>
      </Col>
    </Row>
  );
}
