import {
  CheckCircleFilled,
  CloseCircleFilled,
  ExclamationCircleFilled,
} from "@ant-design/icons";
import {
  Alert,
  Button,
  Card,
  Col,
  Empty,
  Input,
  message,
  notification,
  Row,
  Select,
  Steps,
  Tooltip,
} from "antd";
import {
  Form,
  redirect,
  useActionData,
  useLoaderData,
  useLocation,
  useNavigate,
  useOutletContext,
  useSubmit,
  useTransition,
} from "remix";
import { ENairaCardRedeem, GodModal } from "~/components";
import { userPrefs } from "~/cookies";
import {
  getBankList,
  getFiatRedeemFee,
  redeemENGN,
  redeemFiat,
  verifyOTP,
  verifyYubikey,
} from "~/utils";

const { Step } = Steps;

export const action = async ({ request }) => {
  // await new Promise(res => setTimeout(res, 1000));
  const cookieHeader = request.headers.get("Cookie");
  const cookie = (await userPrefs.parse(cookieHeader)) || {};
  let url = new URL(request.url);
  let redeem_type = url.searchParams.get("type");
  const formData = await request.formData();

  if (redeem_type === "fiat") {
    const amount = formData.get("amount");
    const bankCode = formData.get("bankCode");
    const accountNumber = formData.get("accountNumber");
    const network = formData.get("network");
    // const markdown = formData.get("markdown");
    console.log("Submitted value", amount, bankCode, accountNumber);

    const fiatErrors = {};
    if (!amount) {
      fiatErrors.hasError = true;
      fiatErrors.message = "Amount must be entered";
      return fiatErrors;
    }

    if (Number.parseInt(amount) < 50) {
      fiatErrors.hasError = true;
      fiatErrors.message = "Minimum deposit amount must be NGN 30,000,000";
      return fiatErrors;
    }

    // Call the endpoint to submit amount and get flutterwave link to begin payment
    const res = await redeemFiat(
      cookie.token,
      amount,
      accountNumber,
      bankCode,
      network
    );
    if (res.error) {
      return res;
    }

    return redirect("/dashboard/");
  }
};

export async function loader({ request }) {
  const cookieHeader = request.headers.get("Cookie");
  const cookie = (await userPrefs.parse(cookieHeader)) || {};

  const res = await getBankList(cookie?.token);
  // console.log("🚀 ~ file: redeem.jsx ~ line 119 ~ res", res);

  return { cookie, bankList: res["data"] };
}

export default function Redeem() {
  const { user, balances, isMobile } = useOutletContext();
  const navigate = useNavigate();
  const [selected, setSelected] = React.useState();
  const { cookie, bankList } = useLoaderData();

  const { pathname } = useLocation();
  const { two_factor_auth_enabled, email_verified } = user;

  React.useEffect(() => {
    //Check if the user KYC is verified, if not redirect to /dashboard/
    // if(!user?.kyc_verified) {
    //   message.warn("Verify your KYC to continue with this page", 15);
    //   navigate("/dashboard/");
    // }
  });

  // Check if the user is verified and 2FA Enabled
  React.useEffect(() => {
    // if(!two_factor_auth_enabled && !email_verified) {
    //   message.warn("Verify your email and setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication processes", 15);
    // } else if(!two_factor_auth_enabled) {
    //   message.warn("Setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication processes", 15);
    // } else if(!email_verified) {
    //   message.warn("Verify your email address", 15);
    // }
    return () => {};
  }, [pathname]);

  return (
    <Row
      gutter={[16, 16]}
      style={{ marginLeft: 0, marginRight: 0, padding: 0, width: "100%" }}
    >
      <Col span={24}>
        {!user?.kyc_verified && (
          <Alert
            style={{ marginBottom: 15 }}
            type="warning"
            showIcon
            description={
              <>
                <span>Verify your KYC to continue with this page</span>
                <br />
                <br />
                <Button
                  size="small"
                  type="ghost"
                  onClick={() =>
                    navigate("/dashboard/account?action=kyc", { replace: true })
                  }
                >
                  ACCOUNT
                </Button>
              </>
            }
          />
        )}
        {!two_factor_auth_enabled && (
          <Alert
            type="warning"
            showIcon
            description={
              <>
                <span>
                  Setup 2 factor authentication to carry out transactions. click
                  the button below to carry out authentication process
                </span>
                <br />
                <br />
                <Button
                  size="small"
                  type="ghost"
                  onClick={() =>
                    navigate("/dashboard/account?action=security", {
                      replace: true,
                    })
                  }
                >
                  ACCOUNT
                </Button>
              </>
            }
          />
        )}
      </Col>
      <Col span={24}>
        <TokenRedeemCard
          user={user}
          isMobile={isMobile}
          selected={selected}
          setSelected={setSelected}
          bscBalance={
            balances.filter((val) => val.asset_code === "CNGN (BSC)")[0] || {
              balance: 0,
            }
          }
          xbnBalance={
            balances.filter((val) => val.asset_code === "CNGN (XBN)")[0] || {
              balance: 0,
            }
          }
          cookie={cookie}
        />
      </Col>
      <Col span={24}>
        <FiatRedeemCard
          user={user}
          isMobile={isMobile}
          selected={selected}
          setSelected={setSelected}
          balance={
            balances.filter((val) => val.asset_code === "CNGN")[0] || {
              balance: 0,
            }
          }
          cookie={cookie}
          banks={bankList}
        />
      </Col>
    </Row>
  );
}

// Fiat deposit component
const FiatRedeemCard = ({
  selected,
  setSelected,
  balance,
  cookie,
  isMobile,
  user,
  banks,
}) => {
  const networkList = [
    {
      title: "Bantu (XBN)",
      val: "XBN",
      prefix: "/bantu_network_logo.png",
      disabled: false,
    },
    {
      title: "Binance Smart Chain (BSC)",
      val: "BSC",
      prefix: "/binance_network_logo.png",
      disabled: false,
    },
    {
      title: "Solana (SOL)",
      val: "SOL",
      prefix: "/solana_network_logo.svg",
      disabled: true,
    },
    {
      title: "Ethereum (ETH)",
      val: "ETH",
      prefix: "/ethereum_network_logo.svg",
      disabled: true,
    },
  ];

  const sampleBankList = [{ name: "Guarantee Trust Bank", code: "004" }];
  // const submit = useSubmit();
  const transition = useTransition();
  const actionData = useActionData();
  const formRef = React.useRef();
  const [bankList, setBankList] = React.useState(banks || sampleBankList);
  const [bankVal, setBankVal] = React.useState(
    banks[0].code || bankList[0].code
  );
  const [receivable, setReceivable] = React.useState(0);
  const [amount, setAmount] = React.useState(0);
  const [flFee, setFlFee] = React.useState(0);
  const [accountNumber, setAccountNumber] = React.useState("");
  const [isVerifyModalVisible, setVerifyModalVisible] = React.useState(false);
  const [network, setNetwork] = React.useState(networkList[0].val);

  const { two_factor_auth_enabled, yubikey_activated, email_verified } = user;

  if (actionData?.hasError) {
    message.error(actionData?.message);
  }

  React.useEffect(async () => {
    console.log(network, "Network switch");
    setAmount();
    return () => {
      setNetwork(networkList[0].val);
      setAmount();
    };
  }, [network]);

  return (
    <Card
      style={{ borderRadius: 20 }}
      bodyStyle={{
        paddingLeft: isMobile ? 10 : 20,
        paddingRight: isMobile ? 10 : 20,
      }}
    >
      {(typeof selected === "undefined" || selected !== "fiat-redeem") && (
        <>
          <div
            style={{
              display: "flex",
              flexFlow: "row",
              justifyContent: "space-between",
            }}
          >
            <h1>Fiat Redeem</h1>
            <img src="/flutterwave-logo.png" width={160} height={30} />
          </div>
          <span>
            <ExclamationCircleFilled style={{ color: "#00ADE6" }} /> Select this
            option if you intend to redeem fiat to your Bank account
          </span>
          <br />
          <br />

          <Button
            type="primary"
            size="large"
            onClick={async () => {
              setSelected("fiat-redeem");

              // setBankList(res?.data);
              // setBankVal(bankList[0].code);
            }}
          >
            Select
          </Button>
        </>
      )}
      {selected === "fiat-redeem" && (
        <>
          <div
            style={{
              display: "flex",
              flexFlow: "row",
              justifyContent: "space-between",
            }}
          >
            <h1>Fiat Redeem</h1>
            <img src="/flutterwave-logo.png" width={160} height={30} />
          </div>
          <span>
            <ExclamationCircleFilled style={{ color: "#00ADE6" }} /> Select this
            option if you intend to redeem fiat to your Bank account
          </span>
          <br />
          <br />

          <Row
            gutter={[16, 16]}
            style={{ width: "100%", marginLeft: 0, marginRight: 0 }}
          >
            <Col span={24}>
              <label>
                Select Network
                <Select
                  defaultActiveFirstOption={true}
                  defaultValue={network}
                  onChange={(value) => setNetwork(value)}
                  style={{ minWidth: "100%" }}
                  size="large"
                >
                  {networkList.map((val, i) => (
                    <Select.Option
                      key={i.toString()}
                      value={val.val}
                      disabled={val.disabled}
                    >
                      <img
                        src={val.prefix}
                        width={12}
                        height={12}
                        alt={val.val}
                      />{" "}
                      {val.title}
                    </Select.Option>
                  ))}
                </Select>
              </label>
            </Col>
            <Col span={24}>
              <label>
                Amount to redeem
                <br />
                <Input
                  value={amount}
                  prefix={"NGN"}
                  onChange={async (e) => {
                    if (e.target.value === "") {
                      setReceivable(0);
                      setAmount();
                      setFlFee(0);
                      return;
                    }
                    setAmount(e.target.value);

                    const { data, status, error } = await getFiatRedeemFee(
                      cookie?.token,
                      amount,
                      "NGN"
                    );
                    console.log(data, status, error, "Fee response return");
                    let fee = data[0]?.fee;
                    if (data) {
                      setFlFee(fee);
                      setReceivable(
                        parseFloat(e.target.value) -
                          parseFloat(e.target.value) * 0.001 -
                          parseFloat(fee)
                      );
                    }
                  }}
                  size="large"
                  type="number"
                  placeholder="Minimum Redeemable: 30,000,000"
                  name="amount"
                  id="amount"
                />
              </label>
              <br />
              <span>
                Swap fee: <b>0.001</b>
                <Tooltip
                  placement="right"
                  title="Wrap token charges 0.001 for direct deposit swap from fiat (NGN) to wrapped token (cNGN)"
                >
                  {" "}
                  <ExclamationCircleFilled style={{ color: "#FFC700" }} />
                </Tooltip>
              </span>
              <br />
              <span>
                Receivable Amount: <b>{receivable}</b>{" "}
                <Tooltip
                  placement="right"
                  title="Receivable amount into your wallet when swap fee + flutterwave deposit fee is deducted"
                >
                  {" "}
                  <ExclamationCircleFilled style={{ color: "#FFC700" }} />
                </Tooltip>
              </span>
            </Col>
            <Col xs={{ span: 24 }} lg={{ span: 12 }}>
              <label>
                Select Bank
                <Select
                  name="bankCode"
                  defaultActiveFirstOption
                  placeholder="Select bank to redeem to"
                  style={{ width: "100%" }}
                  onChange={(value) => setBankVal(value)}
                  size="large"
                >
                  {bankList &&
                    bankList.map((val, i) => (
                      <Select.Option key={val?.id} value={val.code}>
                        {val.name}
                      </Select.Option>
                    ))}
                </Select>
              </label>
              <br />
              <span>
                Flutterwave fee: <b>{flFee} NGN</b>{" "}
                <Tooltip
                  placement="right"
                  title="Flutterwave charges 1.4% (0.014) for fiat (NGN) deposit"
                >
                  {" "}
                  <ExclamationCircleFilled style={{ color: "#FFC700" }} />
                </Tooltip>
              </span>
            </Col>
            <Col xs={{ span: 24 }} lg={{ span: 12 }}>
              <label>
                Account number
                <Input
                  value={accountNumber}
                  onChange={(e) => setAccountNumber(e.target.value)}
                  type="text"
                  name="account-number"
                  id="account-number"
                  size="large"
                  style={{ border: "1px solid rgba(0,0,0,.12)" }}
                />
              </label>
              <GodModal
                isMobile={isMobile}
                title="Verify Transaction"
                isModalVisible={isVerifyModalVisible}
                setGodModalVisible={setVerifyModalVisible}
                handleOk={() => {}}
              >
                <VerifyComp
                  isMobile={isMobile}
                  token={cookie?.token}
                  setVerifyModalVisible={setVerifyModalVisible}
                  twoFA={two_factor_auth_enabled}
                  yubiKey={yubikey_activated}
                  amount={receivable}
                  bankVal={bankVal}
                  accountNumber={accountNumber}
                  network={network}
                />
              </GodModal>
            </Col>
          </Row>
          <br />
          <br />

          <Button
            type="ghost"
            size="large"
            onClick={() => setSelected(undefined)}
            style={{ marginRight: 10 }}
          >
            Cancel
          </Button>
          <Button
            type="primary"
            size="large"
            onClick={() => {
              if (!two_factor_auth_enabled && !email_verified) {
                return message.warn(
                  "Verify your email and setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication processes",
                  15
                );
              } else if (!two_factor_auth_enabled) {
                return message.warn(
                  "Setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication processes",
                  15
                );
              } else if (!email_verified) {
                return message.warn("Verify your email address", 15);
              }
              // check balance of the user before sending request

              if (balance.balance <= 0) {
                return message.warn(
                  "Your balance is insufficient for redeem, make a deposit first",
                  6
                );
              }

              // Carry out the verification before withdrawal
              if (two_factor_auth_enabled || yubikey_activated) {
                // Show Godmodal with both path to verify
                setVerifyModalVisible(true);
              }
            }}
            loading={
              transition.state === "submitting" ||
              transition.state === "loading"
            }
          >
            Redeem Fiat
          </Button>
          <br />
          <br />
          <span>
            <ExclamationCircleFilled style={{ color: "red" }} /> Charges include{" "}
            <b>0.001</b> base fee + third party withdrawal fee
          </span>
          <br />
        </>
      )}
    </Card>
  );
};

// Crypto deposit component
const TokenRedeemCard = ({
  selected,
  setSelected,
  xbnBalance,
  bscBalance,
  isMobile,
  user,
  cookie,
}) => {
  const tokenList = [
    { code: "cNGN", disabled: false, prefix: "/icon-512x512.png" },
    { code: "cGHC", disabled: true, prefix: "/icon-512x512.png" },
    { code: "cXOF", disabled: true, prefix: "/icon-512x512.png" },
  ];
  const cbdcList = [
    { code: "eNAIRA", disabled: false, prefix: "/e-naira-logo.png" },
    { code: "eCEDI", disabled: true, prefix: "/e-naira-logo.png" },
    { code: "eFRANC", disabled: true, prefix: "/e-naira-logo.png" },
    { code: "ePOUND", disabled: true, prefix: "/e-naira-logo.png" },
  ];
  const networkList = [
    {
      title: "Bantu (XBN)",
      val: "XBN",
      prefix: "/bantu_network_logo.png",
      disabled: false,
    },
    {
      title: "Binance Smart Chain (BSC)",
      val: "BSC",
      prefix: "/binance_network_logo.png",
      disabled: false,
    },
    {
      title: "Solana (SOL)",
      val: "SOL",
      prefix: "/solana_network_logo.svg",
      disabled: true,
    },
    {
      title: "Ethereum (ETH)",
      val: "ETH",
      prefix: "/ethereum_network_logo.svg",
      disabled: true,
    },
  ];

  const [cryptoSelected, setCryptoSelected] = React.useState();
  const [amount, setAmount] = React.useState();
  const [network, setNetwork] = React.useState(networkList[0].val);
  const [paymentCode, setPaymentCode] = React.useState();
  const [eNGNRedeemLoading, setENGNRedeemLoading] = React.useState(false);
  const submit = useSubmit();
  const transition = useTransition();
  const actionData = useActionData();
  const cngnFormRef = React.useRef();
  const { two_factor_auth_enabled, email_verified } = user;

  console.log("Action details");

  if (actionData?.cngnHasError) {
    message.error(actionData?.cngnMessage);
  }

  if (actionData?.error) {
    message.error(actionData?.message);
  }

  return (
    <Card
      style={{ borderRadius: 20 }}
      bodyStyle={{
        paddingLeft: isMobile ? 10 : 20,
        paddingRight: isMobile ? 10 : 20,
      }}
    >
      {(typeof selected === "undefined" || selected !== "crypto-deposit") && (
        <>
          <div
            style={{
              display: "flex",
              flexFlow: "row",
              justifyContent: "space-between",
            }}
          >
            <h1>E-naira Redeem</h1>
            {/* <img src="/flutterwave-logo.png" width={160} height={30} /> */}
          </div>
          <span>
            <ExclamationCircleFilled style={{ color: "#00ADE6" }} /> Select this
            option if you intend to redeem eNAIRA{" "}
          </span>
          <br />
          <br />

          <Button
            type="primary"
            size="large"
            onClick={() => setSelected("crypto-deposit")}
          >
            Select
          </Button>
        </>
      )}
      {selected === "crypto-deposit" && (
        <>
          <div
            style={{
              display: "flex",
              flexFlow: "row",
              justifyContent: "space-between",
            }}
          >
            <h1>E-naira Redeem</h1>
            {/* <img src="/flutterwave-logo.png" width={160} height={30} /> */}
          </div>
          <span>
            <ExclamationCircleFilled style={{ color: "#00ADE6" }} /> Select this
            option if you intend to redeem eNAIRA{" "}
          </span>
          <br />
          <br />

          {typeof cryptoSelected === "undefined" && (
            <>
              <Row
                gutter={16}
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  width: isMobile ? "100%" : "70%",
                }}
              >
                {/* <Col span={12}>
                <CNGNCardRedeem isMobile={isMobile} tapHandler={() => setCryptoSelected('cNGN')} />
              </Col> */}
                <Col span={24}>
                  <ENairaCardRedeem
                    isMobile={isMobile}
                    tapHandler={() => setCryptoSelected("eNGN")}
                  />
                </Col>
              </Row>
              <br />
              <br />
              <Button
                type="ghost"
                size="large"
                onClick={() => setSelected(undefined)}
                style={{ marginRight: 10 }}
              >
                Cancel
              </Button>
            </>
          )}
          {cryptoSelected === "cNGN" && (
            <Form
              method="post"
              action="/dashboard/redeem?type=cNGN"
              ref={cngnFormRef}
            >
              <Row
                gutter={[16, 16]}
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  width: isMobile ? "100%" : "60%",
                }}
              >
                <Col span={24}>
                  <label>
                    Select Token to Redeem
                    <Select
                      defaultActiveFirstOption={true}
                      defaultValue={tokenList[0].code}
                      style={{ minWidth: "100%" }}
                      size="large"
                    >
                      {tokenList.map((val, i) => (
                        <Select.Option
                          key={i.toString()}
                          value={val.code}
                          disabled={val.disabled}
                        >
                          <img
                            src={val.prefix}
                            width={12}
                            height={12}
                            alt={val.code}
                          />{" "}
                          {val.code}
                        </Select.Option>
                      ))}
                    </Select>
                  </label>
                </Col>
                <Col span={24}>
                  <label>
                    Select Network
                    <Select
                      defaultActiveFirstOption={true}
                      defaultValue={networkList[0].val}
                      style={{ minWidth: "100%" }}
                      size="large"
                    >
                      {networkList.map((val, i) => (
                        <Select.Option
                          key={i.toString()}
                          value={val.val}
                          disabled={val.disabled}
                        >
                          {val.title}
                        </Select.Option>
                      ))}
                    </Select>
                  </label>
                </Col>
                <Col span={24}>
                  <label>
                    Amount
                    <Input
                      type="number"
                      size="large"
                      name="cngn-amount"
                      suffix={
                        <Button
                          type="primary"
                          onClick={() =>
                            setMaxAmount(Number(balance.balance).toFixed(0))
                          }
                        >
                          MAX
                        </Button>
                      }
                    />
                    <h4 style={{ textAlign: "right" }}>
                      cNGN (BSC) Balance:{" "}
                      {Number(bscBalance.balance)
                        .toFixed(2)
                        .replace(/\d(?=(\d{3})+\.)/g, "$&,")}{" "}
                      - cNGN (XBN) Balance:{" "}
                      {Number(xbnBalance.balance)
                        .toFixed(2)
                        .replace(/\d(?=(\d{3})+\.)/g, "$&,")}
                    </h4>
                  </label>
                </Col>
                <Col span={24}>
                  <label>
                    Wallet Address
                    <Input
                      size="large"
                      type="text"
                      name="address"
                      style={{ border: "1px solid rgba(0,0,0,.12)" }}
                    />
                  </label>
                  <br />
                  <br />
                  <Button
                    type="primary"
                    size="large"
                    style={{ marginRight: 10 }}
                    onClick={() => {
                      if (!two_factor_auth_enabled || !email_verified) {
                        return message.warn(
                          "Verify your email an setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication processes",
                          15
                        );
                      }
                      submit(cngnFormRef.current, { replace: false });
                    }}
                    loading={transition.state === "submitting"}
                  >
                    Send Redeem Request
                  </Button>
                  <Button
                    type="ghost"
                    size="large"
                    onClick={() => setCryptoSelected(undefined)}
                  >
                    Cancel
                  </Button>
                  <br />
                </Col>
              </Row>
            </Form>
          )}
          {cryptoSelected === "eNGN" && (
            <Row
              gutter={[16, 16]}
              style={{
                marginLeft: 0,
                marginRight: 0,
                width: isMobile ? "100%" : "60%",
              }}
            >
              <Col span={24}>
                <label>
                  Select Token to Redeem:
                  <Select
                    defaultActiveFirstOption={true}
                    defaultValue={cbdcList[0].code}
                    style={{ minWidth: "100%" }}
                    size="large"
                  >
                    {cbdcList.map((val, i) => (
                      <Select.Option
                        key={i.toString()}
                        value={val.code}
                        disabled={val.disabled}
                      >
                        <img
                          src={val.prefix}
                          width={12}
                          height={12}
                          alt={val.code}
                        />{" "}
                        {val.code}
                      </Select.Option>
                    ))}
                  </Select>
                </label>
              </Col>

              <Col span={24}>
                <label>
                  Select Network (to redeem from):
                  <Select
                    defaultActiveFirstOption={true}
                    onChange={(value) => setNetwork(value)}
                    defaultValue={network}
                    style={{ minWidth: "100%" }}
                    size="large"
                  >
                    {networkList.map((val, i) => (
                      <Select.Option
                        key={i.toString()}
                        value={val.val}
                        disabled={val.disabled}
                      >
                        <img
                          src={val.prefix}
                          width={12}
                          height={12}
                          alt={val.title}
                        />{" "}
                        {val.title}
                      </Select.Option>
                    ))}
                  </Select>
                </label>
              </Col>

              <Col span={24}>
                <label>
                  Amount:
                  <Input
                    value={amount}
                    onChange={(e) => setAmount(e.target.value)}
                    size="large"
                    type="number"
                    suffix={
                      <Button
                        type="primary"
                        onClick={() =>
                          setAmount(Number(balance.balance).toFixed(0))
                        }
                      >
                        MAX
                      </Button>
                    }
                  />
                  <h4 style={{ textAlign: "right" }}>
                    cNGN (BSC) Balance:{" "}
                    {Number(bscBalance.balance)
                      .toFixed(2)
                      .replace(/\d(?=(\d{3})+\.)/g, "$&,")}{" "}
                    - cNGN (XBN) Balance:{" "}
                    {Number(xbnBalance.balance)
                      .toFixed(2)
                      .replace(/\d(?=(\d{3})+\.)/g, "$&,")}
                  </h4>
                </label>
              </Col>
              <Col span={24}>
                <label>
                  Your Payment code
                  <Input
                    size="large"
                    type="text"
                    value={paymentCode}
                    onChange={(e) => setPaymentCode(e.target.value)}
                    style={{ border: "1px solid rgba(0,0,0,.12)" }}
                  />
                  <span>
                    <ExclamationCircleFilled style={{ color: "#00ADE6" }} />{" "}
                    Must be a valid eNaira Speed wallet merchant or personal
                    payment code.
                  </span>
                </label>
                <br />
                <br />

                <Button
                  loading={eNGNRedeemLoading}
                  type="primary"
                  size="large"
                  style={{ marginRight: 10 }}
                  onClick={async () => {
                    if (!two_factor_auth_enabled || !email_verified) {
                      return message.warn(
                        "Verify your email an setup 2 factor authentication to carry out transactions. Go to Account settings in the top right corner to carry out authentication process",
                        15
                      );
                    }

                    setENGNRedeemLoading(true);

                    // Make API call here
                    const { data, message, status } = await redeemENGN(
                      cookie?.token,
                      amount,
                      paymentCode,
                      network
                    );
                    console.log(data, "Enaira deposit data");
                    setENGNRedeemLoading(false);

                    const key = `open-enaira-redeem${Date.now()}`;
                    const btn = (
                      <Button
                        type="primary"
                        size="medium"
                        onClick={() => {
                          if (status !== "success") {
                            notification.close(key);
                          } else {
                            notification.close(key);
                            navigate("/dashboard/");
                          }
                        }}
                      >
                        {status !== "success" ? "Ok" : "Dashboard"}
                      </Button>
                    );
                    notification.open({
                      message: `Transaction ${status.toUpperCase()}`,
                      description: `${message}`,
                      btn,
                      key,
                      onClose: () => {
                        console.log("Notification was closed");
                      },
                    });
                  }}
                >
                  Send Redeem Request
                </Button>
                <Button
                  type="ghost"
                  size="large"
                  onClick={() => setCryptoSelected(undefined)}
                >
                  Cancel
                </Button>
                <br />
              </Col>
            </Row>
          )}
        </>
      )}
    </Card>
  );
};

export function ErrorBoundary({ error }) {
  console.error(error);
  return (
    <Empty
      image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
      imageStyle={{
        height: 60,
      }}
      style={{ height: "100vh", verticalAlign: "middle" }}
      description={
        <span>
          <b>An error occurred, please bare with us</b>
          <br />
          <a href="/dashboard/">Back to Dashboard</a>
        </span>
      }
    >
      {error.toString()}
    </Empty>
  );
}

const VerifyComp = ({
  token,
  setVerifyModalVisible,
  twoFA,
  yubiKey,
  amount,
  bankVal,
  accountNumber,
  network,
  isMobile,
}) => {
  const { cookie } = useLoaderData();
  const submit = useSubmit();
  const [current, setCurrent] = React.useState(0);
  const [otp, setOTP] = React.useState();
  const [isSuccessful, setIsSuccessful] = React.useState();
  const [loading, setIsLoading] = React.useState(false);
  const [method, setMethod] = React.useState("2fa");
  const inputRef = React.useRef();

  console.log(token, cookie, "Authorization header");

  return (
    <>
      <Steps current={current}>
        <Step title="Authentication Method" key="1" />

        <Step title="Finish Authentication" key="2" />
      </Steps>
      {current === 0 && (
        <Row
          gutter={[16, 16]}
          style={{
            width: "100%",
            marginLeft: 0,
            marginRight: 0,
            paddingTop: 30,
            paddingBottom: 30,
          }}
        >
          <Col span={24}>
            <p>Verify transaction before proceeding</p>
          </Col>

          <Col xs={{ span: 24 }} lg={{ span: 24 }}>
            <label>
              Select preferred authentication method
              <Select
                defaultActiveFirstOption={true}
                defaultValue={method}
                style={{ minWidth: "100%" }}
                size="large"
                onChange={(value) => {
                  setMethod(value);
                  inputRef.current.focus();
                  inputRef.currentTarget.focus();
                }}
              >
                <Select.Option value="2fa" disabled={!twoFA}>
                  Google 2 Factor Authentication
                </Select.Option>
                <Select.Option value="yubikey" disabled={!yubiKey}>
                  Yubikey Authentication
                </Select.Option>
              </Select>
            </label>
            <br />
            <br />
            <label>
              <Input
                type="text"
                size="large"
                value={otp}
                onChange={(e) => setOTP(e.target.value)}
                autoFocus
                ref={inputRef}
                style={{ border: "1px solid black" }}
              />
              <span>
                <ExclamationCircleFilled style={{ color: "#00ADE6" }} />{" "}
                {method === "2fa"
                  ? "Please enter code from the Google Auth App"
                  : "Press the button on your YUBIKEY to activate it and continue."}
              </span>
            </label>
            <br />
            <br />
            <div
              style={{
                display: "flex",
                flexFlow: "row",
                alignItems: "center",
                justifyContent: "space-evenly",
                width: "60%",
              }}
            >
              <Button
                type="ghost"
                size="large"
                onClick={() => setVerifyModalVisible(false)}
              >
                CANCEL
              </Button>
              <Button
                type="primary"
                size="large"
                loading={loading}
                onClick={async () => {
                  setIsLoading(true);
                  process.env.NODE_ENV === "development"
                    ? console.log(otp, "OTP")
                    : null;
                  // Call api here and get back verification status
                  if (method === "2fa") {
                    const res = await verifyOTP(cookie?.token || token, otp);
                    setIsSuccessful(res.data);
                    setIsLoading(false);
                  } else if (method === "yubikey") {
                    const res = await verifyYubikey(
                      cookie?.token || token,
                      otp
                    );
                    setIsSuccessful(res.data);
                    setIsLoading(false);
                  }
                  setIsLoading(false);
                  setCurrent((current) => current + 1);
                }}
              >
                {method === "2fa" ? "VERIFY 2FA CODE" : "VERIFY YUBIKEY"}
              </Button>
            </div>
          </Col>
        </Row>
      )}
      {current === 1 && (
        <Row
          gutter={[16, 16]}
          style={{
            width: "100%",
            marginLeft: 0,
            marginRight: 0,
            paddingTop: 30,
            paddingBottom: 30,
          }}
        >
          <Col
            xs={{ span: 24 }}
            lg={{ span: 24 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {isSuccessful && (
              <>
                <CheckCircleFilled
                  style={{
                    color: "#93DFF9",
                    fontSize: isMobile ? "10rem" : 400,
                  }}
                />
                <br />
                <span>Authentication Successful</span>
                <br />
                <span>
                  Carry on with the transaction your preferred method of
                  security authentication was successful
                </span>
                <br />
                <br />
                <Button
                  size="large"
                  type="primary"
                  onClick={() => {
                    // Call actual transaction
                    var formData = new FormData();
                    formData.append("amount", amount.toString());
                    formData.append("bankCode", bankVal.toString());
                    formData.append("accountNumber", accountNumber.toString());
                    formData.append("network", network.toLowerCase());

                    submit(formData, {
                      method: "post",
                      action: "/dashboard/redeem?type=fiat",
                      replace: true,
                    });

                    setVerifyModalVisible(false);
                  }}
                  block
                >
                  CONTINUE TRANSACTION
                </Button>
              </>
            )}
            {!isSuccessful && (
              <>
                <CloseCircleFilled
                  style={{ color: "red", fontSize: isMobile ? "10rem" : 400 }}
                />
                <br />
                <span>Authentication failed</span>
                <br />
                <span>
                  Attempt to authenticate your preferred security authentication
                  failed
                </span>
                <br />
                <br />
                <Button
                  size="large"
                  type="primary"
                  onClick={() => setCurrent((current) => current - 1)}
                  block
                >
                  RETRY
                </Button>
              </>
            )}
          </Col>
        </Row>
      )}
    </>
  );
};
