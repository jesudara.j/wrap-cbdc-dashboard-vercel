import { BellFilled, CloseOutlined, DownOutlined, MenuOutlined, MoreOutlined, UserOutlined } from "@ant-design/icons";
import { Avatar, Badge, Col, Dropdown, Empty, Grid, Layout, Menu, Row, Tooltip } from "antd";
import {
  QueryClient,
  QueryClientProvider
} from "react-query";
import { Link, Outlet, redirect, useCatch, useLoaderData, useLocation, useMatches } from "remix";
import { BalanceCard, DepositCard, RedeemCard } from "~/components";
import { userPrefs } from "~/cookies";
import { DashboardIcon, HelpIcon, TradeIcon, WalletIcon, WithdrawIcon } from "~/icons";
import { getUserDetails } from "~/utils";


const queryClient = new QueryClient();
const { Sider, Header, Footer, Content } = Layout;
const { useBreakpoint } = Grid;
const menu = (
  <Menu style={{minWidth: 150}}>
    <Menu.Item key="1">
      <a target="_self" rel="noopener noreferrer" href="/dashboard/account">
        Account
      </a>
    </Menu.Item>
    <Menu.Item key="2">
      <a target="_blank" rel="noopener noreferrer" href="https://wrapcbdc.com/faq">
        Help
      </a>
    </Menu.Item>
    {/* <Menu.Item key="3">
      <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
        Settings
      </a>
    </Menu.Item> */}
    <Menu.Item danger key="3">
      <a target="_self" rel="noopener noreferrer" href="/logout">
        Logout
      </a>
    </Menu.Item>
  </Menu>
);
// export const links = () => {
//   return [{ rel: "stylesheet", href: adminStyles }];
// };

export const loader = async ({ request }) => {
  const cookieHeader = request.headers.get("Cookie");
  const cookie = (await userPrefs.parse(cookieHeader)) || {};

  const { token } = cookie;
  if (!token || typeof token === "undefined") {
    return redirect("/");
  }

  return await getUserDetails(cookie.token);
};

export default function Dashboard() {
  const screens = useBreakpoint();
  const loadData = useLoaderData();
  const locationInfo = useLocation();
  const matches = useMatches();
  const { pathname } = locationInfo;
  const [menuKey, setMenuKey] = React.useState(pathname.includes('deposit')? 'deposit' : pathname.includes('redeem')? 'redeem' : pathname.includes('history')? 'history' : pathname.includes('help')? 'help' : pathname.includes('account')? 'account' : pathname.includes('withdraw')? 'withdraw' : 'overview')
  const [isMobile, setIsMobile] = React.useState(!screens.xxl && !screens.xl && !screens.lg); //On mobile default collapsed should be true
  const [collapsed, setCollapsed] = React.useState(isMobile); //On mobile default collapsed should be true
  const [isSecVisible, setIsSecVisible] = React.useState(false); //On mobile default collapsed should be true

  console.log(loadData);
  const { user, balances } = loadData?.data;
  const context = { user, balances, screens, isMobile };



  React.useEffect(() => {

    // if(screens.xxl || screens.xl || screens.lg) {
    //   setCollapsed(false);
    // } else {
    //   setCollapsed(true);
    // }
    setIsMobile(!screens.xxl && !screens.xl && !screens.lg);
    // setCollapsed(isMobile);
    
    setMenuKey(matches[matches.length - 1]?.pathname.includes('deposit')? 'deposit' : matches[matches.length - 1]?.pathname.includes('redeem')? 'redeem' : matches[matches.length - 1]?.pathname.includes('history')? 'history' : matches[matches.length - 1]?.pathname.includes('help')? 'help' : matches[matches.length - 1]?.pathname.includes('account')? 'account' : matches[matches.length - 1]?.pathname.includes('withdraw')? 'withdraw' : 'overview');

    return () => console.warn('Dashboard unmounted');
  }, [screens, matches, pathname]);

  
  // console.table(screens);
  return (
    <QueryClientProvider client={queryClient}>

      <Layout style={{ minHeight: "100vh", width: "100%", backgroundColor: "#EFF3F9" }}>
        <Sider collapsible collapsedWidth={0} collapsed={collapsed} breakpoint="lg" onBreakpoint={breakpoint => console.log(breakpoint, "Breaking point")} onCollapse={(collapsed, type) => setCollapsed(collapsed)} style={{backgroundColor: "white", minHeight: "100vh", overflow: "hidden", position: "fixed", zIndex: 100, top: 0, left: 0, bottom: 0}}>
          <div className="logo">
            <img className="logo-img" src="/primary-nav-icon.png" />
            {!isMobile && <span className="logo-title">WRAPPED CBDC</span>}
            {isMobile && <CloseOutlined onClick={() => setCollapsed(collapsed => !collapsed)} style={{marginLeft: "40%"}} /> }
          </div>
          <Menu mode="vertical" defaultSelectedKeys={[menuKey]} selectedKeys={[menuKey]} theme="light" onClick={({ key, keyPath }) => {
            if(key === 'overview') {
              location.assign('/dashboard/')
            } else if(key === 'logout') {
              location.assign('/');
            } else {
              location.assign("/dashboard/"+key)
            }
            setMenuKey(key);
          }} style={{height: "100%"}}>
            <Menu.Item key="overview" icon={<DashboardIcon style={{fontSize: 24}} />}> Overview</Menu.Item>
            { // Enable menu if KYC has been completed
              user?.kyc_verified && <>
                <Menu.Item key="deposit" icon={<TradeIcon style={{fontSize: 24}} />}>Deposit</Menu.Item>
                <Menu.Item key="redeem" icon={<WalletIcon style={{fontSize: 24}} />}>Redeem</Menu.Item>
                <Menu.Item key="withdraw" icon={<WithdrawIcon style={{fontSize: 24}} />}>Withdraw</Menu.Item>
              </>
            }
            { // Disable menu and show tooltip hint if account KYC has not been verified
              !user?.kyc_verified && <>
                <Tooltip title="Complete KYC to access platform feature. Go to Account settings - KYC Verification" placement="right" trigger="click">
                  <Menu.Item key="deposit" icon={<TradeIcon style={{fontSize: 24}} />} disabled={!user?.kyc_verified}>Deposit</Menu.Item>
                </Tooltip>
                <Tooltip title="Complete KYC to access platform feature. Go to Account settings - KYC Verification" placement="right">
                  <Menu.Item key="redeem" icon={<WalletIcon style={{fontSize: 24}} />} disabled={!user?.kyc_verified}>Redeem</Menu.Item>
                </Tooltip>
                <Tooltip title="Complete KYC to access platform feature. Go to Account settings - KYC Verification" placement="right">
                  <Menu.Item key="withdraw" icon={<WithdrawIcon style={{fontSize: 24}} />} disabled={!user?.kyc_verified}>Withdraw</Menu.Item>
                </Tooltip>
              </>
            }
            <Menu.Item key="history" icon={<HelpIcon style={{fontSize: 24}} />}>History</Menu.Item>
            {/* <Menu.Item key="help">Help</Menu.Item>
            <Menu.Item key="logout">Logout</Menu.Item> */}
          </Menu>
        </Sider>
        <Layout style={{ backgroundColor: "#EFF3F9", minHeight: "100%", overflowY: "scroll", marginLeft: isMobile? 0 : collapsed? 0 : 200, transition: "all 1s ease 0s" }}>
          <Header style={{backgroundColor: "transparent", display: "flex", flexFlow: "row", alignItems: "center", justifyContent: "space-between", paddingLeft: isMobile? 20 : 50, paddingRight: isMobile? 10 : 50}}>
            {
              isMobile && <MenuOutlined onClick={() => setCollapsed(collapsed => !collapsed)} />
            }
            <h2 className="page-title">{pathname.includes('deposit')? 'Deposit' : pathname.includes('redeem')? 'Redeem' : pathname.includes('history')? 'History' : pathname.includes('help')? 'Help' : pathname.includes('account')? 'Account Settings' : pathname.includes('withdraw')? 'Withdraw' : 'Overview'}</h2>
            {
              isMobile && <MoreOutlined onClick={() => setIsSecVisible(visible => !visible)} style={{marginLeft: 10, marginRight: 10}} />
            }
            {
              isMobile && isSecVisible && 
            <div style={{position: "absolute", top: 64, right: 20, left: 20, display: "flex", alignItems: "center", justifyContent: "end"}}>
              <Badge count={<BellFilled style={{ color: '#FFF' }} />} style={{background: "rgba(0, 173, 230, 1.0)",
                background: "-webkit-linear-gradient(top left, rgba(0, 173, 230, 1.0), rgba(64, 71, 204, 1.0))",
                background: "-moz-linear-gradient(top left, rgba(0, 173, 230, 1.0), rgba(64, 71, 204, 1.0))",
                background: "linear-gradient(to bottom right, rgba(0, 173, 230, 1.0), rgba(64, 71, 204, 1.0))", padding: "5px 10px", marginRight: 10, borderRadius: 20}} />
              <Avatar shape="square" size="default" icon={<UserOutlined />} />
              <Dropdown.Button overlay={menu} type="link" trigger="hover" icon={<DownOutlined />} style={{paddingLeft: 0, paddingRight: 0}}>
                  <span>{loadData?.data?.user?.first_name || 'No'} {loadData?.data?.user?.last_name || "User"}</span>
              </Dropdown.Button>
            </div>
            }
            {
              !isMobile && 
            <div className="header-right">
              <Badge count={<BellFilled style={{ color: '#FFF' }} />} style={{background: "rgba(0, 173, 230, 1.0)",
                background: "-webkit-linear-gradient(top left, rgba(0, 173, 230, 1.0), rgba(64, 71, 204, 1.0))",
                background: "-moz-linear-gradient(top left, rgba(0, 173, 230, 1.0), rgba(64, 71, 204, 1.0))",
                background: "linear-gradient(to bottom right, rgba(0, 173, 230, 1.0), rgba(64, 71, 204, 1.0))", padding: "5px 10px", marginRight: 10, borderRadius: 20}} />
              <Avatar shape="square" size="default" icon={<UserOutlined />} />
              <Dropdown.Button overlay={menu} type="link" trigger="hover" icon={<DownOutlined />} style={{paddingLeft: 0, paddingRight: 0}}>
                  <span>{loadData?.data?.user?.first_name || 'No'} {loadData?.data?.user?.last_name || "User"}</span>
              </Dropdown.Button>
            </div>
            }
          </Header>
          <Content style={{padding: screens?.xxl? "50px 550px 50px 50px" : screens.sm? "50px 10px 50px 10px" : screens.xs? "50px 10px 50px 10px" : "50px 250px 50px 50px"}}>
            {
              !pathname.includes('account') && user?.kyc_verified &&
              <Row gutter={[16, 16]} style={{ marginLeft: 0, marginRight: 0, marginBottom: 16, padding: 0}}>
                <Col xs={{span: 24}} lg={{span: 8}} style={{height: "inherit" }}>
                  <Link to="/dashboard/">
                    <BalanceCard balances={loadData?.data?.balances} />
                  </Link>
                </Col>
                <Col xs={{span: 24}} lg={{span: 8}} style={{height: "inherit"}}>
                  <Link to="/dashboard/deposit">
                    <DepositCard />
                  </Link>
                </Col>
                <Col xs={{span: 24}} lg={{span: 8}} style={{height: "inherit"}}>
                  <Link to="/dashboard/redeem">
                    <RedeemCard />
                  </Link>
                </Col>
              </Row>
            }
            {
              !user?.kyc_verified && <Row gutter={[16, 16]} style={{ marginLeft: 0, marginRight: 0, marginBottom: 16, padding: 0}}>
              <Col xs={{span: 24}} lg={{span: 8}} style={{height: "inherit" }}>
                <Link to="/dashboard/">
                  <BalanceCard balances={loadData?.data?.balances} />
                </Link>
              </Col>
              <Col xs={{span: 24}} lg={{span: 8}} style={{height: "inherit"}}>
                <Tooltip title="Complete KYC to access platform feature. Go to Account settings - KYC Verification" placement="top">
                  <Link to="#">
                      <DepositCard />
                  </Link>
                </Tooltip>
              </Col>
              <Col xs={{span: 24}} lg={{span: 8}} style={{height: "inherit"}}>
                <Tooltip title="Complete KYC to access platform feature. Go to Account settings - KYC Verification" placement="top">
                  <Link to="#">
                    <RedeemCard />
                  </Link>
                </Tooltip>
              </Col>
            </Row>
            }
            <Outlet context={context} />
          </Content>
          {/* <Footer>Footer Hear</Footer> */}
        </Layout>
      </Layout>
    </QueryClientProvider>
  );
}

export function ErrorBoundary({ error }) {
  console.error(error);
  return (
    <Empty
      image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
      imageStyle={{
        height: 60,
      }}
      style={{height: "100vh", verticalAlign: "middle"}}
      description={
        <span>
          <b>An error occurred with getting the users details, please bare with us</b>
          <br/>
          <a href="/?view=login">Back to Login</a>
        </span>
      }
    >
      {error.toString()}
  </Empty>
  )
}

export function CatchBoundary() {
  const caught = useCatch();

  return (
    <div>
      <h1>Caught</h1>
      <p>Status: {caught.status}</p>
      <pre>
        <code>{JSON.stringify(caught.data, null, 2)}</code>
      </pre>
    </div>
  );
}
