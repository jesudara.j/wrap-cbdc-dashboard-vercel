import { Outlet } from "remix";
export default function Admin () {

  return (
    <Outlet />
  );
}