import { redirect } from "remix";
import { userPrefs } from "~/cookies";

// Clear cookies and redirect users back to login screen
export const loader = async ({ request }) => {
  const cookieHeader = request.headers.get("Cookie");
  let cookie = (await userPrefs.parse(cookieHeader)) || {};

  // Check If token is set and cookie has data
  if(cookie || typeof cookie.token !== 'undefined') { //* If it does reset and redirect to login
    cookie = {};
    return redirect('/', {
      headers: {
        "Set-Cookie": await userPrefs.serialize(cookie),
      }
    });
  }

  //* If not reset anyways and redirect 

  cookie = {};
  return redirect('/', {
    headers: {
      "Set-Cookie": await userPrefs.serialize(cookie),
    }
  });
  
};