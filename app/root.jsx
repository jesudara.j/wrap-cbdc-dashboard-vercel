import antdStyle from "antd/dist/antd.min.css";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
} from "remix";
import globalStyles from "~/styles/global.css";

export const links = () => {
  return [
    { rel: "stylesheet", href: antdStyle },
    { rel: "stylesheet", href: globalStyles },
  ];
};

export function meta() {
  return { title: "Convexity Wrapped CBDC's" };
}

export default function App() {
  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <link rel="icon" type="image/png" href="/primary-nav-icon.png" />
        <Meta />
        <Links />
      </head>
      <body>
        <Outlet />
        <ScrollRestoration />
        <Scripts />
        {process.env.NODE_ENV === "development" && <LiveReload />}
      </body>
    </html>
  );
}

export function ErrorBoundary({ error }) {
  console.error(error);
  return (
    <html>
      <head>
        <title>Oh no!</title>
        <Meta />
        <Links />
      </head>
      <body>
        {/* add the UI you want your users to see */}
        <h1>An error {error} occurred</h1>
        <Scripts />
      </body>
    </html>
  );
}
