import { ethers } from "ethers";
import "isomorphic-fetch";
import Cookies from "js-cookie";
import invariant from "tiny-invariant";
import { contractAddress, tokenABI } from "./bscToken";

const Contract = ethers.Contract;
const Provider = ethers.providers;
const Utils = ethers.utils;

const API_URL =
  process.env.NODE_ENV === "development"
    ? `http://localhost:8080/api/v1`
    : `https://wrapcbdc-staging-web3.herokuapp.com/api/v1`;

function fallbackCopyTextToClipboard(elemRef) {
  // elemRef.focus();
  elemRef.current.focus();
  // elemRef.select();
  elemRef.current.select();
  try {
    var successful = document.execCommand("copy");
    var result = successful
      ? { message: "Successfully copied", error: false }
      : { message: "Unsuccessful", error: true };
    console.log("Fallback: Copying text command was ", result);
    return result;
  } catch (err) {
    console.error("Fallback: Oops, unable to copy", err);
    return { message: "Error copying text try again", error: true };
  }
}
export async function copyTextToClipboard(text, elemRef) {
  if (!navigator.clipboard) {
    return fallbackCopyTextToClipboard(elemRef);
  }
  return await navigator.clipboard.writeText(text).then(
    () => {
      console.log("Async: Copying to clipboard was successful!");
      return { message: "copied to clipboard", error: false };
    },
    (err) => {
      console.error("Async: Could not copy text: ", err);
      return { message: "Could not copy", error: true };
    }
  );
}

export const isCookieExpired = async (firstDate, secondDate) => {
  if (firstDate.setHours(0, 0, 0, 0) <= secondDate.setHours(0, 0, 0, 0)) {
    return true;
  }

  return false;
};

export async function loginUser({ email, password }) {
  // Call login EP, set token and push to dashboard
  console.log(email, password);
  if (email == "" || password == "" || email == null || password == null) {
    return { message: "All fields must be filled correctly", error: true };
  }
  const loginResp = await fetch(`${API_URL}/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json", // this needs to be defined
    },
    body: JSON.stringify({ email, password }),
  });
  const { status, message, data } = await loginResp.json();

  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    Cookies.set("token", data?.token);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function registerUser({
  firstname,
  lastname,
  email,
  phone,
  country,
  password,
  confirmPass,
}) {
  if (
    firstname == "" ||
    lastname == "" ||
    email == "" ||
    phone == "" ||
    country == "" ||
    password == "" ||
    firstname == null ||
    lastname == null ||
    email == null ||
    phone == null ||
    country == null ||
    password == null
  ) {
    return { message: "All fields must be filled correctly", error: true };
  } else if (!/^[a-zA-Z ]*$/.test(firstname)) {
    return { message: "Invalid first name entered", error: true };
  } else if (!/^[a-zA-Z ]*$/.test(lastname)) {
    return { message: "Invalid last name entered", error: true };
  } else if (!/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(email)) {
    return { message: "Invalid email entered", error: true };
  } else if (!/^[0-9]*$/.test(phone)) {
    return { message: "Invalid phone number entered", error: true };
  }
  // Create account
  console.log(email, phone, firstname, lastname, password, confirmPass);
  if (password !== confirmPass) {
    return { message: "Passwords do not match", error: true };
  }
  const signupResp = await fetch(`${API_URL}/signup`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json", // this needs to be defined
    },
    body: JSON.stringify({
      first_name: firstname,
      last_name: lastname,
      email: email,
      phone: phone,
      country: country,
      password: password,
    }),
  });
  const { status, message, data } = await signupResp.json();

  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message: "Account created, verify email and login to use dashboard",
      error: false,
      data,
      status,
    };
  })();
}

export async function getUserDetails(token) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/user-details`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function depositFiat(token, amount, network) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/deposit`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ amount, network }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    // Return the data with the payment link in the data param
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}
export async function verifyFiatDeposit(token, trx_id, network) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(
    `${API_URL}/verify_deposit?transactionId=${trx_id}&network=${network}`,
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`, // this needs to be defined
      },
    }
  );

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    // Return the data with the payment link in the data param
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}
export async function depositCNGN(token, amount, from, network) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);

  //* Listen for incoming payments on the address and call server endpoint when successful trx is gotten
  network = network.toLowerCase(); // transform to lower case for server
  console.log(network, "network");

  const resp = await fetch(`${API_URL}/deposit-cngn`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ amount, from, network }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function depositENGN(token, amount, from, network) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);

  //* Listen for incoming payments on the address and call server endpoint when successful trx is gotten
  network = network.toLowerCase(); // transform to lower case for server
  console.log(network, "network");

  const resp = await fetch(`${API_URL}/deposit-engn`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ amount, from, network }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function redeemENGN(token, amount, from, network) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);

  //* Listen for incoming payments on the address and call server endpoint when successful trx is gotten
  network = network.toLowerCase(); // transform to lower case for server
  console.log(network, "network");

  const resp = await fetch(`${API_URL}/redeem-engn`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ amount, paymentCode: from, network }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function redeemCNGN(token, amount, address, network) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  network = network.toLowerCase();
  const resp = await fetch(`${API_URL}/crypto-redeem`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ amount, walletAddress: address, network }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function getBankList(token) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/bank_codes`, {
    // method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function getFiatRedeemFee(token, amount, currency = "NGN") {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/flutterwave-fee`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ amount, currency }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function redeemFiat(
  token,
  amount,
  bankAccount,
  bankCode,
  network
) {
  // const token = Cookies.get("token");
  network = network.toLowerCase();
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/transfer`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ amount, bankAccount, bankCode, network }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}
export async function preferences(token, type, value) {
  // const token = Cookies.get("token");
  console.log("Cookies", token, type, value);
  const resp = await fetch(
    `${API_URL}/preferences?type=${type}&value=${
      typeof value === String ? value : undefined
    }`,
    {
      method: typeof value === String ? "GET" : "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`, // this needs to be defined
      },
      body: typeof value === String ? null : JSON.stringify(value),
    }
  );

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function streamPayment(address, token, network) {
  // Kick off event source to follow payment to the address for both bantu and bsc (EVM networks)
  console.log(network, "Chosen network to track");
  try {
    switch (network) {
      case "XBN":
        invariant(token, "Access token is required");
        var currentTime = Date.now();
        var es = new EventSource(
          `https://expansion-testnet.bantu.network/accounts/${address}/payments`
        );
        es.onmessage = async function (message) {
          var result = message.data ? JSON.parse(message.data) : message;
          //console.log(
          //"🚀 ~ file: deposit.jsx ~ line 242 ~ CryptoDepositCard ~ result",
          //result
          //);
          if (new Date(result["created_at"]) <= currentTime) {
            // Compare the times on the transaction and current time
            console.warn(
              "This is a past transaction!!",
              "Current date",
              currentTime.toString()
            );
            return es;
          }

          // If a new transaction comes in verify status and asset_code
          if (
            result["transaction_successful"] &&
            result["asset_code"] === "CNGN"
          ) {
            // then send details to server
            console.info("Tranasaction was completed successfully");
            const res = await depositCNGN(
              token,
              result["amount"],
              result["from"],
              network
            );
            es.close(); //Close event before returning
            return location.assign("/dashboard/");
          }
        };
        es.onerror = function (error) {
          console.log(
            "🚀 ~ file: utils.jsx ~ line 246 ~ CryptoDepositCard ~ error",
            error
          );
        };
        return es;
        break;

      case "BSC":
        // Create the contract listener here
        invariant(token, "Access token is required"); //Make sure the token is available
        const provider = new Provider.JsonRpcProvider(
          "https://data-seed-prebsc-1-s1.binance.org:8545/"
        );

        // The contract object
        const contract = new Contract(contractAddress, tokenABI, provider);

        // A filter for when a specific address receives tokens
        const filter = contract.filters.Transfer(null, address);

        console.log(contract, filter, "Objects for listener");

        // Receive an event when that filter occurs
        await contract.on(filter, async (from, to, amount, event) => {
          // The to will always be "address"
          console.log(amount, from, to, event, "filter callback details");
          console.log(
            `I got ${Utils.formatUnits(
              amount,
              6
            )} from ${from}. Raw amount ${amount}`
          );
          // then send details to server
          console.info("Tranasaction was completed successfully");
          const res = await depositCNGN(
            token,
            Utils.formatUnits(amount, 6),
            from,
            network
          );
          console.log(res, "Response from CNGN deposit");
          return location.assign("/dashboard/");
        });
        break;

      default:
        break;
    }
  } catch (err) {
    console.error(
      "🚀 ~ file: utils.jsx ~ line 251 ~ CryptoDepositCard ~ err",
      err
    );
  }
}

export async function setUP2FA(token) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/setup2FA`, {
    // method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    // body: JSON.stringify({ amount, bankAccount, bankCode }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function verifyOTP(token, otp) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/verify2FAToken`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ token: otp }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}
export async function setupYubikey(token, otp) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/setup_yubikey?otp=${otp}`, {
    // method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    // body: JSON.stringify({ otp }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function verifyYubikey(token, otp) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/verify_yubikey?otp=${otp}`, {
    // method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    // body: JSON.stringify({ otp }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function deleteYubikey(token, otp) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/delete_yubikey?otp=${otp}`, {
    // method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    // body: JSON.stringify({ otp }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function resendEmailVerification(token) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/resend-email`, {
    // method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    // body: JSON.stringify({ token: otp }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function requestPasswordReset(email) {
  // const token = Cookies.get("token");
  // console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/reset_request`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      // Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ email }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function resetPassword(id, resetString, newPassword) {
  // const token = Cookies.get("token");
  // console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/reset_password`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      // Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ userId: id, resetString, newPassword }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function initVeriff(token, firstName, lastName, type) {
  // const token = Cookies.get("token");
  // console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/init_kyc`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ firstName, lastName, accountType: type }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    console.log("Error", message, data, status);
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function getCryptoDepositQR(token, amount, network) {
  // const token = Cookies.get("token");
  // console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/deposit_qrcode/${amount}/${network}`, {
    // method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    console.log("Error", message, data, status);
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function uploadKYCFiles(token, formData) {
  // const token = Cookies.get("token");
  // console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/upload_kyc_docs`, {
    method: "POST",
    headers: {
      // "Content-Type": "multipart/form-data",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: formData,
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    console.log("Error", message, data, status);
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function uploadProfilePic(token, formData) {
  // const token = Cookies.get("token");
  // console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/upload-profile`, {
    method: "POST",
    headers: {
      // "Content-Type": "multipart/form-data",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: formData,
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    console.log("Error", message, data, status);
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function addBantuUserId(token, bantuUserId) {
  // const token = Cookies.get("token");
  // console.log("Cookies", token);
  console.log(bantuUserId, "User ID");
  const resp = await fetch(`${API_URL}/bantu_user_id`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ bantuUserId }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    console.log("Error", message, data, status);
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

/*
 * Admin access
 */

export async function loginAdmin({ email, password }) {
  // Call login EP, set token and push to dashboard
  console.log(email, password);
  if (email == "" || password == "" || email == null || password == null) {
    return { message: "All fields must be filled correctly", error: true };
  }
  const loginResp = await fetch(`${API_URL}/admin/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json", // this needs to be defined
    },
    body: JSON.stringify({ email, password }),
  });
  const { status, message, data } = await loginResp.json();

  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    Cookies.set("token", data?.token);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function getAdminDetails(token) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/admin/user-details`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

/*
 * Admin CRUD operations
 *
 *
 */

export async function getTransactions(token) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/admin/transactions`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function getAllUsers(token) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/admin/users`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

// Get list of KYC documents
export async function getKYCDocs(token, userEmail) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/admin/kyc_docs/${userEmail}`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
  });

  return await resp.json();

  // const { status, message, data } = await resp.json();
  // if (status !== "success") {
  //   return {
  //     message,
  //     error: true,
  //     data,
  //     status,
  //   };
  // }
  // return (() => {
  //   console.log("Returned data", data);
  //   return {
  //     message,
  //     error: false,
  //     data,
  //     status,
  //   };
  // })();
}

export async function changeUserAccountStatus(token, accountStatus, userEmail) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/admin/account_status`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ status: accountStatus, email: userEmail }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function approveKYCDocs(token, accountStatus, userEmail) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/admin/approve_kyc`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ status: accountStatus, email: userEmail }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function rejectKYCData(token, docsList, rejectionText, userEmail) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(`${API_URL}/admin/reject_kyc`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`, // this needs to be defined
    },
    body: JSON.stringify({ docsList, email: userEmail, rejectionText }),
  });

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function approveTransactionByAdmin(
  token,
  trx_ref,
  userId,
  adminId
) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(
    `${API_URL}/admin/approve_mint/${userId}/${trx_ref}/${adminId}`,
    {
      // method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`, // this needs to be defined
      },
    }
  );

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function declineTransactionByAdmin(
  token,
  trx_ref,
  userId,
  adminId
) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(
    `${API_URL}/admin/decline_mint/${userId}/${trx_ref}/${adminId}`,
    {
      // method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`, // this needs to be defined
      },
    }
  );

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}

export async function changeENGNTransactionStatusByAdmin(
  token,
  trx_ref,
  userId,
  adminId,
  trx_status
) {
  // const token = Cookies.get("token");
  console.log("Cookies", token);
  const resp = await fetch(
    `${API_URL}/admin/engn_trx_status/${trx_ref}/${trx_status}/${userId}/${adminId}`,
    {
      // method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`, // this needs to be defined
      },
    }
  );

  const { status, message, data } = await resp.json();
  if (status !== "success") {
    return {
      message,
      error: true,
      data,
      status,
    };
  }
  return (() => {
    console.log("Returned data", data);
    return {
      message,
      error: false,
      data,
      status,
    };
  })();
}
