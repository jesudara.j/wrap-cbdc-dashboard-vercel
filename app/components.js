import { ExclamationCircleFilled, UploadOutlined } from "@ant-design/icons";
import { Button, Card, Modal, Upload } from "antd";
import {
  ArcElement,
  CategoryScale,
  Chart as ChartJS,
  Legend,
  LinearScale,
  LineElement,
  PointElement,
  Title,
  Tooltip,
} from "chart.js";
import faker from "faker";
import React from "react";
import { Line, Pie } from "react-chartjs-2";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  ArcElement,
  Title,
  Tooltip,
  Legend
);

export const dataForPie = {
  labels: ["cNGN"],
  datasets: [
    {
      label: "100% cNGN",
      data: [12, 19, 3, 5, 2, 3],
      backgroundColor: [
        "rgba(255, 99, 132, 1)",
        // "rgba(54, 162, 235, 0.2)",
        // "rgba(255, 206, 86, 0.2)",
        // "rgba(75, 192, 192, 0.2)",
        // "rgba(153, 102, 255, 0.2)",
        // "rgba(255, 159, 64, 0.2)",
      ],
      borderColor: [
        "rgba(255, 99, 132, 1)",
        // "rgba(54, 162, 235, 1)",
        // "rgba(255, 206, 86, 1)",
        // "rgba(75, 192, 192, 1)",
        // "rgba(153, 102, 255, 1)",
        // "rgba(255, 159, 64, 1)",
      ],
      borderWidth: 1,
    },
  ],
};

export const pieOptions = {
  plugins: {
    legend: {
      position: "right",
    },
    datalabels: {
      display: true,
      align: "center",
      backgroundColor: "#ccc",
      borderRadius: 3,
      font: {
        size: 18,
      },
      formatter: (value) => {
        return value + "%";
      },
    },
  },
};

export const options = {
  responsive: true,
  bezierCurve: true,
  plugins: {
    legend: {
      position: "top",
    },
    title: {
      display: false,
      text: "Chart.js Line Chart",
    },
  },
};

const labels = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

const monthNumber = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

export const data = {
  labels,
  datasets: [
    {
      label: "Redeem",
      data: labels.map(() =>
        faker.datatype.number({ min: 30000000, max: 100000000 })
      ),
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 99, 132, 0.5)",
      lineTension: 0.5,
      hoverBackgroundColor: "yellow",
      hoverBorderColor: "yellow",
      pointHoverRadius: 6,
    },
    {
      label: "Deposit",
      data: labels.map(() =>
        faker.datatype.number({ min: 30000000, max: 100000000 })
      ),
      borderColor: "rgb(127, 127, 213)",
      backgroundColor: "rgba(231, 134, 215, 1.0)",
      lineTension: 0.5,
      hoverBackgroundColor: "rgba(231, 134, 215, 1.0)",
      hoverBorderColor: "rgba(231, 134, 215, 1.0)",
      pointHoverRadius: 6,
    },
  ],
};

export const LineChart = ({ transactions, isAdmin = false }) => {
  // console.log(transactions);
  const [lineData, setLineData] = React.useState(data); // Data state for the line chart
  const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
  React.useEffect(() => {
    // When the component has mounted get the correct data from the users transaction
    console.log(transactions, "Transaction list");
    setLineData({
      labels,
      datasets: [
        {
          label: "Redeem",
          data: monthNumber.map((month, i) =>
            !isAdmin
              ? transactions
                  ?.filter((val) => val["status"] === "completed")
                  ?.filter(
                    (val) =>
                      Number(
                        new Date(val["createdAt"])
                          .toLocaleString("en-US", { timeZone: timezone })
                          .split("")[0]
                      ) === month && val["trx_type"].match("redeem") !== null
                  )
                  .map((filteredVal, i) => filteredVal["amount"])
                  .reduce((a, b) => a + b, 0)
              : transactions?.filter(
                  (val) =>
                    Number(
                      new Date(val["createdAt"])
                        .toLocaleString("en-US", { timeZone: timezone })
                        .split("")[0]
                    ) === month && val["trx_type"].match("redeem") !== null
                ).length
          ),
          borderColor: "rgb(255, 99, 132)",
          backgroundColor: "rgba(255, 99, 132, 0.5)",
          lineTension: 0.5,
          hoverBackgroundColor: "yellow",
          hoverBorderColor: "yellow",
          pointHoverRadius: 6,
        },
        {
          label: "Deposit",
          data: monthNumber.map((month, i) =>
            !isAdmin
              ? transactions
                  ?.filter(
                    (val) =>
                      Number(
                        new Date(val["createdAt"])
                          .toLocaleString("en-US", { timeZone: timezone })
                          .split("")[0]
                      ) === month && val["trx_type"].match("deposit") !== null
                  )
                  .map((filteredVal, i) => filteredVal["amount"])
                  .reduce((a, b) => a + b, 0)
              : transactions?.filter(
                  (val) =>
                    Number(
                      new Date(val["createdAt"])
                        .toLocaleString("en-US", { timeZone: timezone })
                        .split("")[0]
                    ) === month && val["trx_type"].match("deposit") !== null
                ).length
          ),
          borderColor: "rgb(127, 127, 213)",
          backgroundColor: "rgba(231, 134, 215, 1.0)",
          lineTension: 0.5,
          hoverBackgroundColor: "rgba(231, 134, 215, 1.0)",
          hoverBorderColor: "rgba(231, 134, 215, 1.0)",
          pointHoverRadius: 6,
        },
      ],
    });

    return () => console.log("Unmounted component"); //when component unmounts clean state is necessary
  }, [transactions]);

  // render the (Redeem and Deposit) LineChart
  // @params: options - options for rendering the line chart
  // @params: data - data passed to the chart to be displayed
  return <Line options={options} data={lineData} minheight={260} />;
};

export const PieChart = (props) => {
  return <Pie data={dataForPie} options={pieOptions} />;
};

// TODO: Unhook the balance data from the general user data to make the app load faster, instead of blocking till balance is returned from blockchains
export const BalanceCard = ({ balances }) => {
  return (
    <Card
      bodyStyle={{
        display: "flex",
        flexFlow: "row",
        justifyContent: "space-between",
        alignItems: "center",
      }}
      style={{
        borderRadius: 20,
        height: "100%",
        background: "rgba(215, 246, 255, 1.0)",
        background:
          "-webkit-linear-gradient(bottom left, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
        background:
          "-moz-linear-gradient(bottom left, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
        background:
          "linear-gradient(to top right, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
      }}
      hoverable
    >
      <img src="/dash_balance.png" width={40} height={40} />
      <div
        style={{
          flex: 2,
          display: "flex",
          flexFlow: "column",
          justifyContent: "space-between",
          alignItems: "end",
          color: "white",
          fontSize: 14,
        }}
      >
        {!balances || typeof balances === "undefined" ? (
          <span>
            <b>0.00</b> cNGN
          </span>
        ) : balances.some(
            (el) =>
              el["asset_code"] === "CNGN (XBN)" ||
              el["asset_code"] === "CNGN (BSC)"
          ) ? (
          <span>
            <b>
              {balances
                ?.filter(
                  (item) =>
                    item["asset_code"] === "CNGN (XBN)" ||
                    item["asset_code"] === "CNGN (BSC)"
                )
                .map((val) => Number(val["balance"]))
                .reduce((a, b, i) => a + b, 0)
                .toFixed(2)
                .replace(/\d(?=(\d{3})+\.)/g, "$&,")}
            </b>{" "}
            cNGN
          </span>
        ) : (
          <span>
            <b>0.00</b> cNGN
          </span>
        )}
        {/* {balances.some((el) => el["asset_code"] === "cngn (BSC)") ? (
          balances
            ?.filter((item) => item["asset_code"] === "cngn (BSC)")
            .map((item, i) => (
              <span key={i}>
                <b>
                  {Number(item["balance"])
                    .toFixed(2)
                    .replace(/\d(?=(\d{3})+\.)/g, "$&,")}
                </b>{" "}
                cNGN (BSC)
              </span>
            ))
        ) : (
          <b>0.00</b>
        )} */}
        <span>Balance</span>
      </div>
    </Card>
  );
};
export const DepositCard = (props) => (
  <Card
    bodyStyle={{
      display: "flex",
      flexFlow: "row",
      justifyContent: "center",
      alignItems: "center",
    }}
    style={{
      borderRadius: 20,
      height: "100%",
      display: "flex",
      // flexFlow: "row",
      justifyContent: "start",
      alignItems: "center",
    }}
    hoverable
  >
    <img src="/Wallet.png" width={40} height={40} />
    <div
      style={{
        flex: 2,
        display: "flex",
        flexFlow: "column",
        justifyContent: "center",
        alignItems: "start",
        color: "#3F47CC",
        fontSize: 17,
      }}
    >
      <span>DEPOSIT</span>
    </div>
  </Card>
);

export const RedeemCard = (props) => (
  <Card
    bodyStyle={{
      display: "flex",
      flexFlow: "row",
      justifyContent: "center",
      alignItems: "center",
    }}
    style={{
      borderRadius: 20,
      height: "100%",
      display: "flex",
      // flexFlow: "row",
      justifyContent: "start",
      alignItems: "center",
    }}
    hoverable
  >
    <img src="/Message.png" width={40} height={40} />
    <div
      style={{
        flex: 2,
        display: "flex",
        flexFlow: "column",
        justifyContent: "space-between",
        alignItems: "start",
        color: "#3F47CC",
        fontSize: 17,
      }}
    >
      <span>REDEEM</span>
    </div>
  </Card>
);

export const CNGNCard = ({ tapHandler, isMobile }) => {
  return (
    <Card
      bodyStyle={{
        display: "flex",
        flexFlow: isMobile ? "column" : "row",
        justifyContent: "space-between",
        alignItems: "center",
      }}
      style={{
        borderRadius: 20,
        height: "100%",
        background: "rgba(215, 246, 255, 1.0)",
        background:
          "-webkit-linear-gradient(bottom left, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
        background:
          "-moz-linear-gradient(bottom left, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
        background:
          "linear-gradient(to top right, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
      }}
      onClick={() => tapHandler()}
      hoverable
    >
      <img src="/icon-512x512.png" width={70} height={70} />
      {!isMobile && (
        <div
          style={{
            flex: 2,
            display: "flex",
            flexFlow: "column",
            justifyContent: "space-between",
            alignItems: "end",
            color: "white",
            fontSize: 21,
          }}
        >
          <small>Deposit</small>
          <strong>cNGN</strong>
        </div>
      )}
    </Card>
  );
};

export const ENairaCard = ({ tapHandler, isMobile }) => (
  <Card
    bodyStyle={{
      display: "flex",
      flexFlow: isMobile ? "column" : "row",
      justifyContent: "space-between",
      alignItems: "center",
    }}
    style={{
      borderRadius: 20,
      height: "100%",
      background: "rgba(215, 246, 255, 1.0)",
      background:
        "-webkit-linear-gradient(bottom left, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
      background:
        "-moz-linear-gradient(bottom left, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
      background:
        "linear-gradient(to top right, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
    }}
    onClick={() => tapHandler()}
    hoverable
  >
    <img src="/e-naira-logo.png" width={70} height={70} />
    {!isMobile && (
      <div
        style={{
          flex: 2,
          display: "flex",
          flexFlow: "column",
          justifyContent: "space-between",
          alignItems: "end",
          color: "white",
          fontSize: 21,
        }}
      >
        <small>Deposit</small>
        <strong>eNAIRA</strong>
      </div>
    )}
  </Card>
);
export const CNGNCardRedeem = ({ tapHandler, isMobile }) => (
  <Card
    bodyStyle={{
      display: "flex",
      flexFlow: "row",
      justifyContent: "space-between",
      alignItems: "center",
    }}
    style={{
      borderRadius: 20,
      height: "100%",
      background: "rgba(215, 246, 255, 1.0)",
      background:
        "-webkit-linear-gradient(bottom left, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
      background:
        "-moz-linear-gradient(bottom left, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
      background:
        "linear-gradient(to top right, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
    }}
    onClick={() => tapHandler()}
    hoverable
  >
    <img src="/icon-512x512.png" width={70} height={70} />
    {!isMobile && (
      <div
        style={{
          flex: 2,
          display: "flex",
          flexFlow: "column",
          justifyContent: "space-between",
          alignItems: "end",
          color: "white",
          fontSize: 21,
        }}
      >
        <small>Redeem</small>
        <strong>cNGN</strong>
      </div>
    )}
  </Card>
);
export const CNGNCardWithdraw = ({ tapHandler, isMobile }) => (
  <Card
    bodyStyle={{
      display: "flex",
      flexFlow: "row",
      justifyContent: "space-between",
      alignItems: "center",
    }}
    style={{
      borderRadius: 20,
      height: "100%",
      background: "rgba(215, 246, 255, 1.0)",
      background:
        "-webkit-linear-gradient(bottom left, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
      background:
        "-moz-linear-gradient(bottom left, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
      background:
        "linear-gradient(to top right, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
    }}
    onClick={() => tapHandler()}
    hoverable
  >
    <img src="/icon-512x512.png" width={70} height={70} />
    {!isMobile && (
      <div
        style={{
          flex: 2,
          display: "flex",
          flexFlow: "column",
          justifyContent: "space-between",
          alignItems: "end",
          color: "white",
          fontSize: 21,
        }}
      >
        <small>Withdraw</small>
        <strong>cNGN</strong>
      </div>
    )}
  </Card>
);

export const ENairaCardRedeem = ({ tapHandler, isMobile }) => (
  <Card
    bodyStyle={{
      display: "flex",
      flexFlow: "row",
      justifyContent: "space-between",
      alignItems: "center",
    }}
    style={{
      borderRadius: 20,
      height: "100%",
      background: "rgba(215, 246, 255, 1.0)",
      background:
        "-webkit-linear-gradient(bottom left, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
      background:
        "-moz-linear-gradient(bottom left, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
      background:
        "linear-gradient(to top right, rgba(215, 246, 255, 1.0), rgba(84, 185, 234, 1.0))",
    }}
    onClick={() => tapHandler()}
    hoverable
  >
    <img src="/e-naira-logo.png" width={70} height={70} />
    {!isMobile && (
      <div
        style={{
          flex: 2,
          display: "flex",
          flexFlow: "column",
          justifyContent: "space-between",
          alignItems: "end",
          color: "white",
          fontSize: 21,
        }}
      >
        <small>Redeem</small>
        <strong>eNAIRA</strong>
      </div>
    )}
  </Card>
);

export const GodModal = ({
  isModalVisible,
  setGodModalVisible,
  handleOk,
  title,
  children,
  isMobile,
}) => {
  return (
    <Modal
      title={title}
      visible={isModalVisible}
      onOk={handleOk}
      width={isMobile ? "98.5%" : "60%"}
      style={{ borderRadius: 20 }}
      onCancel={() => setGodModalVisible(false)}
    >
      {children}
    </Modal>
  );
};

export const UploadComp = ({
  docStatus,
  label,
  bottomText,
  required = false,
  onFileSelect,
  onFileRemove,
}) => {
  // console.log(
  //   label
  //     .split(" ")
  //     .join("_")
  //     .replace("(", "_")
  //     .replace(")", "_")
  //     .split("_")
  //     .join(" ")
  //     .toUpperCase(),
  //   "Made all this "
  // );
  docStatus.forEach((element) => {
    console.log(element.field.trim(), "Fields");
    console.log(label.toUpperCase().includes(element.field.trim()));
  });
  const props = {
    name: "files",
    maxCount: 1,
    // action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
    // headers: {
    //   authorization: "authorization-text",
    // },
    // onChange(info) {
    //   if (info.file.status !== "uploading") {
    //     console.log(info.file, info.fileList);
    //   }
    //   if (info.file.status === "done") {
    //     message.success(`${info.file.name} file uploaded successfully`);
    //     onFileSelect(info.file);
    //   } else if (info.file.status === "error") {
    //     message.error(`${info.file.name} file upload failed.`);
    //   }
    // },
    onRemove: (file) => {
      console.log(file, "Removed files");
      onFileRemove(file);
    },
    beforeUpload: (file) => {
      console.log(file, "File reference before upload");
      const fileExt = file.name.split(".").pop();
      console.log(fileExt, "File extension");
      const renamedFile = new File(
        [file],
        `${label.split(" ").join("_")}.${fileExt}`
      );
      console.log(renamedFile, "Renamed File with label");
      onFileSelect(renamedFile);
      return false;
    },
    progress: {
      strokeColor: {
        "0%": "#108ee9",
        "100%": "#87d068",
      },
      strokeWidth: 3,
      format: (percent) => `${parseFloat(percent.toFixed(2))}%`,
    },
  };

  return (
    <>
      {!docStatus ||
        (!docStatus.length && (
          <>
            <span style={{ fontSize: 15 }}>
              <b>{label.toUpperCase()}</b>
            </span>{" "}
            {required && (
              <ExclamationCircleFilled
                style={{ color: "rgba(64, 193, 236, 1)" }}
              />
            )}
            <br />
            <Upload {...props} style={{ width: "100%" }}>
              <Button block style={{ width: "100%" }}>
                Click to upload <UploadOutlined />
              </Button>
            </Upload>
            {bottomText !== null && (
              <span style={{ fontSize: 10, paddingLeft: 20 }}>
                {bottomText}
              </span>
            )}
            <br />
            <br />
          </>
        ))}
      {docStatus.length &&
      docStatus.some(
        (val) =>
          // label
          // .split(" ")
          // .join("_")
          // .replace("(", "_")
          // .replace(")", "_")
          // .split("_")
          // .join(" ")
          // .toUpperCase() == val?.field
          label.toUpperCase().includes(val?.field.trim()) && val?.rejected
      ) ? (
        <>
          <span style={{ fontSize: 15 }}>
            <b>{label.toUpperCase()}</b>
          </span>{" "}
          {required && (
            <ExclamationCircleFilled
              style={{ color: "rgba(64, 193, 236, 1)" }}
            />
          )}
          <br />
          <Upload {...props} style={{ width: "100%" }}>
            <Button block style={{ width: "100%" }}>
              Click to upload <UploadOutlined />
            </Button>
          </Upload>
          {bottomText !== null && (
            <span style={{ fontSize: 10, paddingLeft: 20 }}>{bottomText}</span>
          )}
          <br />
          <br />
        </>
      ) : null}
    </>
  );
};
